// receipt.js
import React from "react";

const Receipt = () => {
  const printReceipt = () => {
    window.print();
  };

  const closeTabAfterPrint = () => {
    window.onafterprint = () => {
      window.close();
    };
  };

  return (
    <div className="ticket">
      <div className="header">
        <div className="centered">
          RECEIPT EXAMPLE <br />
          Address line 1 <br />
          Address line 2
        </div>
      </div>
      <div className="receiver-sender">
        <p>Nama Penerima : Suci</p>
        <p>Nama Pengirim : Aldo</p>
      </div>
      <table className="items">
        <thead>
          <tr>
            <th className="quantity">Q.</th>
            <th className="description">Description</th>
            <th className="price">$$</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="quantity">1.00</td>
            <td className="description">ARDUINO UNO R3</td>
            <td className="price">$25.00</td>
          </tr>
          <tr>
            <td className="quantity">2.00</td>
            <td className="description">JAVASCRIPT BOOK</td>
            <td className="price">$10.00</td>
          </tr>
          <tr>
            <td className="quantity">1.00</td>
            <td className="description">STICKER PACK</td>
            <td className="price">$10.00</td>
          </tr>
          <tr>
            <td colSpan="2" className="total">
              TOTAL
            </td>
            <td className="price">$55.00</td>
          </tr>
        </tbody>
      </table>
      <div className="button-container">
        <button
          id="btnPrint"
          className="hidden-print"
          onClick={printReceipt}
          onMouseDown={closeTabAfterPrint}
        >
          Print
        </button>
      </div>
    </div>
  );
};

export default Receipt;
