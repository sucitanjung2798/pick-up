import { useState } from "react";
import { Box, Button, Typography } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import React from "react";
import BackButton from "../../shared/BackButton";
import Header from "../../shared/Header";
import DeleteModal from "../../shared/DeleteModal";

const UserListDetail = () => {
  const navigate = useNavigate();

  const { id } = useParams();

  const [isOpen, setIsOpen] = useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setIsOpen({ ...isOpen, [anchor]: open });
  };

  return (
    <>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="User List Detail" isNotificationsNeed={true} />
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          p: 2,
        }}
      >
        <BackButton onClick={() => navigate(`/manage-user/lists`)} />

        <Box
          sx={{
            mt: 2,
            display: "flex",
            flexDirection: "column",
            gap: 1,
            border: "1px solid #bdbdbd",
            p: 2,
            borderRadius: "10px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Name</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>Suci Tanjung</Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Email</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>
              sucitanjung@gmail.com
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Department</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>IT</Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Job</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Frontend Developer Staff
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Role</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>Admin</Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Permissions</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>Manage Admin</Typography>
          </Box>
        </Box>

        <Box
          sx={{
            width: "100%",
            display: "flex",
            gap: 2,
          }}
        >
          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",

              color: "#ffffff",
              fontSize: "18px",
              bgcolor: "#78251E",
              height: "45px",
              borderRadius: "10px",
              ":hover": {
                bgcolor: "#78251E",
                color: "#ffffff",
              },
            }}
            onClick={() => navigate(`/manage-user/edit/${id}`)}
            fullWidth
          >
            Edit
          </Button>

          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",

              color: "#78251E",
              fontSize: "18px",
              bgcolor: "#ffffff",
              border: "1px solid #78251E",
              height: "45px",
              borderRadius: "10px",
              ":hover": {
                bgcolor: "#ffffff",
                color: "#78251E",
                border: "1px solid #78251E",
              },
            }}
            onClick={toggleDrawer("bottom", true)}
            fullWidth
          >
            Delete
          </Button>
        </Box>
      </Box>

      <DeleteModal
        isOpen={isOpen}
        toggleDrawer={toggleDrawer}
        title="Delete User List"
        path="manage-user/lists"
        message="Are you sure wan't to delete this User?"
      />
    </>
  );
};

export default UserListDetail;
