import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  MenuItem,
  OutlinedInput,
  Select,
  Switch,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import CustomButtonSave from "../../shared/CustomButtonSave";
import { viewDepartmentActive } from "../../api/departments";
import { viewRoleActive } from "../../api/roles";
import { addNewUser } from "../../api/users";
import CheckIcon from "../../assets/check-icon.png";
import CheckIconInactive from "../../assets/check-icon-inactive.png";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const AddNewUser = () => {
  const navigate = useNavigate();

  const [departmentData, setDepartmentData] = useState([]);
  const [roleData, setRoleData] = useState([]);

  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [departmentName, setDepartmentName] = useState("");
  const [roleName, setRoleName] = useState("");
  const [description, setDescription] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [status, setStatus] = useState(false);

  const [isEmailError, setIsEmailError] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isUserNameError, setIsUserNameError] = useState(false);
  const [isPhoneNumberError, setIsPhoneNumberError] = useState(false);
  const [isDepartmentNameError, setIsDepartmentNameError] = useState(false);
  const [isEmployeeIdError, setIsEmployeeIdError] = useState(false);

  const [isRoleNameError, setIsRoleNameError] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const emailValid = (email) => {
    return /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/.test(email);
  };

  const getDepartments = async () => {
    try {
      const {
        results: { data },
      } = await viewDepartmentActive();
      setDepartmentData(data);
    } catch (err) {
      console.error(err);
    }
  };

  const getRoleData = async () => {
    try {
      const {
        results: { data },
      } = await viewRoleActive();
      setRoleData(data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getDepartments();
    getRoleData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (
      email !== "" &&
      emailValid(email) &&
      userName !== "" &&
      phoneNumber !== "" &&
      roleName !== "" &&
      departmentName !== "" &&
      employeeId !== ""
    ) {
      try {
        setIsLoading(true);

        const data = {
          email,
          employeeId,
          name: userName,
          phoneNumber,
          description,
          status: status === false ? 0 : 1,
          roleId: roleName,
          departmentId: departmentName,
        };

        await addNewUser(data);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
        navigate("/manage-user/lists");
      }
    } else {
      email ? setIsEmailError(false) : setIsEmailError(true);
      emailValid(email) ? setIsEmailValid(false) : setIsEmailValid(true);
      userName ? setIsUserNameError(false) : setIsUserNameError(true);
      phoneNumber ? setIsPhoneNumberError(false) : setIsPhoneNumberError(true);
      roleName ? setIsRoleNameError(false) : setIsRoleNameError(true);
      departmentName
        ? setIsDepartmentNameError(false)
        : setIsDepartmentNameError(true);
      employeeId ? setIsEmployeeIdError(false) : setIsEmployeeIdError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ zIndex: 10, bgcolor: "#fff", position: "sticky", top: 0 }}>
        <Header title="Add New User" isNotificationsNeed={true} />

        <Box sx={{ p: 2 }}>
          <BackButton onClick={() => navigate("/manage-user/lists")} />
        </Box>
      </Box>

      <Box component="form" onSubmit={handleSubmit} sx={{ p: 2 }}>
        <Box sx={{ border: "1px solid #bdbdbd", p: 2, borderRadius: "10px" }}>
          <Box>
            <Typography>
              Email <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the email"
                sx={{ fontSize: "0.875rem" }}
                value={email}
                error={!email && isEmailError}
                onChange={(e) => {
                  if (emailValid(email)) {
                    setIsEmailValid(false);
                  } else {
                    setIsEmailValid(true);
                  }

                  setEmail(e.target.value);

                  if (email) {
                    setIsEmailError(false);
                  }
                }}
              />

              {isEmailError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input your email
                </FormHelperText>
              ) : isEmailValid ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the valid email
                </FormHelperText>
              ) : (
                ""
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the name"
                sx={{ fontSize: "0.875rem" }}
                value={userName}
                error={!userName && isUserNameError}
                onChange={(e) => {
                  setUserName(e.target.value);

                  if (userName) {
                    setIsUserNameError(false);
                  }
                }}
              />

              {isUserNameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the name
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Phone Number <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the phone number"
                sx={{ fontSize: "0.875rem" }}
                value={phoneNumber}
                type="number"
                error={!phoneNumber && isPhoneNumberError}
                onChange={(e) => {
                  setPhoneNumber(e.target.value);

                  if (phoneNumber) {
                    setIsPhoneNumberError(false);
                  }
                }}
              />

              {isPhoneNumberError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the phone number
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Employee ID <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the employee id"
                sx={{ fontSize: "0.875rem" }}
                value={employeeId}
                error={!employeeId && isEmployeeIdError}
                onChange={(e) => {
                  setEmployeeId(e.target.value);

                  if (employeeId) {
                    setIsEmployeeIdError(false);
                  }
                }}
              />

              {isEmployeeIdError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the employee id
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Role Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <FormControl
              fullWidth
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  // width: "7.188rem",
                  borderRadius: "8px",
                  fontSize: "0.875rem",
                },
              }}
            >
              <Select
                displayEmpty
                value={roleName}
                onChange={(e) => {
                  setRoleName(e.target.value);

                  if (roleName) {
                    setIsRoleNameError(false);
                  }
                }}
                input={<OutlinedInput error={!roleName && isRoleNameError} />}
              >
                <MenuItem disabled value="">
                  <Typography sx={{ fontSize: "0.875rem", color: "#bdbdbd" }}>
                    Select the role name
                  </Typography>
                </MenuItem>
                {roleData.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.roleId}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
              </Select>
              {isRoleNameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the role name
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Department Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <FormControl
              fullWidth
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  // width: "7.188rem",
                  borderRadius: "8px",
                  fontSize: "0.875rem",
                },
              }}
            >
              <Select
                displayEmpty
                value={departmentName}
                onChange={(e) => {
                  setDepartmentName(e.target.value);

                  if (departmentName) {
                    setIsDepartmentNameError(false);
                  }
                }}
                input={
                  <OutlinedInput
                    error={!departmentName && isDepartmentNameError}
                  />
                }
              >
                <MenuItem disabled value="">
                  <Typography sx={{ fontSize: "0.875rem", color: "#bdbdbd" }}>
                    Select the department name
                  </Typography>
                </MenuItem>
                {departmentData.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.departmentId}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
              </Select>
              {isDepartmentNameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the department name
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>Description</Typography>
            <Box
              sx={{
                mt: 0.5,
                borderRadius: "10px",
                "& .MuiTextField-root": { width: "100%" },
                "& .MuiOutlinedInput-root": {
                  borderColor: "#bdbdbd",
                  borderRadius: "10px",
                  fontSize: "0.875rem",
                },
              }}
              noValidate
              autoComplete="off"
              placeholder="Input your description"
            >
              <TextField
                multiline
                rows={4}
                placeholder="Input your description"
                value={description || ""}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Box>
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Status</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                label={
                  status === true ? (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Active
                    </Typography>
                  ) : (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Non Active
                    </Typography>
                  )
                }
                control={
                  <CustomSwitch
                    checked={status}
                    onChange={() => setStatus(!status)}
                  />
                }
              />
            </FormGroup>
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <CustomButtonSave content="Save" isLoading={isLoading} />
        </Box>
      </Box>
    </Box>
  );
};

export default AddNewUser;
