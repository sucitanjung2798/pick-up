import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import Header from "../../shared/Header";
import { allUsers, deleteUser } from "../../api/users";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import ScrollOnTop from "../../shared/ScrollOnTop";
import ManageUserCards from "./ManageUserCards";
import SearchWithDebounce from "../../shared/SearchWithDebounce";
import ListStatusDelivery from "../../shared/ListStatusDelivery";

const statusLists = [
  { value: 1, label: "Active", color: "00FF00" },
  { value: 2, label: "Inactive", color: "FF0000" },
];

const UserLists = ({ permissions }) => {
  const navigate = useNavigate();

  const [userData, setUserData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  const [openDialog, setOpenDialog] = useState(false);
  const [userId, setUserId] = useState(null);

  const [hasMore, setHasMore] = useState(false);
  const [status, setStatus] = useState("All");

  const getAllUsers = async ({ search, status }) => {
    setIsLoading(true);
    try {
      const res = await allUsers({ search, status });
      setUserData(res?.results?.data || []);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getAllUsers({ search: savedSearch || "", status });
  }, [savedSearch, status]);

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleDeleteUser = async () => {
    setIsLoading(true);

    await deleteUser(userId)
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoading(false);
        getAllUsers();
        setOpenDialog(false);
      });
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Manage Users" isNotificationsNeed={true} />

        <Box
          sx={{
            px: 2,
            pt: 2,
            display: "flex",
            flexDirection: "column",
            gap: 2,
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            <SearchWithDebounce
              search={search}
              setSearch={setSearch}
              setSearchParams={setSearchParams}
              updatedSearchParams={updatedSearchParams}
              setSavedSearch={setSavedSearch}
            />
            <Button
              sx={{
                border: "1px solid #78251E",
                borderRadius: "10px",
                px: 3,
                height: "40px",
                textTransform: "capitalize",
                color: "#000",
                fontWeight: 600,
              }}
              startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
              onClick={() => navigate("/add-new-user")}
            >
              New
            </Button>
          </Box>

          <Box>
            <ListStatusDelivery
              defaultText={"All"}
              listStatus={statusLists}
              selectedFile={status}
              setSelectedFile={setStatus}
              isDefault={true}
              textTransform="capitalize"
            />
          </Box>
        </Box>
      </Box>

      <ScrollOnTop />

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}>
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              variant="rounded"
              height="50px"
              animation="wave"
              key={index}
              sx={{ mt: 2 }}
            />
          ))}

        {!isLoading && userData?.length === 0 && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <Typography sx={{ textAlign: "center" }}>
              The User Data is empty
            </Typography>
          </Box>
        )}

        <ScrollOnTop />

        {!isLoading && userData?.length > 0 && (
          <ManageUserCards
            userData={userData}
            setHasMore={setHasMore}
            handleCloseDialog={handleCloseDialog}
            handleDeleteUser={handleDeleteUser}
            hasMore={hasMore}
            openDialog={openDialog}
            permissions={permissions}
            setOpenDialog={setOpenDialog}
            setUserData={setUserData}
            setUserId={setUserId}
          />
        )}
      </Box>
    </Box>
  );
};

export default UserLists;
