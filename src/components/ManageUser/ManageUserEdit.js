import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  FormHelperText,
  MenuItem,
  Skeleton,
  Typography,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import CustomButtonSave from "../../shared/CustomButtonSave";
import { viewDepartmentActive } from "../../api/departments";
import { viewRoleActive } from "../../api/roles";
import { editUser, showUser } from "../../api/users";
import CustomSwitch from "../../shared/CustomSwitch";
import CustomInput from "../../shared/CustomInput";
import CustomSelect from "../../shared/CustomSelect";
import CustomDescriptionField from "../../shared/CustomDescriptionField";

const AddNewUser = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [departmentData, setDepartmentData] = useState([]);
  const [roleData, setRoleData] = useState([]);

  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [departmentName, setDepartmentName] = useState("");
  const [roleName, setRoleName] = useState("");
  const [description, setDescription] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [status, setStatus] = useState(false);

  const [isEmailError, setIsEmailError] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isUserNameError, setIsUserNameError] = useState(false);
  const [isPhoneNumberError, setIsPhoneNumberError] = useState(false);
  const [isDepartmentNameError, setIsDepartmentNameError] = useState(false);
  const [isEmployeeIdError, setIsEmployeeIdError] = useState(false);

  const [isRoleNameError, setIsRoleNameError] = useState(false);

  const [isLoadingEdit, setIsLoadingEdit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const emailValid = (email) => {
    return /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/.test(email);
  };

  const getDepartments = async () => {
    try {
      const {
        results: { data },
      } = await viewDepartmentActive();
      setDepartmentData(data);
    } catch (err) {
      console.error(err);
    }
  };

  const getRoleData = async () => {
    try {
      const {
        results: { data },
      } = await viewRoleActive();
      setRoleData(data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getDepartments();
    getRoleData();

    const handleGetUser = async () => {
      setIsLoading(true);

      try {
        const {
          result: {
            emailAddress,
            name: userName,
            phoneNumber,
            department: { id: departmentId },
            role: { id: roleId },
            description,
            employeeId,
            status,
          },
        } = await showUser(id);
        setEmail(emailAddress);
        setUserName(userName);
        setPhoneNumber(phoneNumber);
        setDepartmentName(departmentId);
        setRoleName(roleId);
        setDescription(description);
        setEmployeeId(employeeId);
        setStatus(status?.value === 0 ? false : true);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    };
    handleGetUser(id);
  }, [id]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (
      email !== "" &&
      emailValid(email) &&
      userName !== "" &&
      phoneNumber !== "" &&
      roleName !== "" &&
      departmentName !== "" &&
      employeeId !== ""
    ) {
      try {
        setIsLoadingEdit(true);

        const data = {
          id,
          email,
          employeeId,
          name: userName,
          phoneNumber,
          description,
          status: status === false ? 0 : 1,
          roleId: roleName,
          departmentId: departmentName,
        };

        await editUser(id, data);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoadingEdit(false);
        navigate("/manage-user/lists");
      }
    } else {
      email ? setIsEmailError(false) : setIsEmailError(true);
      emailValid(email) ? setIsEmailValid(false) : setIsEmailValid(true);
      userName ? setIsUserNameError(false) : setIsUserNameError(true);
      phoneNumber ? setIsPhoneNumberError(false) : setIsPhoneNumberError(true);
      roleName ? setIsRoleNameError(false) : setIsRoleNameError(true);
      departmentName
        ? setIsDepartmentNameError(false)
        : setIsDepartmentNameError(true);
      employeeId ? setIsEmployeeIdError(false) : setIsEmployeeIdError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ zIndex: 10, bgcolor: "#fff", position: "sticky", top: 0 }}>
        <Header title="Add New User" isNotificationsNeed={true} />

        <Box sx={{ p: 2 }}>
          <BackButton onClick={() => navigate("/manage-user/lists")} />
        </Box>
      </Box>

      {isLoading && (
        <Box sx={{ p: 2 }}>
          <Skeleton
            sx={{ mt: 2 }}
            variant="rounded"
            height="700px"
            animation="wave"
          />
        </Box>
      )}

      {!isLoading && (
        <Box component="form" onSubmit={handleSubmit} sx={{ p: 2 }}>
          <Box sx={{ border: "1px solid #bdbdbd", p: 2, borderRadius: "10px" }}>
            <Box>
              <Typography>
                Email <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <CustomInput
                value={email}
                placeholder="Enter the email"
                sx={{ fontSize: "0.875rem" }}
                isValueError={isEmailError}
                isHelperText={true}
                onChange={(e) => {
                  if (emailValid(email)) {
                    setIsEmailValid(false);
                  } else {
                    setIsEmailValid(true);
                  }

                  setEmail(e.target.value);

                  if (email) {
                    setIsEmailError(false);
                  }
                }}
                helperText={
                  isEmailError ? (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input your email
                    </FormHelperText>
                  ) : isEmailValid ? (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input the valid email
                    </FormHelperText>
                  ) : (
                    ""
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={userName}
                onChange={(e) => {
                  setUserName(e.target.value);

                  if (userName) {
                    setIsUserNameError(false);
                  }
                }}
                placeholder="Enter the name"
                sx={{ fontSize: "0.875rem" }}
                isValueError={isUserNameError}
                isHelperText={true}
                helperText={
                  isUserNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input the name
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Phone Number <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={phoneNumber}
                onChange={(e) => {
                  setPhoneNumber(e.target.value);

                  if (phoneNumber) {
                    setIsPhoneNumberError(false);
                  }
                }}
                placeholder="Enter the phone number"
                isValueError={isPhoneNumberError}
                isHelperText={true}
                helperText={
                  isPhoneNumberError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input the phone number
                    </FormHelperText>
                  )
                }
                sx={{ fontSize: "0.875rem" }}
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Employee ID <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={employeeId}
                onChange={(e) => {
                  setEmployeeId(e.target.value);

                  if (employeeId) {
                    setIsEmployeeIdError(false);
                  }
                }}
                placeholder="Enter the employee id"
                isValueError={isEmployeeIdError}
                isHelperText={true}
                helperText={
                  isEmployeeIdError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input the employee id
                    </FormHelperText>
                  )
                }
                sx={{ fontSize: "0.875rem" }}
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Role Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomSelect
                value={roleName}
                onChange={(e) => {
                  setRoleName(e.target.value);

                  if (roleName) {
                    setIsRoleNameError(false);
                  }
                }}
                isValueError={isRoleNameError}
                data={roleData.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.roleId}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
                isHelperText={true}
                helperText={
                  isRoleNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                    >
                      Please select the role name
                    </FormHelperText>
                  )
                }
                placeholder="Please select the role name"
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Department Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomSelect
                value={departmentName}
                onChange={(e) => {
                  setDepartmentName(e.target.value);

                  if (departmentName) {
                    setIsDepartmentNameError(false);
                  }
                }}
                isValueError={isDepartmentNameError}
                data={departmentData.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.departmentId}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
                isHelperText={true}
                helperText={
                  isDepartmentNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                    >
                      Please select the department name
                    </FormHelperText>
                  )
                }
                placeholder="Please select the department name"
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>Description</Typography>
              <CustomDescriptionField
                value={description}
                setValue={setDescription}
                sx={{ mt: 0.5 }}
              />
            </Box>

            <Box
              sx={{
                mt: 2,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography>Status</Typography>
              <CustomSwitch
                label={
                  status === true ? (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Active
                    </Typography>
                  ) : (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Inactive
                    </Typography>
                  )
                }
                value={status}
                setValue={setStatus}
              />
            </Box>
          </Box>

          <Box sx={{ mt: 2 }}>
            <CustomButtonSave content="Save" isLoading={isLoadingEdit} />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default AddNewUser;
