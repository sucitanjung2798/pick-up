import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Skeleton,
  Typography,
} from "@mui/material";
import InfiniteScroll from "react-infinite-scroll-component";
import MenuComponent from "../../shared/MenuComponent";
import CustomDialog from "../../shared/CustomDialog";
import { allUsers } from "../../api/users";

const ManageUserCards = ({
  userData,
  setUserData,
  setHasMore,
  hasMore,
  setOpenDialog,
  setUserId,
  openDialog,
  handleCloseDialog,
  permissions,
  handleDeleteUser,
}) => {
  const navigate = useNavigate();
  const [page, setPage] = useState(2);

  const fetchMoreUser = async ({ page, search, status }) => {
    const {
      results: { data: newUser },
      pages: { currentPage, totalPages },
    } = await allUsers({ page, search, status });

    setUserData([...userData, ...newUser]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };
  return (
    <Box>
      <InfiniteScroll
        dataLength={userData?.length || 0}
        next={() => fetchMoreUser({ page })}
        hasMore={hasMore}
        loader={
          userData?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ mt: 2, display: "flex", gap: 2, flexDirection: "column" }}>
          {userData.map((user, index) => {
            const menuLists = [
              {
                name: "Edit User",
                clickHandler: () => {
                  navigate(`/manage-user/edit/${user.userId}`);
                },
              },
              {
                name: "Delete User",
                clickHandler: () => {
                  setOpenDialog(true);
                  setUserId(user.userId);
                },
              },
            ];
            return (
              <Box
                sx={{
                  boxShadow: 3,
                  p: 1.5,
                  borderRadius: "8px",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 1,
                }}
                key={index}
                // onClick={() => navigate(`/manage-user/detail/${user.userId}`)}
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {user.name}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem", color: "#757575" }}>
                    {user.role?.name} - {user.department?.name}
                  </Typography>
                </Box>

                <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                  <Box
                    sx={{
                      border: `1px solid #${user?.status?.color}`,
                      color: `#${user?.status?.color}`,
                      py: 0.5,
                      px: 1,
                      borderRadius: "50px",
                      display: "flex",
                      alignItems: "center",
                      gap: 1,
                    }}
                  >
                    <i className={user?.status?.icon} />
                    <Typography
                      sx={{
                        fontSize: "0.75rem",
                      }}
                    >
                      {user.status?.label}
                    </Typography>
                  </Box>

                  <MenuComponent menuLists={menuLists} id={user.userId} />
                </Box>
              </Box>
            );
          })}

          <CustomDialog open={openDialog} handleClose={handleCloseDialog}>
            {!permissions.includes("user.delete") ? (
              <>
                <DialogContent>
                  <DialogContentText
                    id="alert-dialog-description"
                    sx={{ color: "#000000" }}
                  >
                    You are not permitted to access this action
                  </DialogContentText>
                </DialogContent>
                <DialogActions sx={{ px: 2 }}>
                  <Button
                    sx={{
                      backgroundColor: "#78251E",
                      border: "1px solid #78251E",
                      color: "#fff",
                      textTransform: "capitalize",
                      px: 2,
                      borderRadius: "8px",
                    }}
                    onClick={() => setOpenDialog(false)}
                    autoFocus
                    size="small"
                  >
                    Close
                  </Button>
                </DialogActions>{" "}
              </>
            ) : (
              <>
                <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
                  Delete User
                </DialogTitle>
                <DialogContent>
                  <DialogContentText
                    id="alert-dialog-description"
                    sx={{ color: "#000000" }}
                  >
                    Are you sure want to delete this user?
                  </DialogContentText>
                </DialogContent>
                <DialogActions sx={{ px: 2 }}>
                  <Button
                    sx={{
                      border: "1px solid #78251E",
                      textTransform: "capitalize",
                      color: "#000000",
                      px: 2,
                      borderRadius: "8px",
                      mr: 1,
                    }}
                    size="small"
                    onClick={() => setOpenDialog(false)}
                  >
                    Cancel
                  </Button>
                  <Button
                    sx={{
                      backgroundColor: "#78251E",
                      border: "1px solid #78251E",
                      color: "#fff",
                      textTransform: "capitalize",
                      px: 2,
                      borderRadius: "8px",
                    }}
                    onClick={handleDeleteUser}
                    autoFocus
                    size="small"
                  >
                    Delete
                  </Button>
                </DialogActions>
              </>
            )}
          </CustomDialog>
        </Box>
      </InfiniteScroll>
    </Box>
  );
};

export default ManageUserCards;
