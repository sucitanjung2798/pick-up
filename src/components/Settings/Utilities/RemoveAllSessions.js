import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import VerificationDialog from "../VerificationDialog";
import { removeAllSessions } from "../../../api/utilities";
import { toastSuccess } from "../../../helpers/enums";
import ToastComponent from "../../../shared/ToastComponent";

const RemoveAllSessions = () => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [openSessions, setOpenSessions] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleDeleteAllSessions = async () => {
    try {
      setIsLoading(true);
      await removeAllSessions();
      setOpenDialog(false);
      toastSuccess("All active user sessions were removed successfully");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Remove All Sessions
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Typography>Remove all sessions</Typography>
        <Button
          sx={{
            textTransform: "capitalize",
            bgcolor: "#78251E",
            px: 2,
            color: "#fff",
            ":hover": {
              bgcolor: "#78251E",
              color: "#fff",
            },
          }}
          size="small"
          onClick={() => {
            setOpenDialog(true);
            setOpenSessions(true);
          }}
        >
          Remove
        </Button>
      </Box>

      <VerificationDialog
        open={openDialog}
        setOpen={setOpenDialog}
        openSessions={openSessions}
        isLoading={isLoading}
        handleSubmit={handleDeleteAllSessions}
      />
      <ToastComponent />
    </Box>
  );
};

export default RemoveAllSessions;
