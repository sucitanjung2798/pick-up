import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import VerificationDialog from "../VerificationDialog";
import { clearAllPosts } from "../../../api/utilities";
import { toastSuccess } from "../../../helpers/enums";
import ToastComponent from "../../../shared/ToastComponent";

const ClearAllPost = () => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [openPost, setOpenPost] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleClearAllPosts = async () => {
    try {
      setIsLoading(true);
      await clearAllPosts();
      setOpenDialog(false);
      toastSuccess("All post transactions cleared successfully");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Clear All Posts
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Typography>Clear all posts</Typography>
        <Button
          sx={{
            textTransform: "capitalize",
            bgcolor: "#78251E",
            px: 2,
            color: "#fff",
            ":hover": {
              bgcolor: "#78251E",
              color: "#fff",
            },
          }}
          size="small"
          onClick={() => {
            setOpenDialog(true);
            setOpenPost(true);
          }}
        >
          Clear
        </Button>
      </Box>

      <VerificationDialog
        open={openDialog}
        setOpen={setOpenDialog}
        openPost={openPost}
        isLoading={isLoading}
        handleSubmit={handleClearAllPosts}
      />
      <ToastComponent />
    </Box>
  );
};

export default ClearAllPost;
