import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import VerificationDialog from "../VerificationDialog";
import { databaseImport } from "../../../api/utilities";

const DatabaseImport = () => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [openImport, setOpenImport] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleDatabaseImport = async () => {
    try {
      setIsLoading(true);
      await databaseImport();
      setOpenDialog(false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Database Import
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Typography>Database import</Typography>
        <Button
          sx={{
            textTransform: "capitalize",
            bgcolor: "#78251E",
            px: 2,
            color: "#fff",
            ":hover": {
              bgcolor: "#78251E",
              color: "#fff",
            },
          }}
          size="small"
          onClick={() => {
            setOpenDialog(true);
            setOpenImport(true);
          }}
        >
          Import
        </Button>
      </Box>

      <VerificationDialog
        open={openDialog}
        setOpen={setOpenDialog}
        openImport={openImport}
        isLoading={isLoading}
        handleSubmit={handleDatabaseImport}
      />
    </Box>
  );
};

export default DatabaseImport;
