import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import VerificationDialog from "../VerificationDialog";
import { databaseExport } from "../../../api/utilities";

const DatabaseExport = () => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [openExport, setOpenExport] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleDatabaseExport = async () => {
    try {
      setIsLoading(true);
      await databaseExport();
      setOpenDialog(false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Database Export
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Typography>Database export</Typography>
        <Button
          sx={{
            textTransform: "capitalize",
            bgcolor: "#78251E",
            px: 2,
            color: "#fff",
            ":hover": {
              bgcolor: "#78251E",
              color: "#fff",
            },
          }}
          size="small"
          onClick={() => {
            setOpenDialog(true);
            setOpenExport(true);
          }}
        >
          Export
        </Button>
      </Box>

      <VerificationDialog
        open={openDialog}
        setOpen={setOpenDialog}
        openExport={openExport}
        isLoading={isLoading}
        handleSubmit={handleDatabaseExport}
      />
    </Box>
  );
};

export default DatabaseExport;
