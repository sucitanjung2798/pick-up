import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import VerificationDialog from "../VerificationDialog";
import { emailServerConnection } from "../../../api/utilities";
import { toastSuccess } from "../../../helpers/enums";
import ToastComponent from "../../../shared/ToastComponent";

const EmailServerConnection = () => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [openEmail, setOpenEmail] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleEmailConnection = async () => {
    try {
      setIsLoading(true);
      await emailServerConnection();
      setOpenDialog(false);
      toastSuccess("Connection succeed to the Email Server");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Email Server Connection
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Typography>Email server connection</Typography>
        <Button
          sx={{
            textTransform: "capitalize",
            bgcolor: "#78251E",
            px: 2,
            color: "#fff",
            ":hover": {
              bgcolor: "#78251E",
              color: "#fff",
            },
          }}
          size="small"
          onClick={() => {
            setOpenDialog(true);
            setOpenEmail(true);
          }}
        >
          Connect
        </Button>
      </Box>

      <VerificationDialog
        open={openDialog}
        setOpen={setOpenDialog}
        openEmail={openEmail}
        handleSubmit={handleEmailConnection}
        isLoading={isLoading}
      />
      <ToastComponent />
    </Box>
  );
};

export default EmailServerConnection;
