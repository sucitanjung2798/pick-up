import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import {
  ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon,
  Settings as SettingsIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
  AppSettingsAlt as AppSettingsAltIcon,
  Storage as StorageIcon,
  Email as EmailIcon,
  EmailRounded as EmailRoundedIcon,
  ManageAccountsRounded as ManageAccountsRoundedIcon,
  WebAssetRounded as WebAssetRoundedIcon,
  ImportExportRounded as ImportExportRoundedIcon,
  ClearAllRounded as ClearAllRoundedIcon,
} from "@mui/icons-material";

const Settings = () => {
  const navigate = useNavigate();

  return (
    <Box
      sx={{
        height: "100vh",
        bgcolor: "rgba(242, 243, 243, 1.00)",
        pb: 20,
      }}
    >
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate("/user-profile")}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Settings
          </Typography>
        </Box>
      </Box>

      <Box sx={{ p: 2 }}>
        <Box>
          <Typography
            sx={{ color: "rgba(3, 18, 26, 1.00)", fontSize: "0.875rem", mb: 1 }}
          >
            Configurations
          </Typography>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
            onClick={() => navigate("config/default")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <SettingsIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>Default</Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>
          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => navigate("config/apps")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <AppSettingsAltIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Applications
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>
          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => navigate("config/ftps")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <StorageIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>FTPs</Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>
          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
            }}
            onClick={() => navigate("config/email")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <EmailIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>Email</Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{ color: "rgba(3, 18, 26, 1.00)", fontSize: "0.875rem", mb: 1 }}
          >
            Utilities
          </Typography>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
            onClick={() => {
              navigate("email-connect");
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <EmailRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Email Server Connection
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => {
              navigate("ftp-connect");
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <StorageIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                FTP Server Connection
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => {
              navigate("sess-remove");
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <ManageAccountsRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Remove All Sessions
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => {
              navigate("asset-clear");
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <WebAssetRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Clear All Remote Assets
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => {
              navigate("db-import");
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <ImportExportRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Database Import
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
            }}
            onClick={() => navigate("db-export")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <ImportExportRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Database Export
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>

          <Box
            sx={{
              bgcolor: "#fff",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              p: 2,
              borderBottom: "1px solid #e0e0e0",
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
            }}
            onClick={() => navigate("db-clear")}
          >
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <ClearAllRoundedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "0.875rem" }}>
                Clear All Posts
              </Typography>
            </Box>

            <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Settings;
