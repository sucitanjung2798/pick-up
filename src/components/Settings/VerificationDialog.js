import React from "react";
import CustomDialog from "../../shared/CustomDialog";
import {
  Button,
  CircularProgress,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

const VerificationDialog = ({
  open,
  setOpen,
  openEmail,
  openFtp,
  openSessions,
  openAssets,
  openImport,
  openExport,
  openPost,
  isLoading,
  handleSubmit,
}) => {
  return (
    <CustomDialog open={open} handleClose={() => setOpen(false)}>
      <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
        {openEmail
          ? "Email Server Connection"
          : openFtp
          ? "FTP Server Connection"
          : openSessions
          ? "Remove All Sessions"
          : openAssets
          ? "Clear All Remote Assets"
          : openImport
          ? "Database Import"
          : openExport
          ? "Database Export"
          : openPost
          ? "Clear All Posts"
          : ""}
      </DialogTitle>
      <DialogContent>
        <DialogContentText
          id="alert-dialog-description"
          sx={{ color: "#000000" }}
        >
          {openEmail
            ? "Are you sure want to connect the email server?"
            : openFtp
            ? "Are you sure want to connect the ftp server?"
            : openSessions
            ? "Are you sure want to remove all active user sessions?"
            : openAssets
            ? "Are you sure want to clear all asset files in remote server?"
            : openImport
            ? "Are you sure want to import the database?"
            : openExport
            ? "Are you sure want to export the database?"
            : openPost
            ? "Are you sure want to clear all post transactions?"
            : ""}
        </DialogContentText>
      </DialogContent>
      <DialogActions sx={{ px: 2 }}>
        <Button
          sx={{
            border: "1px solid #78251E",
            textTransform: "capitalize",
            color: "#000000",
            px: 2,
            borderRadius: "8px",
            mr: 1,
          }}
          size="small"
          onClick={() => setOpen(false)}
        >
          No
        </Button>
        <Button
          sx={{
            backgroundColor: "#78251E",
            border: "1px solid #78251E",
            color: "#fff",
            textTransform: "capitalize",
            px: 2,
            borderRadius: "8px",
            display: "flex",
            gap: 1,
            alignItems: "center",
            ":hover": {
              bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
              color: "#ffffff",
              border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
            },
          }}
          autoFocus
          size="small"
          disabled={isLoading ? true : false}
          onClick={handleSubmit}
        >
          {isLoading && (
            <CircularProgress
              sx={{ color: "grey" }}
              thickness={4}
              size={20}
              disableShrink
            />
          )}
          Yes
        </Button>
      </DialogActions>
    </CustomDialog>
  );
};

export default VerificationDialog;
