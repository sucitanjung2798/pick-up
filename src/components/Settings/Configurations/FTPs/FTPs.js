import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Fab, Typography } from "@mui/material";
import {
  ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
  Add as AddIcon,
} from "@mui/icons-material";
import { viewFtps } from "../../../../api/config";
import Loading from "../../../../shared/Loading";

const FTPs = () => {
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [ftpData, setFtpData] = useState([]);

  const getViewFtp = async () => {
    try {
      setIsLoading(true);
      const { results } = await viewFtps();
      setFtpData(results);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getViewFtp();
  }, []);

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            FTPs
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2 }}>
          <Box
            sx={{
              position: "fixed",
              bottom: 80,
              right: 20,
              color: "red",
              zIndex: 10,
            }}
            onClick={() => navigate("/settings/config/default/add-new-ftp")}
          >
            <Fab size="small" sx={{ color: "#fff", bgcolor: "#78251E" }}>
              <AddIcon sx={{ fontSize: "1.5rem" }} />
            </Fab>
          </Box>
          <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
            {ftpData?.map((item, index) => (
              <Box
                sx={{
                  bgcolor: "#fff",
                  borderRadius: "8px",
                  px: 2,
                  py: 1,
                  boxShadow: 3,
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 2,
                }}
                key={index}
                onClick={() => navigate(`/settings/config/ftps/${item?.ftpId}`)}
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {item?.username}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item?.hostname}
                  </Typography>
                </Box>

                <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
              </Box>
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default FTPs;
