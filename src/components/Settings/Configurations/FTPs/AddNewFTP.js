import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Typography,
} from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import CustomInput from "../../../../shared/CustomInput";
import CustomSwitch from "../../../../shared/CustomSwitch";
import CustomButtonSave from "../../../../shared/CustomButtonSave";
import { addNewFtp } from "../../../../api/config";

const AddNewFTP = () => {
  const navigate = useNavigate();

  const [hostname, setHostname] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [port, setPort] = useState("");
  const [path, setPath] = useState("");
  const [remotePath, setRemotePath] = useState("");
  const [status, setStatus] = useState(false);

  const [isHostnameError, setIsHostnameError] = useState(false);
  const [isUsernameError, setIsUsernameError] = useState(false);
  const [isPasswordError, setIsPasswordError] = useState(false);
  const [isPortError, setIsPortError] = useState(false);
  const [isPathError, setIsPathError] = useState(false);
  const [isRemotePathError, setIsRemotePathError] = useState(false);
  const [isPortLength, setIsPortLength] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      hostname !== "" &&
      username !== "" &&
      password !== "" &&
      port !== "" &&
      path !== "" &&
      remotePath !== "" &&
      port > 0 &&
      port < 65535
    ) {
      try {
        setIsLoading(true);
        const data = {
          hostname,
          username,
          password,
          port,
          path,
          remotePath,
          status: status === true ? 1 : 0,
        };
        await addNewFtp(data);
        navigate("/settings/config/ftps");
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    } else {
      hostname !== "" ? setIsHostnameError(false) : setIsHostnameError(true);
      username !== "" ? setIsUsernameError(false) : setIsUsernameError(true);
      password !== "" ? setIsPasswordError(false) : setIsPasswordError(true);
      port !== "" ? setIsPortError(false) : setIsPortError(true);
      path !== "" ? setIsPathError(false) : setIsPathError(true);
      remotePath !== ""
        ? setIsRemotePathError(false)
        : setIsRemotePathError(true);
      port > 0 && port < 65535 ? setIsPortLength(false) : setIsPortLength(true);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Add New FTP
          </Typography>
        </Box>
      </Box>

      <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
        <Box>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Hostname <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={hostname}
            placeholder="Enter the hostname"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isHostnameError}
            isHelperText={true}
            onChange={(e) => {
              setHostname(e.target.value);

              if (hostname) {
                setIsHostnameError(false);
              }
            }}
            helperText={
              isHostnameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the hostname
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Username <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={username}
            placeholder="Enter the username"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isUsernameError}
            isHelperText={true}
            onChange={(e) => {
              setUsername(e.target.value);

              if (username) {
                setIsUsernameError(false);
              }
            }}
            helperText={
              isUsernameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the username
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Password <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={password}
            placeholder="Enter the password"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isPasswordError}
            isHelperText={true}
            onChange={(e) => {
              setPassword(e.target.value);

              if (password) {
                setIsPasswordError(false);
              }
            }}
            helperText={
              isPasswordError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the password
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Port <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={port}
            placeholder="Enter the port"
            sx={{
              fontSize: "0.875rem",
              "& .MuiOutlinedInput-root": {
                borderColor: "yellow",
              },
            }}
            isValueError={isPortError}
            type="number"
            isHelperText={true}
            onChange={(e) => {
              setPort(e.target.value);

              if (port) {
                setIsPortError(false);
              } else if (port > 0 && port < 65535) {
                setIsPortLength(false);
              }
            }}
            helperText={
              isPortError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the port
                </FormHelperText>
              ) : isPortLength ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The FTP Port field must contain a number greater than or equal
                  to 0
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Path <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={path}
            placeholder="Enter the path"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isPathError}
            isHelperText={true}
            onChange={(e) => {
              setPath(e.target.value);

              if (path) {
                setIsPathError(false);
              }
            }}
            helperText={
              isPathError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the path
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Remote Path <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={remotePath}
            placeholder="Enter the remote path"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isRemotePathError}
            isHelperText={true}
            onChange={(e) => {
              setRemotePath(e.target.value);

              if (remotePath) {
                setIsRemotePathError(false);
              }
            }}
            helperText={
              isRemotePathError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the remote path
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box
          sx={{
            mt: 2,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography>Status</Typography>
          <FormGroup
            sx={{
              "& .MuiFormControlLabel-root": {
                marginRight: 0,
              },
            }}
          >
            <FormControlLabel
              labelPlacement="end"
              label={
                status === true ? (
                  <Typography sx={{ fontSize: "14px", ml: 1 }}>
                    Active
                  </Typography>
                ) : (
                  <Typography sx={{ fontSize: "14px", ml: 1 }}>
                    Non Active
                  </Typography>
                )
              }
              control={<CustomSwitch value={status} setValue={setStatus} />}
            />
          </FormGroup>
        </Box>

        <CustomButtonSave content="Save" isLoading={isLoading} />
      </Box>
    </Box>
  );
};

export default AddNewFTP;
