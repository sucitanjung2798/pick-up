import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import { deleteFtp, showFtp } from "../../../../api/config";
import Loading from "../../../../shared/Loading";
import DeleteModalVerification from "../Default/DeleteModalVerification";

const FTPsDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [ftpData, setFtpData] = useState(null);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const getShowFtp = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showFtp(id);
      setFtpData(result);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowFtp(id);
  }, [id]);

  const handleDelete = async () => {
    try {
      setIsLoadingDelete(true);

      await deleteFtp(id);
      setOpenDialogDelete(false);
      navigate("/settings/config/ftps");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoadingDelete(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate("/settings/config/ftps")}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            FTP Detail
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 0.5 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Hostname</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.hostname || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Username</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.username || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Password</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.password || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Port</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.port || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Path</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.path || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Remote Path</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {ftpData?.remotePath || "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
                color: `#${ftpData?.status?.color}`,
              }}
            >
              {ftpData?.status?.label || "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
              mt: 4,
            }}
          >
            <Button
              sx={{
                border: "1px solid #78251E",
                px: 2,
                width: "100%",
                borderRadius: "8px",
                color: "#000000",
                textTransform: "capitalize",
              }}
              onClick={() => navigate(`/settings/config/ftps/edit/${id}`)}
            >
              Edit
            </Button>
            <Button
              sx={{
                width: "100%",
                bgcolor: "#78251E",
                border: "1px solid #78251E",
                borderRadius: "8px",
                ":hover": {
                  bgcolor: "#78251E",
                  border: "1px solid #78251E",
                },
                color: "#fff",
                textTransform: "capitalize",
              }}
              onClick={() => setOpenDialogDelete(true)}
            >
              Delete
            </Button>
          </Box>
        </Box>
      )}

      <DeleteModalVerification
        open={openDialogDelete}
        setOpen={setOpenDialogDelete}
        handleDelete={handleDelete}
        isLoading={isLoadingDelete}
      />
    </Box>
  );
};

export default FTPsDetail;
