import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  MenuItem,
  Typography,
} from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import CustomInput from "../../../../shared/CustomInput";
import CustomSelect from "../../../../shared/CustomSelect";
import CustomSwitch from "../../../../shared/CustomSwitch";
import CustomButtonSave from "../../../../shared/CustomButtonSave";
import { editApp, showApp } from "../../../../api/config";
import Loading from "../../../../shared/Loading";

const AppEdit = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [showDataLimit, setShowDataLimit] = useState("");
  const [trackingPrefix, setTrackingPrefix] = useState("");
  const [randomChars, setRandomChars] = useState("");
  const [emailNotif, setEmailNotif] = useState(false);
  const [status, setStatus] = useState(false);

  const [isShowDataLimitError, setIsShowDataLimitError] = useState(false);
  const [isShowDataLimitLength, setIsShowDataLimitLength] = useState(false);
  const [isTrackingPrefixError, setIsTrackingPrefixError] = useState(false);
  const [isRandomCharsError, setIsRandomCharsError] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingSave, setIsLoadingSave] = useState(false);

  const getShowApp = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showApp(id);
      setShowDataLimit(result.showDataLimit);
      setTrackingPrefix(result.trackingPrefix);
      setRandomChars(result.randomChars);
      setEmailNotif(result.emailNotification);
      setStatus(result.status.value === 1 ? true : false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowApp(id);
  }, [id]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (showDataLimit !== "" && trackingPrefix !== "" && randomChars !== "") {
      setIsLoadingSave(true);
      try {
        const data = {
          showDataLimit,
          trackingPrefix: trackingPrefix.toUpperCase(),
          randomChars,
          emailNotification: emailNotif === true ? 1 : 0,
          status: status === true ? 1 : 0,
        };
        await editApp(id, data);
        navigate("/settings/config/apps");
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoadingSave(false);
      }
    } else {
      showDataLimit !== ""
        ? setIsShowDataLimitError(false)
        : setIsShowDataLimitError(true);
      trackingPrefix !== ""
        ? setIsTrackingPrefixError(false)
        : setIsTrackingPrefixError(true);
      randomChars !== ""
        ? setIsRandomCharsError(false)
        : setIsRandomCharsError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate(`/settings/config/apps/${id}`)}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Edit Application
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Show Data Limit <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={showDataLimit}
              placeholder="Enter the limit"
              sx={{
                fontSize: "0.875rem",
                "& .MuiOutlinedInput-root": {
                  borderColor: "yellow",
                },
              }}
              isValueError={isShowDataLimitError}
              type="number"
              isHelperText={true}
              onChange={(e) => {
                setShowDataLimit(e.target.value);

                if (showDataLimit) {
                  setIsShowDataLimitError(false);
                } else if (showDataLimit > 0 && showDataLimit < 256) {
                  setIsShowDataLimitLength(false);
                }
              }}
              helperText={
                isShowDataLimitError ? (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the limit
                  </FormHelperText>
                ) : isShowDataLimitLength ? (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    The Limit field must contain a number greater than or equal
                    to 0
                  </FormHelperText>
                ) : (
                  ""
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Tracking Prefix <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={trackingPrefix}
              placeholder="Enter the tracking prefix"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isTrackingPrefixError}
              isHelperText={true}
              inputProps={{ style: { textTransform: "uppercase" } }}
              onChange={(e) => {
                setTrackingPrefix(e.target.value);

                if (trackingPrefix) {
                  setIsTrackingPrefixError(false);
                }
              }}
              helperText={
                isTrackingPrefixError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the tracking prefix
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Random Chars <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <CustomSelect
              value={randomChars || ""}
              onChange={(e) => {
                setRandomChars(e.target.value);

                if (randomChars) {
                  setIsRandomCharsError(false);
                }
              }}
              isValueError={isRandomCharsError}
              data={[
                { value: "numeric", label: "Numeric" },
                { value: "alphabeth", label: "Alphabeth" },
                { value: "alphanumeric", label: "Alphanumeric" },
              ].map((option, index) => (
                <MenuItem
                  key={index}
                  value={option.value}
                  sx={{ fontSize: "0.875rem" }}
                >
                  {option.label}
                </MenuItem>
              ))}
              isHelperText={true}
              helperText={
                isRandomCharsError === true ? (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please select the random chars
                  </FormHelperText>
                ) : (
                  ""
                )
              }
              placeholder="Please select the random chars"
            />
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Email Notification</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                control={
                  <CustomSwitch value={emailNotif} setValue={setEmailNotif} />
                }
              />
            </FormGroup>
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Status</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                control={<CustomSwitch value={status} setValue={setStatus} />}
              />
            </FormGroup>
          </Box>

          <CustomButtonSave content="Save" isLoading={isLoadingSave} />
        </Box>
      )}
    </Box>
  );
};

export default AppEdit;
