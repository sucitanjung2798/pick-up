import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import { deleteApp, showApp } from "../../../../api/config";
import Loading from "../../../../shared/Loading";
import dayjs from "dayjs";
import DeleteModalVerification from "../Default/DeleteModalVerification";

const ApplicationDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [appData, setAppData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const getShowApp = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showApp(id);
      setAppData(result);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowApp(id);
  }, [id]);

  const handleDelete = async () => {
    try {
      setIsLoadingDelete(true);
      await deleteApp(id);
      navigate("/settings/config/apps");
      setOpenDialogDelete(false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate("/settings/config/apps")}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Application Detail
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 0.5 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Show Data Limit
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {appData?.showDataLimit || "0"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Tracking Prefix
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {appData?.trackingPrefix || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Random Chars</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {appData?.randomChars || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Email Notification
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {appData?.emailNotification === true ? "true" : "false"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
                color: `#${appData?.status?.color}`,
              }}
            >
              {appData?.status?.label || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Created At</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
              }}
            >
              {dayjs(appData?.createdAt?.date).format("DD-MM-YYYY") || "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
              mt: 4,
            }}
          >
            <Button
              sx={{
                border: "1px solid #78251E",
                px: 2,
                width: "100%",
                borderRadius: "8px",
                color: "#000000",
                textTransform: "capitalize",
              }}
              onClick={() => navigate(`/settings/config/app/edit/${id}`)}
            >
              Edit
            </Button>
            <Button
              sx={{
                width: "100%",
                bgcolor: "#78251E",
                border: "1px solid #78251E",
                borderRadius: "8px",
                ":hover": {
                  bgcolor: "#78251E",
                  border: "1px solid #78251E",
                },
                color: "#fff",
                textTransform: "capitalize",
              }}
              onClick={() => setOpenDialogDelete(true)}
            >
              Delete
            </Button>
          </Box>
        </Box>
      )}

      <DeleteModalVerification
        open={openDialogDelete}
        setOpen={setOpenDialogDelete}
        handleDelete={handleDelete}
        isLoading={isLoadingDelete}
      />
    </Box>
  );
};

export default ApplicationDetail;
