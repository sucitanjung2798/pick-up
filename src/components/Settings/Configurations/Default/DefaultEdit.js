import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Typography,
} from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import CustomInput from "../../../../shared/CustomInput";
import dayjs from "dayjs";
import CustomDescriptionField from "../../../../shared/CustomDescriptionField";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { MobileDatePicker } from "@mui/x-date-pickers";
import CustomSwitch from "../../../../shared/CustomSwitch";
import CustomButtonSave from "../../../../shared/CustomButtonSave";
import { editConfigDefault, showConfigDefault } from "../../../../api/config";
import Loading from "../../../../shared/Loading";

const DefaultEdit = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const dateToday = dayjs().format("YYYY/MM/DD");
  const thisYear = dayjs().format("YYYY");

  const [isLoading, setIsLoading] = useState(false);

  const [title, setTitle] = useState("");
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [keywords, setKeywords] = useState("");
  const [description, setDescription] = useState("");
  const [version, setVersion] = useState("");
  const [versionDate, setVersionDate] = useState(dateToday);
  const [copyright, setCopyright] = useState("");
  const [year, setYear] = useState(thisYear);
  const [status, setStatus] = useState(false);

  const [isTitleError, setIsTitleError] = useState(false);
  const [isNameError, setIsNameError] = useState(false);
  const [isVersionError, setIsVersionError] = useState(false);
  const [isCopyrightError, setIsCopyrightError] = useState(false);

  const getShowConfigDefault = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showConfigDefault(id);
      setTitle(result.title);
      setName(result.name);
      setAuthor(result.author);
      setKeywords(result.keywords);
      setDescription(result.description);
      setVersion(result.version);
      setVersionDate(result.versionDate);
      setCopyright(result.copyright);
      setYear(result.year);
      setStatus(result.status.value === 1 ? true : false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowConfigDefault(id);
  }, [id]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (title !== "" && name !== "" && version !== "" && copyright !== "") {
      try {
        setIsLoading(true);
        const data = {
          title,
          name,
          author,
          keywords,
          description,
          version,
          versionDate: dayjs(versionDate).format("YYYY-MM-DD"),
          copyright,
          year,
          status: status === true ? 1 : 0,
        };
        await editConfigDefault(id, data);
        navigate("/settings/config/default");
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    } else {
      title !== "" ? setIsTitleError(false) : setIsTitleError(true);
      name !== "" ? setIsNameError(false) : setIsNameError(true);
      version !== "" ? setIsVersionError(false) : setIsVersionError(true);
      copyright !== "" ? setIsCopyrightError(false) : setIsCopyrightError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate(`/settings/config/default/${id}`)}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Default Edit
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
          <Box>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Title <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={title}
              placeholder="Enter the title"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isTitleError}
              isHelperText={true}
              onChange={(e) => {
                setTitle(e.target.value);

                if (title) {
                  setIsTitleError(false);
                }
              }}
              helperText={
                isTitleError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the title
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={name}
              placeholder="Enter the name"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isNameError}
              isHelperText={true}
              onChange={(e) => {
                setName(e.target.value);

                if (name) {
                  setIsNameError(false);
                }
              }}
              helperText={
                isNameError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the name
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>Author</Typography>
            <CustomInput
              value={author}
              placeholder="Enter the author"
              sx={{ fontSize: "0.875rem" }}
              onChange={(e) => {
                setAuthor(e.target.value);
              }}
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>Keywords</Typography>
            <CustomInput
              value={keywords}
              placeholder="Enter the keywords"
              sx={{ fontSize: "0.875rem" }}
              onChange={(e) => {
                setKeywords(e.target.value);
              }}
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>Description</Typography>
            <CustomDescriptionField
              setValue={setDescription}
              value={description}
              placeholder="Input the description"
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Version <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={version}
              placeholder="Enter the version"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isVersionError}
              isHelperText={true}
              onChange={(e) => {
                setVersion(e.target.value);

                if (version) {
                  setIsVersionError(false);
                }
              }}
              helperText={
                isVersionError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the version
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Version Date <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <DemoContainer
              sx={{
                "& .MuiStack-root": {
                  overflow: "hidden !important",
                },
              }}
              components={["MobileDatePicker"]}
            >
              <DemoItem>
                <MobileDatePicker
                  fullWidth
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      height: "44px",
                      borderRadius: "8px",
                      fontSize: "0.875rem",
                    },
                  }}
                  value={dayjs(versionDate)}
                  onChange={(newValue) => {
                    const formattedDate = newValue
                      ? dayjs(newValue).format("MM/DD/YYYY")
                      : "";
                    setVersionDate(formattedDate);
                  }}
                />
              </DemoItem>
            </DemoContainer>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Copyright <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <CustomInput
              value={copyright}
              placeholder="Enter the copyright"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isCopyrightError}
              isHelperText={true}
              onChange={(e) => {
                setCopyright(e.target.value);

                if (copyright) {
                  setIsCopyrightError(false);
                }
              }}
              helperText={
                isCopyrightError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the copyright
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Year <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <DemoContainer
              sx={{
                "& .MuiStack-root": {
                  overflow: "hidden !important",
                },
              }}
              components={["MobileDatePicker"]}
            >
              <DemoItem>
                <MobileDatePicker
                  fullWidth
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      height: "44px",
                      borderRadius: "8px",
                      fontSize: "0.875rem",
                    },
                  }}
                  value={dayjs(year)}
                  onChange={(newValue) => {
                    const formattedDate = newValue
                      ? dayjs(newValue).format("YYYY")
                      : "";
                    setYear(formattedDate);
                  }}
                  openTo="year"
                  views={["year"]}
                />
              </DemoItem>
            </DemoContainer>
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Status</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                label={
                  status === true ? (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Active
                    </Typography>
                  ) : (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Non Active
                    </Typography>
                  )
                }
                control={<CustomSwitch value={status} setValue={setStatus} />}
              />
            </FormGroup>
          </Box>

          <CustomButtonSave content="Save" isLoading={isLoading} />
        </Box>
      )}
    </Box>
  );
};

export default DefaultEdit;
