import React from "react";
import {
  Button,
  CircularProgress,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import CustomDialog from "../../../../shared/CustomDialog";

const DeleteModalVerification = ({
  open,
  setOpen,
  handleDelete,
  isLoading,
}) => {
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <CustomDialog open={open} handleClose={handleClose}>
      <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
        Delete Default Config
      </DialogTitle>
      <DialogContent>
        <DialogContentText
          id="alert-dialog-description"
          sx={{ color: "#000000" }}
        >
          Are you sure want to delete this config?
        </DialogContentText>
      </DialogContent>
      <DialogActions sx={{ px: 2 }}>
        <Button
          sx={{
            border: "1px solid #78251E",
            textTransform: "capitalize",
            color: "#000000",
            px: 2,
            borderRadius: "8px",
            mr: 1,
          }}
          size="small"
          onClick={() => setOpen(false)}
        >
          Cancel
        </Button>
        <Button
          sx={{
            backgroundColor: "#78251E",
            border: "1px solid #78251E",
            color: "#fff",
            textTransform: "capitalize",
            px: 2,
            borderRadius: "8px",
            display: "flex",
            gap: 1,
            alignItems: "center",
            ":hover": {
              bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
              color: "#ffffff",
              border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
            },
          }}
          onClick={handleDelete}
          autoFocus
          size="small"
          disabled={isLoading ? true : false}
        >
          {isLoading && (
            <CircularProgress
              sx={{ color: "grey" }}
              thickness={4}
              size={20}
              disableShrink
            />
          )}
          Delete
        </Button>
      </DialogActions>
    </CustomDialog>
  );
};

export default DeleteModalVerification;
