import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import { deleteConfigDefault, showConfigDefault } from "../../../../api/config";
import Loading from "../../../../shared/Loading";
import dayjs from "dayjs";
import DeleteModalVerification from "./DeleteModalVerification";

const DefaultDetail = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [configDefaultData, setConfigDefaultData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const getShowConfigDefault = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showConfigDefault(id);
      setConfigDefaultData(result);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowConfigDefault(id);
  }, [id]);

  const handleDelete = async () => {
    try {
      setIsLoadingDelete(true);
      await deleteConfigDefault(id);
      setOpenDialogDelete(false);
      navigate("/settings/config/default");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate("/settings/config/default")}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Default Detail
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 0.5 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Title</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.title || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Name</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.name || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Author</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.author || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Keywords</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.keywords || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Description</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.description || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Version</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.version || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Version Date</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.versionDate || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Copyright</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.copyright || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Year</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {configDefaultData?.year || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
                color: `#${configDefaultData?.status?.color}`,
              }}
            >
              {configDefaultData?.status?.label || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Created At</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
              }}
            >
              {dayjs(configDefaultData?.createdAt?.date).format("DD-MM-YYYY") ||
                "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
              mt: 4,
            }}
          >
            <Button
              sx={{
                border: "1px solid #78251E",
                px: 2,
                width: "100%",
                borderRadius: "8px",
                color: "#000000",
                textTransform: "capitalize",
              }}
              onClick={() => navigate(`/settings/config/default/edit/${id}`)}
            >
              Edit
            </Button>
            <Button
              sx={{
                width: "100%",
                bgcolor: "#78251E",
                border: "1px solid #78251E",
                borderRadius: "8px",
                ":hover": {
                  bgcolor: "#78251E",
                  border: "1px solid #78251E",
                },
                color: "#fff",
                textTransform: "capitalize",
              }}
              onClick={() => setOpenDialogDelete(true)}
            >
              Delete
            </Button>
          </Box>
        </Box>
      )}

      <DeleteModalVerification
        open={openDialogDelete}
        setOpen={setOpenDialogDelete}
        handleDelete={handleDelete}
        isLoading={isLoadingDelete}
      />
    </Box>
  );
};

export default DefaultDetail;
