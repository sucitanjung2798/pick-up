import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  MenuItem,
  Typography,
} from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import CustomInput from "../../../../shared/CustomInput";
import CustomSelect from "../../../../shared/CustomSelect";
import CustomSwitch from "../../../../shared/CustomSwitch";
import CustomButtonSave from "../../../../shared/CustomButtonSave";
import { priorityLevel } from "../../../../api/enum";
import { addNewEmail } from "../../../../api/config";

const AddNewEmail = () => {
  const navigate = useNavigate();

  const [userAgent, setUserAgent] = useState("");
  const [charset, setCharset] = useState("");
  const [protocol, setProtocol] = useState("smtp");
  const [mailPath, setMailPath] = useState("");
  const [priority, setPriority] = useState(`3`);
  const [smtpHost, setSmtpHost] = useState("");
  const [smtpUser, setSmtpUser] = useState("");
  const [smtpPass, setSmtpPass] = useState("");
  const [smtpPort, setSmtpPort] = useState("");
  const [smtpTimeout, setSmtpTimeout] = useState("");
  const [smtpCrypto, setSmtpCrypto] = useState("ssl");
  const [mailType, setMailType] = useState("");
  const [wordWrap, setWordWrap] = useState(false);
  const [wrapChars, setWrapChars] = useState(80);
  const [emailFrom, setEmailFrom] = useState("");
  const [emailFromName, setEmailFromName] = useState("");
  const [emailReplyTo, setEmailReplyTo] = useState("");
  const [emailReplyToName, setEmailReplyToName] = useState("");
  const [status, setStatus] = useState(false);

  const [isUserAgentError, setIsUserAgentError] = useState(false);
  const [isCharsetError, setIsCharsetError] = useState(false);
  const [isProtocolError, setIsProtocolError] = useState(false);
  const [isPriorityError, setIsPriorityError] = useState(false);
  const [isSmtpUserError, setIsSmtpUserError] = useState(false);
  const [isSmtpPassError, setIsSmtpPassError] = useState(false);
  const [isSmtpPortError, setIsSmtpPortError] = useState(false);
  const [isSmtpTimeoutError, setIsSmtpTimeoutError] = useState(false);
  const [isSmtpCryptoError, setIsSmtpCryptoError] = useState(false);
  const [isMailTypeError, setIsMailTypeError] = useState(false);
  const [isWrapCharsError, setIsWrapCharsError] = useState(false);
  const [isEmailFromError, setIsEmailFromError] = useState(false);
  const [isEmailFromNameError, setIsEmailFromNameError] = useState(false);
  const [isEmailReplyToError, setIsEmailReplyToError] = useState(false);
  const [isEmailReplyToNameError, setIsEmailReplyToNameError] = useState(false);

  const [isEmailFromValid, setIsEmailFromValid] = useState(false);
  const [isEmailReplyToValid, setIsEmailReplyToValid] = useState(false);
  const [isSmtpUserValid, setIsSmtpUserValid] = useState(false);
  const [isSmtpTimeoutLength, setIsSmtpTimeoutLength] = useState(false);
  const [isSmtpPortLength, setIsSmtpPortLength] = useState(false);
  const [isWrapCharsLength, setIsWrapCharsLength] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);

  const [priorityData, setPriorityData] = useState([]);

  const emailValid = (value) => {
    return /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/.test(value);
  };

  const getPriorityLevel = async () => {
    try {
      const { results } = await priorityLevel();
      setPriorityData(results);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getPriorityLevel();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      userAgent !== "" &&
      charset !== "" &&
      protocol !== "" &&
      priority !== "" &&
      smtpHost !== ""
        ? smtpUser !== "" &&
          smtpPass !== "" &&
          smtpPort !== "" &&
          smtpTimeout !== "" &&
          emailValid(smtpUser) &&
          smtpTimeout > 0 &&
          smtpTimeout < 1000 &&
          smtpPort > 0 &&
          smtpPort < 65535
        : null &&
          smtpCrypto !== "" &&
          mailType !== "" &&
          wrapChars !== "" &&
          emailFrom !== "" &&
          emailFromName !== "" &&
          emailReplyTo !== "" &&
          emailReplyToName !== "" &&
          emailValid(emailFrom) &&
          emailValid(emailReplyTo) &&
          wrapChars > 0 &&
          wrapChars < 100
    ) {
      try {
        setIsLoading(true);
        const data = {
          userAgent,
          charset,
          protocol,
          mailPath,
          priority,
          smtpHost,
          smtpUser,
          smtpPass,
          smtpPort,
          smtpTimeout,
          smtpCrypto,
          mailType,
          wordWrap: wordWrap === true ? 1 : 0,
          wrapChars,
          emailFrom,
          emailFromName,
          emailReplyTo,
          emailReplyToName,
          status: status === true ? 1 : 0,
        };
        await addNewEmail(data);
        navigate("/settings/config/email");
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    } else {
      userAgent !== "" ? setIsUserAgentError(false) : setIsUserAgentError(true);
      charset !== "" ? setIsCharsetError(false) : setIsCharsetError(true);
      protocol !== "" ? setIsProtocolError(false) : setIsProtocolError(true);
      priority !== "" ? setIsPriorityError(false) : setIsPriorityError(true);
      smtpUser !== "" ? setIsSmtpUserError(false) : setIsSmtpUserError(true);
      smtpPass !== "" ? setIsSmtpPassError(false) : setIsSmtpPassError(true);
      smtpPort !== "" ? setIsSmtpPortError(false) : setIsSmtpPortError(true);
      smtpTimeout !== ""
        ? setIsSmtpTimeoutError(false)
        : setIsSmtpTimeoutError(true);
      smtpCrypto !== ""
        ? setIsSmtpCryptoError(false)
        : setIsSmtpCryptoError(true);
      mailType !== "" ? setIsMailTypeError(false) : setIsMailTypeError(true);
      wrapChars !== "" ? setIsWrapCharsError(false) : setIsWrapCharsError(true);
      emailFrom !== "" ? setIsEmailFromError(false) : setIsEmailFromError(true);
      emailFromName !== ""
        ? setIsEmailFromNameError(false)
        : setIsEmailFromNameError(true);
      emailReplyTo !== ""
        ? setIsEmailReplyToError(false)
        : setIsEmailReplyToError(true);
      emailReplyToName !== ""
        ? setIsEmailReplyToNameError(false)
        : setIsEmailReplyToNameError(true);
      emailValid(emailFrom)
        ? setIsEmailFromValid(false)
        : setIsEmailFromValid(true);
      emailValid(emailReplyTo)
        ? setIsEmailReplyToValid(false)
        : setIsEmailReplyToValid(true);
      emailValid(smtpUser)
        ? setIsSmtpUserValid(false)
        : setIsSmtpUserValid(true);
      smtpTimeout > 0 && smtpTimeout < 1000
        ? setIsSmtpTimeoutLength(false)
        : setIsSmtpTimeoutLength(true);
      smtpPort > 0 && smtpPort < 65535
        ? setIsSmtpPortLength(false)
        : setIsSmtpPortLength(true);
      wrapChars > 0 && wrapChars < 100
        ? setIsWrapCharsLength(false)
        : setIsWrapCharsLength(true);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Add New Email
          </Typography>
        </Box>
      </Box>

      <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            User Agent <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={userAgent}
            placeholder="Enter the user agent"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isUserAgentError}
            isHelperText={true}
            onChange={(e) => {
              setUserAgent(e.target.value);

              if (userAgent) {
                setIsUserAgentError(false);
              }
            }}
            helperText={
              isUserAgentError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the user agent
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Charset <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={charset}
            placeholder="Enter the charset"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isCharsetError}
            isHelperText={true}
            onChange={(e) => {
              setCharset(e.target.value);

              if (charset) {
                setIsCharsetError(false);
              }
            }}
            helperText={
              isCharsetError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the charset
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Protocol <span style={{ color: "#B80000" }}>*</span>
          </Typography>

          <CustomSelect
            value={protocol || ""}
            onChange={(e) => {
              setProtocol(e.target.value);

              if (protocol) {
                setIsProtocolError(false);
              }
            }}
            isValueError={isProtocolError}
            data={[
              { value: "mail", label: "Mail" },
              { value: "sendmail", label: "Sendmail" },
              { value: "smtp", label: "Smtp" },
            ].map((option, index) => (
              <MenuItem
                key={index}
                value={option.value}
                sx={{ fontSize: "0.875rem" }}
              >
                {option.label}
              </MenuItem>
            ))}
            isHelperText={true}
            helperText={
              isProtocolError === true ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the protocol
                </FormHelperText>
              ) : (
                ""
              )
            }
            placeholder="Please select the protocol"
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>Mail Path</Typography>
          <CustomInput
            value={mailPath}
            placeholder="Enter the mail path"
            sx={{ fontSize: "0.875rem" }}
            onChange={(e) => {
              setMailPath(e.target.value);
            }}
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Priority <span style={{ color: "#B80000" }}>*</span>
          </Typography>

          <CustomSelect
            value={priority || ""}
            onChange={(e) => {
              setPriority(e.target.value);

              if (priority) {
                setIsPriorityError(false);
              }
            }}
            isValueError={isPriorityError}
            data={priorityData?.map((option, index) => (
              <MenuItem
                key={index}
                value={option.value}
                sx={{ fontSize: "0.875rem" }}
              >
                {option.label}
              </MenuItem>
            ))}
            isHelperText={true}
            helperText={
              isPriorityError === true ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the priority
                </FormHelperText>
              ) : (
                ""
              )
            }
            placeholder="Please select the priority"
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>Smtp Host</Typography>
          <CustomInput
            value={smtpHost}
            placeholder="Enter the smtp host"
            sx={{ fontSize: "0.875rem" }}
            onChange={(e) => {
              setSmtpHost(e.target.value);

              if (e.target.value) {
                setIsDisabled(false);
                setSmtpTimeout(30);
              } else {
                setIsDisabled(true);
                setSmtpUser("");
                setSmtpPass("");
                setSmtpPort("");
                setSmtpTimeout("");
                setIsSmtpUserError(false);
                setIsSmtpPassError(false);
                setIsSmtpPortError(false);
                setIsSmtpTimeoutError(false);
                setIsSmtpUserValid(false);
                setIsSmtpPortLength(false);
              }
            }}
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Smtp User <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={smtpUser}
            placeholder="Enter the smtp user"
            sx={{ fontSize: "0.875rem" }}
            disabled={isDisabled}
            isValueError={isSmtpUserError}
            isHelperText={true}
            onChange={(e) => {
              setSmtpUser(e.target.value);

              if (emailValid(smtpUser)) {
                setIsSmtpUserValid(false);
              } else {
                setIsSmtpUserValid(true);
              }

              if (smtpUser) {
                setIsSmtpUserError(false);
              }
            }}
            helperText={
              isSmtpUserError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The SMTP User field is required when SMTP Hostname is present
                </FormHelperText>
              ) : isSmtpUserValid ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the valid email
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Smtp Pass <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={smtpPass}
            placeholder="Enter the smtp pass"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isSmtpPassError}
            disabled={isDisabled}
            isHelperText={true}
            onChange={(e) => {
              setSmtpPass(e.target.value);

              if (smtpPass) {
                setIsSmtpPassError(false);
              }
            }}
            helperText={
              isSmtpPassError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The SMTP Password field is required when SMTP Hostname is
                  present
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Smtp Port <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={smtpPort}
            placeholder="Enter the smtp port"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isSmtpPortError}
            disabled={isDisabled}
            type="number"
            isHelperText={true}
            onChange={(e) => {
              setSmtpPort(e.target.value);

              if (smtpPort > 0 && smtpPort < 65535) {
                setIsSmtpPortLength(false);
              }

              if (smtpPort) {
                setIsSmtpPortError(false);
              }
            }}
            helperText={
              isSmtpPortError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The SMTP Port field is required when SMTP Hostname is present
                </FormHelperText>
              ) : isSmtpPortLength ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The Smtp Port must contain a number greater than or equal to 0
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Smtp Timeout <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={smtpTimeout}
            placeholder="Enter the smtp timeout"
            disabled={isDisabled}
            sx={{ fontSize: "0.875rem" }}
            isValueError={isSmtpTimeoutError}
            type="number"
            isHelperText={true}
            onChange={(e) => {
              setSmtpTimeout(e.target.value);

              if (smtpTimeout) {
                setIsSmtpTimeoutError(false);
              }
            }}
            helperText={
              isSmtpTimeoutError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The SMTP Timeout field is required when SMTP Hostname is
                  present
                </FormHelperText>
              ) : isSmtpTimeoutLength ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The Smtp Timeout must contain a number greater than or equal
                  to 0
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Smtp Crypto <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomSelect
            value={smtpCrypto || ""}
            onChange={(e) => {
              setSmtpCrypto(e.target.value);

              if (smtpCrypto) {
                setIsSmtpCryptoError(false);
              }
            }}
            isValueError={isSmtpCryptoError}
            data={["tsl", "ssl"].map((option, index) => (
              <MenuItem
                key={index}
                value={option}
                sx={{ fontSize: "0.875rem" }}
              >
                {option}
              </MenuItem>
            ))}
            isHelperText={true}
            helperText={
              isSmtpCryptoError === true ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the smtp crypto
                </FormHelperText>
              ) : (
                ""
              )
            }
            placeholder="Please select the smtp crypto"
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Mail Type <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={mailType}
            placeholder="Enter the mail type"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isMailTypeError}
            isHelperText={true}
            onChange={(e) => {
              setMailType(e.target.value);

              if (mailType) {
                setIsMailTypeError(false);
              }
            }}
            helperText={
              isMailTypeError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the mail type
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Wrap Chars <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={wrapChars}
            placeholder="Enter the wrap chars"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isWrapCharsError}
            type="number"
            isHelperText={true}
            onChange={(e) => {
              setWrapChars(e.target.value);

              if (wrapChars) {
                setIsWrapCharsError(false);
              } else if (wrapChars > 0 && wrapChars > 100) {
                setIsWrapCharsLength(false);
              }
            }}
            helperText={
              isWrapCharsError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the wrap chars
                </FormHelperText>
              ) : isWrapCharsLength ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  The Wrap Chars must contain a number greater than or equal to
                  0
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Email From <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={emailFrom}
            placeholder="Enter the email from"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isEmailFromError}
            isHelperText={true}
            onChange={(e) => {
              setEmailFrom(e.target.value);

              if (emailValid(emailFrom)) {
                setIsEmailFromValid(false);
              } else {
                setIsEmailFromValid(true);
              }

              if (emailFrom) {
                setIsEmailFromError(false);
              }
            }}
            helperText={
              isEmailFromError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the email from
                </FormHelperText>
              ) : isEmailFromValid ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the valid email
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Email From Name <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={emailFromName}
            placeholder="Enter the email from name"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isEmailFromNameError}
            isHelperText={true}
            onChange={(e) => {
              setEmailFromName(e.target.value);

              if (emailFromName) {
                setIsEmailFromNameError(false);
              }
            }}
            helperText={
              isEmailFromNameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the email from name
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Email Reply To <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={emailReplyTo}
            placeholder="Enter the email reply to"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isEmailReplyToError}
            isHelperText={true}
            onChange={(e) => {
              setEmailReplyTo(e.target.value);

              if (emailValid(emailReplyTo)) {
                setIsEmailReplyToValid(false);
              } else {
                setIsEmailReplyToValid(true);
              }

              if (emailReplyTo) {
                setIsEmailReplyToError(false);
              }
            }}
            helperText={
              isEmailReplyToError ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the email reply to
                </FormHelperText>
              ) : isEmailReplyToValid ? (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the valid email
                </FormHelperText>
              ) : (
                ""
              )
            }
          />
        </Box>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.875rem" }}>
            Email Reply To Name <span style={{ color: "#B80000" }}>*</span>
          </Typography>
          <CustomInput
            value={emailReplyToName}
            placeholder="Enter the email reply to name"
            sx={{ fontSize: "0.875rem" }}
            isValueError={isEmailReplyToNameError}
            isHelperText={true}
            onChange={(e) => {
              setEmailReplyToName(e.target.value);

              if (emailReplyToName) {
                setIsEmailReplyToNameError(false);
              }
            }}
            helperText={
              isEmailReplyToNameError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                >
                  Please input the email reply to name
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box
          sx={{
            mt: 2,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography>Word Wrap</Typography>
          <FormGroup
            sx={{
              "& .MuiFormControlLabel-root": {
                marginRight: 0,
              },
            }}
          >
            <FormControlLabel
              labelPlacement="end"
              control={<CustomSwitch value={wordWrap} setValue={setWordWrap} />}
            />
          </FormGroup>
        </Box>

        <Box
          sx={{
            mt: 2,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography>Status</Typography>
          <FormGroup
            sx={{
              "& .MuiFormControlLabel-root": {
                marginRight: 0,
              },
            }}
          >
            <FormControlLabel
              labelPlacement="end"
              control={<CustomSwitch value={status} setValue={setStatus} />}
            />
          </FormGroup>
        </Box>

        <CustomButtonSave content="Save" isLoading={isLoading} />
      </Box>
    </Box>
  );
};

export default AddNewEmail;
