import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Fab, Typography } from "@mui/material";
import {
  ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon,
  Add as AddIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
} from "@mui/icons-material";
import { viewEmail } from "../../../../api/config";
import Loading from "../../../../shared/Loading";

const Email = () => {
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [emailData, setEmailData] = useState([]);

  const getViewEmail = async () => {
    try {
      setIsLoading(false);
      const { results } = await viewEmail();
      setEmailData(results);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getViewEmail();
  }, []);

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate("/settings")} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Email
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2 }}>
          <Box
            sx={{
              position: "fixed",
              bottom: 80,
              right: 20,
              color: "red",
              zIndex: 10,
            }}
            onClick={() => navigate("/settings/config/default/add-new-email")}
          >
            <Fab size="small" sx={{ color: "#fff", bgcolor: "#78251E" }}>
              <AddIcon sx={{ fontSize: "1.5rem" }} />
            </Fab>
          </Box>
          <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
            {emailData?.map((item, index) => (
              <Box
                sx={{
                  bgcolor: "#fff",
                  borderRadius: "8px",
                  px: 2,
                  py: 1,
                  boxShadow: 3,
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 2,
                }}
                key={index}
                onClick={() =>
                  navigate(`/settings/config/email/${item?.emailId}`)
                }
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {item?.userAgent}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item?.protocol}
                  </Typography>
                </Box>

                <KeyboardArrowRightIcon sx={{ color: "#78251E" }} />
              </Box>
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default Email;
