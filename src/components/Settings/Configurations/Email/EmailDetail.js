import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import { deleteEmail, showEmail } from "../../../../api/config";
import Loading from "../../../../shared/Loading";
import dayjs from "dayjs";
import DeleteModalVerification from "../Default/DeleteModalVerification";

const EmailDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [emailData, setEmailData] = useState(null);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const getShowEmail = async (id) => {
    try {
      setIsLoading(true);
      const { result } = await showEmail(id);
      setEmailData(result);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getShowEmail(id);
  }, [id]);

  const handleDelete = async () => {
    try {
      setIsLoadingDelete(true);
      await deleteEmail(id);
      setOpenDialogDelete(false);
      navigate("/settings/config/email");
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoadingDelete(false);
    }
  };

  return (
    <Box>
      <Box sx={{ bgcolor: "#78251E" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,
            p: 2,
            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon
            onClick={() => navigate("/settings/config/email")}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Email Detail
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 0.5 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>User Agent</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.userAgent || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Charset</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.charset || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Protocol</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.protocol || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Mail Path</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.mailPath !== null ? emailData?.mailPath : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Priority</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
                color: `#${emailData?.priority?.color}`,
              }}
            >
              {emailData?.priority?.label || "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp Host</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpHost !== null ? emailData?.smtpHost : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp User</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpUser !== null ? emailData?.smtpUser : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp Pass</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpPass !== null ? emailData?.smtpPass : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp Port</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpPort !== null ? emailData?.smtpPort : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp Timeout</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpTimeout !== null ? emailData?.smtpTimeout : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Smtp Crypto</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.smtpCrypto !== null ? emailData?.smtpCrypto : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Mail Type</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.mailType !== null ? emailData?.mailType : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Word Wrap</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.wordWrap === true ? "true" : "false"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Wrap Chars</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.wrapChars !== null ? emailData?.wrapChars : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Email From</Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.emailFrom !== null ? emailData?.emailFrom : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Email From Name
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.emailFromName !== null
                ? emailData?.emailFromName
                : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Email Reply To
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.emailReplyTo !== null ? emailData?.emailReplyTo : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Email Reply To Name
            </Typography>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
              {emailData?.emailReplyToName !== null
                ? emailData?.emailReplyToName
                : "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
                color: `#${emailData?.status?.color}`,
              }}
            >
              {emailData?.status?.label || "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Created At</Typography>
            <Typography
              sx={{
                fontSize: "0.875rem",
                textAlign: "right",
              }}
            >
              {dayjs(emailData?.createdAt?.date).format("DD-MM-YYYY") || "-"}
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
              mt: 4,
            }}
          >
            <Button
              sx={{
                border: "1px solid #78251E",
                px: 2,
                width: "100%",
                borderRadius: "8px",
                color: "#000000",
                textTransform: "capitalize",
              }}
              onClick={() => navigate(`/settings/config/email/edit/${id}`)}
            >
              Edit
            </Button>
            <Button
              sx={{
                width: "100%",
                bgcolor: "#78251E",
                border: "1px solid #78251E",
                borderRadius: "8px",
                ":hover": {
                  bgcolor: "#78251E",
                  border: "1px solid #78251E",
                },
                color: "#fff",
                textTransform: "capitalize",
              }}
              onClick={() => setOpenDialogDelete(true)}
            >
              Delete
            </Button>
          </Box>
        </Box>
      )}

      <DeleteModalVerification
        open={openDialogDelete}
        setOpen={setOpenDialogDelete}
        handleDelete={handleDelete}
        isLoading={isLoadingDelete}
      />
    </Box>
  );
};

export default EmailDetail;
