import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Typography, Grid, Skeleton } from "@mui/material";
import Header from "../shared/Header";
import PackageImage from "../assets/package-icon.png";
import {
  viewPackageCourier,
  viewPackages,
  viewPackagesAdmin,
} from "../api/package";
import dayjs from "dayjs";

const Dashboard = ({ permissions }) => {
  const navigate = useNavigate();

  const [packageData, setPackageData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getPackageDataArray = async () => {
      try {
        setIsLoading(true);

        let res;

        if (permissions?.includes("package.admin")) {
          res = await viewPackagesAdmin({});
        } else if (permissions?.includes("consign.courier")) {
          res = await viewPackageCourier({});
        } else {
          res = await viewPackages({});
        }

        setPackageData(res?.results?.data || []);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    };

    getPackageDataArray();
  }, [permissions]);

  const formattedData = packageData?.map((item) => ({
    ...item,
    createdAt: {
      ...item.createdAt,
      date: dayjs(item.createdAt.date).format("DD-MM-YYYY"),
    },
  }));

  const getDataWeekly = formattedData?.filter((item) => {
    const itemDate = dayjs(item.createdAt?.date, "DD-MM-YYYY");
    const startDate = dayjs().subtract(1, "week").startOf("day");
    const endDate = dayjs().endOf("day");
    return itemDate.isBetween(startDate, endDate, null, "[]");
  });

  const getDataToday = formattedData?.filter((item) => {
    const itemDate = dayjs(item.createdAt?.date, "DD-MM-YYYY");
    const today = dayjs().startOf("day");
    return itemDate.isSame(today, "day");
  });

  const currentDate = new Date();
  const lastWeekDate = new Date(
    currentDate.getTime() - 7 * 24 * 60 * 60 * 1000
  );

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header isNotificationsNeed={true} />

        <Box sx={{ pt: 2, px: 2 }}>
          <Box sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                {isLoading ? (
                  <Skeleton
                    sx={{ borderRadius: "10px", mt: 0.1 }}
                    variant="rectangular"
                    height="113px"
                    animation="wave"
                  />
                ) : (
                  <Box
                    sx={{
                      bgcolor: "#78251E",
                      height: "90px",
                      boxShadow: 3,
                      borderRadius: "10px",
                      p: 1.5,
                      color: "#ffffff",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                    }}
                  >
                    <Box>
                      <Typography sx={{ fontSize: "0.875rem" }}>
                        Weekly Orders
                      </Typography>
                      <Typography sx={{ fontSize: "0.563rem" }}>
                        {dayjs(lastWeekDate).format("DD MMM YYYY")} -{" "}
                        {dayjs(currentDate).format("DD MMM YYYY")}
                      </Typography>
                    </Box>

                    <Typography sx={{ fontSize: "1.125rem" }}>
                      {getDataWeekly?.length || "0"}{" "}
                      {getDataWeekly?.length > 1 ? "Orders" : "Order"}
                    </Typography>
                  </Box>
                )}
              </Grid>
              <Grid item xs={6}>
                {isLoading ? (
                  <Skeleton
                    sx={{ borderRadius: "10px", mt: 0.1 }}
                    variant="rectangular"
                    height="113px"
                    animation="wave"
                  />
                ) : (
                  <Box
                    sx={{
                      bgcolor: "#78251E",
                      height: "5.625rem",
                      boxShadow: 3,
                      borderRadius: "10px",
                      p: 1.5,
                      color: "#ffffff",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                    }}
                  >
                    <Box>
                      <Typography sx={{ fontSize: "0.875rem" }}>
                        Daily Order
                      </Typography>
                      <Typography sx={{ fontSize: "0.563rem" }}>
                        {dayjs(currentDate).format("DD MMM YYYY")}
                      </Typography>
                    </Box>

                    <Typography sx={{ fontSize: "1.125rem" }}>
                      {getDataToday?.length || "0"}{" "}
                      {getDataToday?.length > 1 ? "Orders" : "Order"}
                    </Typography>
                  </Box>
                )}
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Box>

      <Box sx={{ p: 2 }}>
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2.5, mt: 2 }}>
          {isLoading &&
            [1, 2, 3].map((_, index) => (
              <Skeleton
                sx={{ borderRadius: "10px", mt: 0.1 }}
                variant="rectangular"
                height="70px"
                animation="wave"
                key={index}
              />
            ))}

          {!isLoading && getDataToday?.length === 0 && (
            <Typography>There's no packages today</Typography>
          )}

          {!isLoading &&
            packageData?.length > 0 &&
            getDataToday?.map((item, index) => (
              <Box
                sx={{
                  boxShadow: 3,
                  borderRadius: "10px",
                  px: 2,
                  py: 1,
                }}
                key={index}
              >
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 2,
                  }}
                  key={index}
                  onClick={() => navigate(`/package/detail/${item.packageId}`)}
                >
                  <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                    <img src={PackageImage} alt="Package" />
                    <Box>
                      <Typography
                        sx={{ fontWeight: 600, fontSize: "0.875rem" }}
                      >
                        {item?.senderName}
                      </Typography>
                      <Typography sx={{ fontSize: "0.625rem" }}>
                        {item?.createdAt?.date}
                      </Typography>
                      <Typography
                        sx={{ fontSize: "0.75rem", color: "#bdbdbd" }}
                      >
                        Sending To : {item?.recipientName}
                      </Typography>
                    </Box>
                  </Box>

                  <Box
                    sx={{
                      display: "flex",
                      gap: 1,
                      alignItems: "center",
                      border: `1px solid #${item?.status?.color}`,
                      px: 1,
                      py: 0.5,
                      borderRadius: "8px",
                      color: `#${item?.status?.color}`,
                    }}
                  >
                    <i
                      className={item?.status?.icon}
                      style={{ fontSize: "15px" }}
                    />
                    <Typography
                      sx={{
                        fontSize: "0.75rem",
                        fontWeight: 600,
                      }}
                    >
                      {item?.status?.label}
                    </Typography>
                  </Box>
                </Box>
              </Box>
            ))}
        </Box>
      </Box>
    </Box>
  );
};

export default Dashboard;
