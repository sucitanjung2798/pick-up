import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Skeleton,
  Typography,
} from "@mui/material";
import MenuComponent from "../../shared/MenuComponent";
import CustomDialog from "../../shared/CustomDialog";
import { deleteUom, getAllUom } from "../../api/uom";
import InfiniteScroll from "react-infinite-scroll-component";

const UomCard = ({
  uomList,
  setIsLoading,
  fetchUom,
  permissions,
  setUomList,
  setHasMore,
  hasMore,
}) => {
  const navigate = useNavigate();

  const [openDialog, setOpenDialog] = useState(false);
  const [uomId, setUomId] = useState(null);

  const [page, setPage] = useState(2);

  const fetchMoreUom = async ({ page, search, status }) => {
    const {
      results: { data: newUom },
      pages: { currentPage, totalPages },
    } = await getAllUom({ page, search, status });
    setUomList(newUom);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  const handleDeleteUom = async () => {
    try {
      setIsLoading(true);

      await deleteUom(uomId);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
      fetchUom();
      setOpenDialog(false);
    }
  };

  return (
    <Box>
      <InfiniteScroll
        dataLength={uomList?.length || 0}
        next={() => fetchMoreUom({ page })}
        hasMore={hasMore}
        loader={
          uomList?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ mt: 2, display: "flex", flexDirection: "column", gap: 2 }}>
          {uomList.map((uom, index) => {
            const menuLists = [
              {
                name: "Edit UOM",
                clickHandler: () => {
                  navigate(`/uom/edit/${uom.uomId}`);
                },
              },
              {
                name: "Delete UOM",
                clickHandler: () => {
                  setOpenDialog(true);
                  setUomId(uom.uomId);
                },
              },
            ];
            return (
              <Box key={index} sx={{ boxShadow: 3, p: 2, borderRadius: "8px" }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    gap: 1,
                    alignItems: "center",
                  }}
                >
                  <Box>
                    <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
                      {uom.name}
                    </Typography>
                    <Typography sx={{ fontSize: "0.75rem" }}>
                      {uom.abbr}
                    </Typography>
                  </Box>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                    <Typography
                      sx={{
                        fontSize: "0.75rem",
                        border: `1px solid #${uom?.status?.color}`,
                        color: `#${uom?.status?.color}`,
                        py: 0.5,
                        px: 1,

                        borderRadius: "50px",
                        display: "flex",
                        alignItems: "center",
                        gap: 1,
                      }}
                    >
                      <i
                        className={`${uom?.status?.icon}`}
                        style={{
                          fontSize: "15px",
                          color: `#${uom?.status?.color}`,
                        }}
                      ></i>
                      {uom.status?.label}
                    </Typography>

                    <MenuComponent
                      menuLists={menuLists}
                      isNavigateUses={true}
                    />
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
        <CustomDialog
          open={openDialog}
          handleClose={() => setOpenDialog(false)}
        >
          {!permissions.includes("uom.delete") ? (
            <>
              <DialogContent>
                <DialogContentText
                  id="alert-dialog-description"
                  sx={{ color: "#000000" }}
                >
                  You are not permitted to access this action
                </DialogContentText>
              </DialogContent>
              <DialogActions sx={{ px: 2 }}>
                <Button
                  sx={{
                    backgroundColor: "#78251E",
                    border: "1px solid #78251E",
                    color: "#fff",
                    textTransform: "capitalize",
                    px: 2,
                    borderRadius: "8px",
                  }}
                  onClick={() => setOpenDialog(false)}
                  autoFocus
                  size="small"
                >
                  Close
                </Button>
              </DialogActions>
            </>
          ) : (
            <>
              {" "}
              <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
                Delete UOM
              </DialogTitle>
              <DialogContent>
                <DialogContentText
                  id="alert-dialog-description"
                  sx={{ color: "#000000" }}
                >
                  Are you sure want to delete this unit of measurement?
                </DialogContentText>
              </DialogContent>
              <DialogActions sx={{ px: 2 }}>
                <Button
                  sx={{
                    border: "1px solid #78251E",
                    textTransform: "capitalize",
                    color: "#000000",
                    px: 2,
                    borderRadius: "8px",
                    mr: 1,
                  }}
                  size="small"
                  onClick={() => setOpenDialog(false)}
                >
                  Cancel
                </Button>
                <Button
                  sx={{
                    backgroundColor: "#78251E",
                    border: "1px solid #78251E",
                    color: "#fff",
                    textTransform: "capitalize",
                    px: 2,
                    borderRadius: "8px",
                  }}
                  onClick={() => handleDeleteUom(uomId)}
                  autoFocus
                  size="small"
                >
                  Delete
                </Button>
              </DialogActions>
            </>
          )}
        </CustomDialog>
      </InfiniteScroll>
    </Box>
  );
};

export default UomCard;
