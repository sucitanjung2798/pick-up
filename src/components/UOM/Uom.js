import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import UomCard from "./UomCard";
import { getAllUom } from "../../api/uom";
import ScrollOnTop from "../../shared/ScrollOnTop";
import SearchWithDebounce from "../../shared/SearchWithDebounce";
import ListStatusDelivery from "../../shared/ListStatusDelivery";

const statusLists = [
  { value: 1, label: "Active", color: "00FF00" },
  { value: 2, label: "Inactive", color: "FF0000" },
];

const Uom = ({ permissions }) => {
  const navigate = useNavigate();

  const [uomList, setUomList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  const [hasMore, setHasMore] = useState(false);
  const [status, setStatus] = useState("All");

  const fetchUom = async ({ search, status }) => {
    setIsLoading(true);
    try {
      const res = await getAllUom({ search, status });
      setUomList(res?.results?.data || []);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchUom({ search: savedSearch || "", status });
  }, [savedSearch, status]);

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, zIndex: 10, bgcolor: "#fff" }}>
        <Header title="Unit of Measurement" isNotificationsNeed={true} />
      </Box>

      <Box
        sx={{ px: 2, pt: 2, display: "flex", flexDirection: "column", gap: 2 }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            gap: 2,
          }}
        >
          <SearchWithDebounce
            search={search}
            setSearch={setSearch}
            setSearchParams={setSearchParams}
            updatedSearchParams={updatedSearchParams}
            setSavedSearch={setSavedSearch}
          />
          <Button
            sx={{
              border: "1px solid #78251E",
              borderRadius: "10px",
              px: 3,
              height: "40px",
              textTransform: "capitalize",
              color: "#000",
              fontWeight: 600,
            }}
            onClick={() => navigate("/add-new-uom")}
            startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
          >
            New
          </Button>
        </Box>

        <Box>
          <ListStatusDelivery
            textTransform="capitalize"
            setSelectedFile={setStatus}
            selectedFile={status}
            listStatus={statusLists}
            defaultText="All"
            isDefault={true}
          />
        </Box>
      </Box>

      <ScrollOnTop />

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}>
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              variant="rounded"
              height="50px"
              animation="wave"
              key={index}
              sx={{ mt: 2 }}
            />
          ))}

        {!isLoading && uomList?.length === 0 && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <Typography sx={{ textAlign: "center" }}>
              The Uom Data is empty
            </Typography>
          </Box>
        )}

        <ScrollOnTop />

        {!isLoading && uomList?.length > 0 && (
          <UomCard
            setIsLoading={setIsLoading}
            uomList={uomList}
            fetchUom={fetchUom}
            permissions={permissions}
            setUomList={setUomList}
            hasMore={hasMore}
            setHasMore={setHasMore}
          />
        )}
      </Box>
    </Box>
  );
};

export default Uom;
