import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  OutlinedInput,
  Switch,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import CheckIconInactive from "../../assets/check-icon-inactive.png";
import { Check as CheckIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import CustomButtonSave from "../../shared/CustomButtonSave";
import { addNewUom } from "../../api/uom";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const AddNewUom = () => {
  const navigate = useNavigate();

  const [uomName, setUomName] = useState("");
  const [abbr, setAbbr] = useState("");
  const [status, setStatus] = useState(false);
  const [description, setDescription] = useState("");

  const [isUomNameEmpty, setIsUomNameEmpty] = useState(false);
  const [isAbbrEmpty, setIsAbbrEmpty] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (uomName !== "" && abbr !== "") {
      try {
        setIsLoading(true);

        const data = {
          name: uomName,
          abbr,
          description,
          status: status === false ? 0 : 1,
        };

        await addNewUom(data);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
        navigate("/uom-lists");
      }
    } else {
      uomName !== "" ? setIsUomNameEmpty(false) : setIsUomNameEmpty(true);
      abbr !== "" ? setIsAbbrEmpty(false) : setIsAbbrEmpty(true);
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", zIndex: 10, bgcolor: "#fff", top: 0 }}>
        <Header title="Add New UOM" />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate("/uom-lists")} />
        </Box>
      </Box>

      <Box component="form" onSubmit={handleSubmit} sx={{ p: 2 }}>
        <Box sx={{ p: 2, border: "1px solid #bdbdbd", borderRadius: "8px" }}>
          <Box>
            <Typography>
              UOM Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              value={uomName}
              error={!uomName && isUomNameEmpty}
              onChange={(e) => {
                setUomName(e.target.value);
                if (uomName) {
                  setIsUomNameEmpty(false);
                }
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the uom name"
                sx={{ fontSize: "0.875rem" }}
              />

              {isUomNameEmpty && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please input UOM Name
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Abbr <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <FormControl
              variant="outlined"
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  display: "flex",
                  alignItems: "center",
                },
              }}
              value={abbr}
              error={!abbr && isAbbrEmpty}
              onChange={(e) => {
                setAbbr(e.target.value);
                if (abbr) {
                  setIsAbbrEmpty(false);
                }
              }}
              fullWidth
            >
              <OutlinedInput
                placeholder="Enter the abbr"
                sx={{ fontSize: "0.875rem" }}
              />

              {isAbbrEmpty && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please input abbr
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>Description</Typography>
            <Box
              sx={{
                mt: 0.5,
                borderRadius: "10px",
                "& .MuiTextField-root": { width: "100%" },
                "& .MuiOutlinedInput-root": {
                  borderColor: "#bdbdbd",
                  borderRadius: "10px",
                  fontSize: "0.875rem",
                },
              }}
              noValidate
              autoComplete="off"
              placeholder="Input your description"
            >
              <TextField
                multiline
                rows={4}
                placeholder="Input your description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Box>
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Status</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                label={
                  status === true ? (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Active
                    </Typography>
                  ) : (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Non Active
                    </Typography>
                  )
                }
                control={
                  <CustomSwitch
                    checked={status}
                    onChange={() => setStatus(!status)}
                  />
                }
              />
            </FormGroup>
          </Box>
        </Box>

        <CustomButtonSave content="Save" isLoading={isLoading} />
      </Box>
    </Box>
  );
};

export default AddNewUom;
