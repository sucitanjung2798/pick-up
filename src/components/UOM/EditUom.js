import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  OutlinedInput,
  Skeleton,
  Switch,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import CustomButtonSave from "../../shared/CustomButtonSave";
import CheckIcon from "../../assets/check-icon.png";
import CheckIconInactive from "../../assets/check-icon-inactive.png";
import { editUom, showUom } from "../../api/uom";
import NotifCustom from "../../shared/NotifCustom";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,

  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const AddNewUser = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [isLoadingEdit, setIsLoadingEdit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [uomName, setUomName] = useState("");
  const [abbr, setAbbr] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState(false);

  const [isUomNameError, setIsUomNameError] = useState(false);
  const [isAbbrError, setIsAbbrError] = useState(false);

  useEffect(() => {
    const showUomById = async () => {
      try {
        setIsLoading(true);
        const {
          result: { name, abbr, status, description },
        } = await showUom(id);
        setUomName(name);
        setAbbr(abbr);
        setDescription(description);
        setStatus(status?.label === "Active" ? true : false);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    };

    showUomById();
  }, [id]);

  const handleEdit = async (event) => {
    event.preventDefault();

    if (uomName !== "" && abbr !== "") {
      try {
        setIsLoadingEdit(true);

        const data = {
          id,
          name: uomName,
          abbr,
          description,
          status: status === true ? 1 : 0,
        };

        await editUom(id, data);
        navigate("/uom-lists");
        NotifCustom();
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoadingEdit(false);
      }
    } else {
      uomName !== "" ? setIsUomNameError(false) : setIsUomNameError(true);
      abbr !== "" ? setIsAbbrError(false) : setIsAbbrError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ zIndex: 10, bgcolor: "#fff", position: "sticky", top: 0 }}>
        <Header title="Add New User" isNotificationsNeed={true} />

        <Box sx={{ p: 2 }}>
          <BackButton onClick={() => navigate("/uom-lists")} />
        </Box>
      </Box>

      {isLoading && (
        <Box sx={{ p: 2 }}>
          <Skeleton
            sx={{ mt: 2 }}
            variant="rounded"
            height="400px"
            animation="wave"
          />
        </Box>
      )}

      {!isLoading && (
        <Box component="form" onSubmit={handleEdit} sx={{ p: 2 }}>
          <Box sx={{ border: "1px solid #bdbdbd", p: 2, borderRadius: "10px" }}>
            <Box>
              <Typography>
                UOM Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter the uom name"
                  sx={{ fontSize: "0.875rem" }}
                  value={uomName || ""}
                  error={!uomName && isUomNameError}
                  onChange={(e) => {
                    setUomName(e.target.value);

                    if (uomName) {
                      setIsUomNameError(false);
                    }
                  }}
                />

                {isUomNameError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the uom name
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Abbr <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter the name"
                  sx={{ fontSize: "0.875rem" }}
                  value={abbr || ""}
                  error={!abbr && isAbbrError}
                  onChange={(e) => {
                    setAbbr(e.target.value);

                    if (abbr) {
                      setIsAbbrError(false);
                    }
                  }}
                />

                {isAbbrError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input the abbr
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>Description</Typography>
              <Box
                sx={{
                  mt: 0.5,
                  borderRadius: "10px",
                  "& .MuiTextField-root": { width: "100%" },
                  "& .MuiOutlinedInput-root": {
                    borderColor: "#bdbdbd",
                    borderRadius: "10px",
                    fontSize: "0.875rem",
                  },
                }}
                noValidate
                autoComplete="off"
                placeholder="Input your description"
              >
                <TextField
                  multiline
                  rows={4}
                  placeholder="Input your description"
                  value={description || ""}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Box>
            </Box>

            <Box
              sx={{
                mt: 2,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography>Status</Typography>
              <FormGroup
                sx={{
                  "& .MuiFormControlLabel-root": {
                    marginRight: 0,
                  },
                }}
              >
                <FormControlLabel
                  labelPlacement="end"
                  label={
                    status === true ? (
                      <Typography sx={{ fontSize: "14px", ml: 1 }}>
                        Active
                      </Typography>
                    ) : (
                      <Typography sx={{ fontSize: "14px", ml: 1 }}>
                        Non Active
                      </Typography>
                    )
                  }
                  control={
                    <CustomSwitch
                      checked={status}
                      onChange={() => setStatus(!status)}
                    />
                  }
                />
              </FormGroup>
            </Box>
          </Box>

          <Box sx={{ mt: 2 }}>
            <CustomButtonSave content="Save" isLoading={isLoadingEdit} />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default AddNewUser;
