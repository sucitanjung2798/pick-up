import React, { useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import { ArrowBackRounded as ArrowBackRoundedIcon } from "@mui/icons-material";
import VerificationCodeIcon from "../../assets/otp-image.svg";
import CustomButtonSave from "../../shared/CustomButtonSave";

import VerifiedResetPasswordModal from "./VerifiedResetPasswordModal";
import { sendOtp } from "../../api/auth";

const VerificationCode = () => {
  const navigate = useNavigate();

  const [otp, setOtp] = useState(new Array(6).fill(""));
  const [error, setError] = useState(null);
  const otpBoxReference = useRef([]);

  const [isLoading, setIsLoading] = useState(false);

  const handleBackspaceAndEnter = (e, index) => {
    if (e.key === "Backspace" && !e.target.value && index > 0) {
      otpBoxReference.current[index - 1].focus();
    }
    if (e.key === "Enter" && e.target.value && index < 6 - 1) {
      otpBoxReference.current[index - 1].focus();
    }
  };

  // useEffect(() => {
  //   if (otp.join("") !== "" && otp.join("") !== correctOTP) {
  //     setOtpError("Wrong OTP Please Check Again");
  //   } else {
  //     setOtpError(null);
  //   }
  // }, [otp]);

  document.querySelectorAll('input[type="number"]').forEach((input) => {
    input.oninput = () => {
      if (input.value.length > input.maxLength)
        input.value = input.value.slice(0, input.maxLength);
    };
  });

  const [openDialog, setOpenDialog] = useState(false);

  const handleVerifyOtp = async (event) => {
    event.preventDefault();

    try {
      setIsLoading(true);

      const data = {
        email: localStorage.getItem("email"),
        verification_code: otp.join(""),
      };

      await sendOtp(data);
      setOpenDialog(true);
    } catch (err) {
      const errorResponse = err?.response?.status;

      if (errorResponse === 400) {
        setError("The otp is invalid");
      } else {
        setError("Error");
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleChange = (value, index) => {
    let newArr = [...otp];
    newArr[index] = value;
    setOtp(newArr);

    if (value && index < 6 - 1) {
      otpBoxReference.current[index + 1].focus();
    }
  };

  return (
    <Box sx={{ p: 2 }}>
      <Box
        sx={{ display: "flex", gap: 2 }}
        onClick={() => navigate("/recovery-password")}
      >
        <ArrowBackRoundedIcon />
        <Typography sx={{ fontWeight: 600 }}>Verification Code</Typography>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          mt: 2,
        }}
      >
        <Box component="img" src={VerificationCodeIcon} sx={{ width: 250 }} />
      </Box>

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ textAlign: "center", fontWeight: 600 }}>
          Check your phone
        </Typography>

        <Typography
          sx={{ textAlign: "center", fontSize: "0.875rem", color: "#757575" }}
        >
          We've sent the code to your phone
        </Typography>
      </Box>

      {error && (
        <Typography
          sx={{
            mb: 2,
            mt: 1,
            bgcolor: "#ef9a9a",
            color: "#d50000",
            p: 1,
            fontSize: "0.875rem",
            borderRadius: "3px",
          }}
        >
          {error}
        </Typography>
      )}

      <Box component="form" onSubmit={handleVerifyOtp}>
        <Box sx={{ mt: 2 }}>
          <div className="grid-container">
            {otp.map((digit, index) => (
              <div className="grid-item" key={index}>
                <input
                  key={index}
                  value={digit}
                  maxLength="1"
                  onChange={(e) => {
                    handleChange(e.target.value, index);
                  }}
                  onKeyUp={(e) => handleBackspaceAndEnter(e, index)}
                  ref={(reference) =>
                    (otpBoxReference.current[index] = reference)
                  }
                  className="input-otp"
                  style={{
                    border: `1px solid #bdbdbd `,
                  }}
                  type="number"
                />
              </div>
            ))}
          </div>
        </Box>
        {/* 
        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "0.75rem", textAlign: "center" }}>
            Code expires in <span style={{ fontWeight: 600 }}>03:12</span>
          </Typography>
        </Box> */}

        <Box sx={{ mt: 2 }}>
          <CustomButtonSave content="Verify" isLoading={isLoading} />

          {/* <Button
            sx={{
              mt: 1.5,
              textTransform: "capitalize",

              color: "#78251E",
              fontSize: "16px",
              bgcolor: "#ffffff",
              border: "1px solid #78251E",
              height: "45px",
              borderRadius: "8px",
              ":hover": {
                bgcolor: "#ffffff",
                color: "#78251E",
                border: "1px solid #78251E",
              },
            }}
            fullWidth
          >
            Send Again
          </Button> */}
        </Box>
      </Box>

      <VerifiedResetPasswordModal
        openDialog={openDialog}
        setOpenDialog={setOpenDialog}
      />
    </Box>
  );
};

export default VerificationCode;
