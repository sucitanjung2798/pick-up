import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControl,
  FormHelperText,
  InputAdornment,
  OutlinedInput,
  Typography,
} from "@mui/material";
import {
  ArrowBackRounded as ArrowBackRoundedIcon,
  EmailRounded as EmailRoundedIcon,
} from "@mui/icons-material";
import EmailIcon from "../../assets/mail-sent-icon.svg";
import { sendEmailForgotPassword } from "../../api/auth";
import CustomButtonSave from "../../shared/CustomButtonSave";

const RecoveryPassword = () => {
  const navigate = useNavigate();

  const [recoveryEmail, setRecoveryEmail] = useState("");
  const [isRecoveryEmailError, setIsRecoveryEmailError] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const emailValid = () => {
    return /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/.test(recoveryEmail);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (recoveryEmail !== "" && emailValid(recoveryEmail)) {
      try {
        setIsLoading(true);

        const data = {
          email: recoveryEmail,
        };

        await sendEmailForgotPassword(data);
        navigate("/verification-code");
        localStorage.setItem("email", recoveryEmail);
      } catch (err) {
        const errorResponse = err?.response?.status;

        if (errorResponse === 400) {
          setError("This email is not registered");
        } else if (errorResponse === 500) {
          setError("Internal Server Error");
        } else {
          setError("Error");
        }
      } finally {
        setIsLoading(false);
      }
    } else {
      recoveryEmail
        ? setIsRecoveryEmailError(false)
        : setIsRecoveryEmailError(true);

      emailValid(recoveryEmail)
        ? setIsEmailValid(false)
        : setIsEmailValid(true);
    }
  };

  return (
    <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
      <Box sx={{ display: "flex", gap: 2 }} onClick={() => navigate("/")}>
        <ArrowBackRoundedIcon />
        <Typography sx={{ fontWeight: 600 }}>Recovery Password</Typography>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          mt: 2,
        }}
      >
        <Box component="img" src={EmailIcon} sx={{ width: 250 }} />
      </Box>

      {error && (
        <Typography
          sx={{
            mb: 2,
            bgcolor: "#ef9a9a",
            color: "#d50000",
            p: 1,
            fontSize: "0.875rem",
            borderRadius: "3px",
          }}
        >
          {error}
        </Typography>
      )}

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ fontSize: "12px", color: "#BDBDBD" }}>
          Email
        </Typography>
        <FormControl
          variant="outlined"
          sx={{
            mt: 0.5,
            "& .MuiOutlinedInput-root": {
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
            },
          }}
          fullWidth
        >
          <OutlinedInput
            placeholder="Enter your email address"
            value={recoveryEmail}
            onChange={(e) => {
              if (emailValid(recoveryEmail)) {
                setIsEmailValid(false);
              } else {
                setIsEmailValid(true);
              }

              setRecoveryEmail(e.target.value);

              if (recoveryEmail) {
                setIsRecoveryEmailError(false);
              }
            }}
            error={!recoveryEmail && isRecoveryEmailError}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": { width: "0.75em", margin: "-1px" },
                }}
              >
                <EmailRoundedIcon />
              </InputAdornment>
            }
            sx={{ fontSize: "12px" }}
          />
        </FormControl>
        {isRecoveryEmailError ? (
          <FormHelperText sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}>
            Please input your email
          </FormHelperText>
        ) : isEmailValid ? (
          <FormHelperText sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}>
            Please input the valid email
          </FormHelperText>
        ) : (
          ""
        )}
      </Box>

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ fontSize: "0.875rem" }}>
          We will send a verification code to this email if it matches the
          associated account.
        </Typography>
      </Box>

      <Box sx={{ mt: 2 }}>
        <CustomButtonSave isLoading={isLoading} content="Next" />
      </Box>
    </Box>
  );
};

export default RecoveryPassword;
