import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  InputAdornment,
  OutlinedInput,
  Typography,
} from "@mui/material";
import {
  ArrowBackRounded as ArrowBackRoundedIcon,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import ChangePasswordIcon from "../../assets/reset-password.svg";

const ChangePassword = () => {
  const navigate = useNavigate();

  const [isShowNewPassword, setIsShowNewPassword] = useState(false);
  const [isShowConfirmationPassword, setIsShowConfirmationPassword] =
    useState(false);
  const [newPassword, setNewPassword] = useState("");
  const [newPasswordErrors, setNewPasswordErrors] = useState(false);

  const handleNewPasswordChange = (e) => {
    const newPassword = e.target.value;
    setNewPassword(newPassword);

    if (!newPassword && newPassword < 8) {
      setNewPasswordErrors(true);
    } else {
      setNewPasswordErrors(false);
    }
  };

  const handleClickShowNewPassword = () =>
    setIsShowNewPassword((show) => !show);
  const handleClickShowConfirmationPassword = () =>
    setIsShowConfirmationPassword((show) => !show);

  const handleMouseDownNewPassword = (event) => {
    event.preventDefault();
  };

  const handleMouseDownConfirmationPassword = (event) => {
    event.preventDefault();
  };

  return (
    <Box sx={{ py: 2, px: 1 }}>
      <Box
        sx={{ display: "flex", gap: 2 }}
        onClick={() => navigate("/recovery-password")}
      >
        <ArrowBackRoundedIcon />
        <Typography sx={{ fontWeight: 600 }}>Change Password</Typography>
      </Box>

      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          mt: 2,
        }}
      >
        <Box component="img" src={ChangePasswordIcon} sx={{ width: 250 }} />
      </Box>

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ fontSize: "12px", color: "#BDBDBD" }}>
          New Password
        </Typography>
        <FormControl
          variant="outlined"
          sx={{
            mt: 0.5,
            "& .MuiOutlinedInput-root": {
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
            },
          }}
          fullWidth
        >
          <OutlinedInput
            placeholder="Enter your new password"
            type={isShowNewPassword ? "text" : "password"}
            value={newPassword}
            onChange={handleNewPasswordChange}
            error={!newPassword && newPasswordErrors}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowNewPassword}
                  onMouseDown={handleMouseDownNewPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowNewPassword ? <Visibility /> : <VisibilityOff />}
                </Box>
              </InputAdornment>
            }
            sx={{ fontSize: "12px" }}
          />

          {newPasswordErrors && (
            <FormHelperText sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}>
              Password must be at least 8 characters
            </FormHelperText>
          )}
        </FormControl>
      </Box>

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ fontSize: "12px", color: "#BDBDBD" }}>
          Confirmation Password
        </Typography>
        <FormControl
          variant="outlined"
          sx={{
            mt: 0.5,
            "& .MuiOutlinedInput-root": {
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
            },
          }}
          fullWidth
        >
          <OutlinedInput
            placeholder="Enter your confirmation password"
            type={isShowConfirmationPassword ? "text" : "password"}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowConfirmationPassword}
                  onMouseDown={handleMouseDownConfirmationPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowConfirmationPassword ? (
                    <Visibility />
                  ) : (
                    <VisibilityOff />
                  )}
                </Box>
              </InputAdornment>
            }
            sx={{ fontSize: "12px" }}
          />
        </FormControl>
      </Box>

      <Box sx={{ mt: 2 }}>
        <Button
          sx={{
            mt: 3,
            textTransform: "capitalize",
            color: "#ffffff",
            fontSize: "16px",
            bgcolor: "#78251E",
            height: "45px",
            borderRadius: "8px",
            ":hover": {
              bgcolor: "#78251E",
              color: "#ffffff",
            },
          }}
          fullWidth
          onClick={() => navigate("/")}
        >
          Next
        </Button>
      </Box>
    </Box>
  );
};

export default ChangePassword;
