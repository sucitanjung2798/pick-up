import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, DialogContent, DialogContentText } from "@mui/material";
import CustomDialog from "../../shared/CustomDialog";
import VerifiedImage from "../../assets/verified-image.svg";

const VerifiedResetPasswordModal = ({ openDialog, setOpenDialog }) => {
  const navigate = useNavigate();

  return (
    <CustomDialog open={openDialog}>
      <DialogContent>
        <Box
          noValidate
          component="form"
          sx={{
            display: "flex",
            flexDirection: "column",
            m: "auto",
            width: "fit-content",
          }}
        >
          <Box component="img" src={VerifiedImage} sx={{ width: "250px" }} />
        </Box>

        <DialogContentText
          sx={{ textAlign: "center", color: "#000000", mt: 1 }}
        >
          Your password has been reset
        </DialogContentText>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            mt: 1,
          }}
        >
          <Button
            sx={{
              backgroundColor: "#78251E",
              border: "1px solid #78251E",
              color: "#fff",
              textTransform: "capitalize",
              px: 2,
              borderRadius: "8px",
              width: "200px",
            }}
            onClick={() => {
              setOpenDialog(false);
              navigate("/");
              localStorage.removeItem("email");
            }}
            autoFocus
          >
            Back to Login Page
          </Button>
        </Box>
      </DialogContent>
    </CustomDialog>
  );
};

export default VerifiedResetPasswordModal;
