import React from "react";
import { useNavigate } from "react-router-dom";
import NotFoundImage from "../assets/not-found-image.svg";
import { Box, Typography, Button } from "@mui/material";

const NotFound = () => {
  const navigate = useNavigate();

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        gap: 1,
      }}
    >
      <Box component="img" src={NotFoundImage} />
      <Box sx={{ textAlign: "center", px: 2 }}>
        <Typography sx={{ fontSize: "20px", fontWeight: 600 }}>
          Your page didn't response
        </Typography>
        <Box sx={{ mt: 1, color: "#757575" }}>
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 500 }}>
            This page doesn't exists or maybe fall a sleep!
          </Typography>
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 500 }}>
            We suggest you to back home
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",
              color: "#ffffff",
              fontSize: "18px",
              bgcolor: "#78251E",
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              width: 200,
              gap: 1,
              justifyContent: "center",
              alignItems: "center",
              ":hover": {
                bgcolor: "#78251E",
                color: "#ffffff",
              },
            }}
            fullWidth
            onClick={() => navigate(-1)}
          >
            Go Back Home
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default NotFound;
