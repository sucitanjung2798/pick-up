import React from "react";
import { useNavigate } from "react-router-dom";
import {
  Autocomplete,
  Box,
  Divider,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import Loading from "../../shared/Loading";
import CustomInput from "../../shared/CustomInput";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { MobileDatePicker } from "@mui/x-date-pickers";
import CustomSwitch from "../../shared/CustomSwitch";
import AddNewPackageDetail from "./AddNewPackageDetail";
import CustomButtonSave from "../../shared/CustomButtonSave";
import dayjs from "dayjs";
import CustomDescriptionField from "../../shared/CustomDescriptionField";

const PackageEditContent = ({
  isLoading,
  handleSubmit,
  senderLocation,
  setSenderLocation,
  isSenderLocationError,
  recipientLocation,
  setRecipientLocation,
  isRecipientLocationError,
  isDisabled,
  senderName,
  setSenderName,
  isSenderNameError,
  setIsSenderNameError,
  recipientData,
  setRecipientData,
  setIsDisabled,
  setIsRecipientDataError,
  recipientOptions,
  isRecipientDataError,
  recipientName,
  isRecipientNameError,
  setRecipientName,
  setIsRecipientNameError,
  priorityLevel,
  setPriorityLevel,
  setIsPriorityLevelError,
  isPriorityLevelError,
  enumPriority,
  date,
  setDate,
  isDateError,
  confidential,
  setConfidential,
  packageDetailArray,
  setPackageDetailArray,
  uomLists,
  isPackageNameError,
  setIsPackageNameError,
  isUomError,
  setIsUomError,
  isQuantityError,
  setIsQuantityError,
  isNoteError,
  setIsNoteError,
  isLoadingEdit,
  description,
  setDescription,
}) => {
  const navigate = useNavigate();
  return (
    <Box>
      <Box sx={{ position: "sticky", zIndex: 10, top: 0, bgcolor: "#fff" }}>
        <Header
          title="Package Edit"
          isNotificationsNeed={true}
          isHistoryNeed={true}
        />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate(-1)} />
        </Box>
      </Box>

      {isLoading && <Loading />}

      {!isLoading && (
        <Box component="form" onSubmit={handleSubmit} sx={{ p: 2 }}>
          <Box
            sx={{
              border: "1px solid #bdbdbd",
              p: 2,
              borderRadius: "10px",
            }}
          >
            <div className="step">
              <div className="v-stepper">
                <div className="circle"></div>
                <div className="line"></div>
              </div>

              <div className="content" style={{ marginBottom: "4px" }}>
                <Typography
                  sx={{
                    fontSize: "0.875rem",
                    color: "#bdbdbd",
                    fontWeight: 600,
                  }}
                >
                  From
                </Typography>
                <TextField
                  value={senderLocation}
                  onChange={(e) => setSenderLocation(e.target.value)}
                  fullWidth
                  variant="standard"
                  error={!senderLocation && isSenderLocationError}
                  InputProps={{
                    disableUnderline: true,
                    style: {
                      fontSize: "14px",
                      color: isSenderLocationError && "#B80000",
                    },
                  }}
                  sx={{
                    mt: -0.5,
                  }}
                  placeholder="Input your location"
                />
              </div>
            </div>

            <Divider sx={{ ml: 2.8, mb: 1 }} />

            <div className="step">
              <div className="v-stepper">
                <div className="circle-grey"></div>
                <div className="line"></div>
              </div>

              <div className="content">
                <Typography
                  sx={{
                    fontSize: "0.875rem",
                    color: "#bdbdbd",
                    fontWeight: 600,
                  }}
                >
                  To
                </Typography>
                <TextField
                  value={recipientLocation}
                  onChange={(e) => setRecipientLocation(e.target.value)}
                  fullWidth
                  variant="standard"
                  InputProps={{
                    disableUnderline: true,
                    style: {
                      fontSize: "14px",
                      color: isRecipientLocationError && "#B80000",
                      // fontWeight: 600,
                    },
                  }}
                  disabled={isDisabled}
                  sx={{
                    mt: -0.5,
                  }}
                  placeholder="Input your destination"
                />
              </div>
            </div>
          </Box>

          <Box
            sx={{
              mt: 2,
              border: "1px solid #bdbdbd",
              borderRadius: "10px",
              p: 2,
            }}
          >
            <Box sx={{ mt: 2 }}>
              <Typography>
                Sender Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={senderName}
                placeholder="Enter the sender name"
                sx={{ fontSize: "0.875rem" }}
                isValueError={isSenderNameError}
                isHelperText={true}
                onChange={(e) => {
                  setSenderName(e.target.value);

                  if (senderName) {
                    setIsSenderNameError(false);
                  }
                }}
                helperText={
                  isSenderNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input sender name
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Recipient <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <Autocomplete
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    fontSize: "0.875rem",
                    padding: "5px 4px 7.5px 9px",
                  },
                }}
                fullWidth
                value={recipientData}
                onChange={(event, newValue) => {
                  setRecipientData(newValue);

                  if (newValue) {
                    setIsDisabled(false);
                    setIsRecipientDataError(false);
                  } else {
                    setIsDisabled(true);
                    setIsRecipientDataError(true);
                  }
                }}
                getOptionLabel={(option) => option?.label || null}
                options={recipientOptions}
                renderOption={(props, option) => (
                  <MenuItem {...props} key={option.id}>
                    {option.label}
                  </MenuItem>
                )}
                isOptionEqualToValue={(option, value) => option.id === value.id}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Please input recipient"
                    error={!recipientData && isRecipientDataError}
                    sx={{
                      "& .MuiFormHelperText-root": {
                        marginLeft: 0,
                      },
                    }}
                    helperText={
                      isRecipientDataError && (
                        <span
                          sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                        >
                          Please input recipient
                        </span>
                      )
                    }
                  />
                )}
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Recipient Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={recipientName}
                placeholder="Enter the recipient name"
                sx={{ fontSize: "0.875rem" }}
                isValueError={isRecipientNameError}
                isHelperText={true}
                disabled={isDisabled}
                onChange={(e) => {
                  setRecipientName(e.target.value);

                  if (recipientName) {
                    setIsRecipientNameError(false);
                  }
                }}
                helperText={
                  isRecipientNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input recipient name
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Priority <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <FormControl
                fullWidth
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    // width: "7.188rem",
                    borderRadius: "8px",
                    fontSize: "0.875rem",
                  },
                }}
              >
                <Select
                  displayEmpty
                  value={priorityLevel}
                  onChange={(e) => {
                    setPriorityLevel(e.target.value);

                    if (priorityLevel) {
                      setIsPriorityLevelError(false);
                    }
                  }}
                  input={
                    <OutlinedInput
                      error={!priorityLevel && isPriorityLevelError}
                    />
                  }
                >
                  <MenuItem disabled value="">
                    <Typography sx={{ fontSize: "0.875rem", color: "#bdbdbd" }}>
                      Select the priority level
                    </Typography>
                  </MenuItem>
                  {enumPriority?.map((option, index) => (
                    <MenuItem
                      key={index}
                      value={option.value}
                      sx={{ fontSize: "0.875rem" }}
                    >
                      {option.label}
                    </MenuItem>
                  ))}
                </Select>
                {isPriorityLevelError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please select the priority level
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Estimate Time of Arrival{" "}
                <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <DemoContainer
                sx={{
                  "& .MuiStack-root": {
                    overflow: "hidden !important",
                  },
                }}
                components={["MobileDatePicker"]}
              >
                <DemoItem>
                  <MobileDatePicker
                    fullWidth
                    sx={{
                      mt: 0.5,

                      "& .MuiOutlinedInput-root": {
                        height: "44px",
                        borderRadius: "8px",
                        fontSize: "0.875rem",
                      },
                    }}
                    value={dayjs(date)}
                    onChange={(newValue) => setDate(newValue)}
                  />
                </DemoItem>
              </DemoContainer>
              {isDateError && (
                <Typography sx={{ fontSize: "11px", color: "#d50000" }}>
                  Please select the date
                </Typography>
              )}
            </Box>

            <Box
              sx={{
                mt: 2,
              }}
            >
              <Typography>Description</Typography>
              <CustomDescriptionField
                placeholder="Write your description"
                sx={{ mt: 0.5 }}
                setValue={setDescription}
                value={description}
              />
            </Box>

            <Box
              sx={{
                mt: 2,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography>Confidential</Typography>
              <FormGroup
                sx={{
                  "& .MuiFormControlLabel-root": {
                    marginRight: 0,
                  },
                }}
              >
                <FormControlLabel
                  labelPlacement="end"
                  control={
                    <CustomSwitch
                      value={confidential}
                      setValue={setConfidential}
                    />
                  }
                />
              </FormGroup>
            </Box>
          </Box>

          <AddNewPackageDetail
            packageDetailArray={packageDetailArray}
            setPackageDetailArray={setPackageDetailArray}
            uomLists={uomLists}
            isPackageNameError={isPackageNameError}
            setIsPackageNameError={setIsPackageNameError}
            isUomError={isUomError}
            setIsUomError={setIsUomError}
            isQuantityError={isQuantityError}
            setIsQuantityError={setIsQuantityError}
            isNoteError={isNoteError}
            setIsNoteError={setIsNoteError}
          />

          <CustomButtonSave isLoading={isLoadingEdit} content="Save" />
        </Box>
      )}
    </Box>
  );
};

export default PackageEditContent;
