import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import {
  viewPackageCourier,
  viewPackages,
  viewPackagesAdmin,
} from "../../api/package";
import PackageCard from "./PackageCard";
import ScrollOnTop from "../../shared/ScrollOnTop";
import SearchWithDebounce from "../../shared/SearchWithDebounce";
import FilterDialog from "./FilterDialog";

const Orders = ({ permissions }) => {
  const navigate = useNavigate();

  const [startDateSave, setStartDateSave] = useState(null);
  const [endDateSave, setEndDateSave] = useState(null);

  const [startCreatedDateSave, setStartCreatedDateSave] = useState(null);
  const [endCreatedDateSave, setEndCreatedDateSave] = useState(null);

  const [packageLists, setPackageLists] = useState([] || null);
  const [isLoading, setIsLoading] = useState(false);
  const [status, setStatus] = useState("All");
  const [statusSave, setStatusSave] = useState(null);
  const [errMessage, setErrMessage] = useState(null);
  const [priority, setPriority] = useState(null);
  const [prioritySave, setPrioritySave] = useState(null);

  const [hasMore, setHasMore] = useState(false);

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  useEffect(() => {
    const getPackageLists = async ({
      status,
      search,
      etaBeginDate,
      etaEndDate,
      priority,
      createdBeginDate,
      createdEndDate,
    }) => {
      try {
        setIsLoading(true);

        let res;
        if (permissions.includes("package.admin")) {
          res = await viewPackagesAdmin({
            status,
            search,
            etaBeginDate,
            etaEndDate,
            priority,
            createdBeginDate,
            createdEndDate,
          });
        } else if (permissions.includes("consign.courier")) {
          res = await viewPackageCourier({ search });
        } else {
          res = await viewPackages({
            search,
            status,
            etaBeginDate,
            etaEndDate,
            priority,
            createdBeginDate,
            createdEndDate,
          });
        }

        const currentPage = res?.results?.pages?.currentPage;
        const totalPage = res?.results?.pages?.totalPage;

        if (totalPage - currentPage) {
          if (currentPage) {
            setHasMore(true);
          } else {
            setHasMore(false);
          }
        }

        setPackageLists(res?.results?.data || []);
        setErrMessage(null);
      } catch (err) {
        console.error(err);

        const errStatus = err?.response?.status;

        if (errStatus === 400) {
          setErrMessage(err?.response?.data?.errors?.eta_end_date);
        }

        setPackageLists([]);
      } finally {
        setIsLoading(false);
      }
    };

    getPackageLists({
      status: statusSave,
      search: savedSearch || "",
      etaBeginDate: startDateSave,
      etaEndDate: endDateSave,
      priority: prioritySave,
      createdBeginDate: startCreatedDateSave,
      createdEndDate: endCreatedDateSave,
    });
  }, [
    statusSave,
    savedSearch,
    permissions,
    startDateSave,
    endDateSave,
    prioritySave,
    startCreatedDateSave,
    endCreatedDateSave,
  ]);

  return (
    <Box sx={{ pb: 4 }}>
      <Box
        sx={{
          position: "sticky",
          top: 0,
          bgcolor: "#ffffff",
          zIndex: 10,
          width: "100%",
        }}
      >
        <Header title="Package" isNotificationsNeed={true} />

        <Box sx={{ p: 2 }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            {!permissions.includes("consign.courier") && (
              <FilterDialog
                setEndDateSave={setEndDateSave}
                setStartDateSave={setStartDateSave}
                setStatusSave={setStatusSave}
                status={status}
                setStatus={setStatus}
                priority={priority}
                setPriority={setPriority}
                setPrioritySave={setPrioritySave}
                setStartCreatedDateSave={setStartCreatedDateSave}
                setEndCreatedDateSave={setEndCreatedDateSave}
              />
            )}

            <SearchWithDebounce
              search={search}
              setSearch={setSearch}
              setSearchParams={setSearchParams}
              updatedSearchParams={updatedSearchParams}
              setSavedSearch={setSavedSearch}
            />
            <Button
              sx={{
                border: "1px solid #78251E",
                borderRadius: "10px",
                px: 3,
                height: "40px",
                textTransform: "capitalize",
                color: "#000",
                fontWeight: 600,
              }}
              startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
              onClick={() => navigate("/add-new-package")}
            >
              New
            </Button>
          </Box>
        </Box>
      </Box>

      <ScrollOnTop />

      <Box sx={{ display: "flex", flexDirection: "column", gap: 2, px: 2 }}>
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              variant="rounded"
              height="60px"
              animation="wave"
              key={index}
            />
          ))}
      </Box>

      {!isLoading && packageLists?.length === 0 && !errMessage && (
        <Typography
          sx={{
            display: "flex",
            justifyContent: "center",
            height: "50vh",
            alignItems: "center",
            fontSize: "0.875rem",
          }}
        >
          The Package List is empty
        </Typography>
      )}

      {!isLoading && packageLists?.length === 0 && errMessage && (
        <Typography sx={{ p: 4, textAlign: "center", fontSize: "0.875rem" }}>
          {errMessage}
        </Typography>
      )}

      {!isLoading && packageLists?.length > 0 && (
        <PackageCard
          packageLists={packageLists}
          hasMore={hasMore}
          setHasMore={setHasMore}
          setPackageLists={setPackageLists}
          permissions={permissions}
          startDate={startDateSave}
          endDate={endDateSave}
        />
      )}
    </Box>
  );
};

export default Orders;
