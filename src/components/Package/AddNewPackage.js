import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Autocomplete,
  Box,
  Divider,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
  Typography,
} from "@mui/material";

import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import { viewUserActive } from "../../api/users";
import { AuthContext } from "../../contexts/AuthContext";
import { MobileDatePicker } from "@mui/x-date-pickers";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import dayjs from "dayjs";
import CustomButtonSave from "../../shared/CustomButtonSave";
import { createPackage, createPackageDetail } from "../../api/package";
import CustomSwitch from "../../shared/CustomSwitch";
import CustomInput from "../../shared/CustomInput";
import AddNewPackageDetail from "./AddNewPackageDetail";
import { getAllUom } from "../../api/uom";
import CustomDescriptionField from "../../shared/CustomDescriptionField";

const AddNewPackage = () => {
  const navigate = useNavigate();
  const { enumPriority, profileData } = useContext(AuthContext);

  const dateToday = dayjs().format("MM/DD/YYYY");

  const [senderLocation, setSenderLocation] = useState("");
  const [recipientLocation, setRecipientLocation] = useState("");
  const [senderName, setSenderName] = useState("");
  const [recipientName, setRecipientName] = useState("");
  const [date, setDate] = useState(dateToday);
  const [confidential, setConfidential] = useState(false);
  const [priorityLevel, setPriorityLevel] = useState("");
  const [description, setDescription] = useState("");

  const [recipientId, setRecipientId] = useState(null);

  const [isRecipientIdError, setIsRecipientIdError] = useState(false);
  const [isSenderLocationError, setIsSenderLocationError] = useState(false);
  const [isRecipientLocationError, setIsRecipientLocationError] =
    useState(false);
  const [isSenderNameError, setIsSenderNameError] = useState(false);
  const [isRecipientNameError, setIsRecipientNameError] = useState(false);
  const [isPriorityLevelError, setIsPriorityLevelError] = useState(false);
  const [isDateError, setIsDateError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);

  const [recipientOptions, setRecipientOptions] = useState([]);
  const [uomLists, setUomLists] = useState([]);

  const [isPackageNameError, setIsPackageNameError] = useState(false);
  const [isUomError, setIsUomError] = useState(false);
  const [isQuantityError, setIsQuantityError] = useState(false);
  const [isNoteError, setIsNoteError] = useState(false);

  const [packageDetailArray, setPackageDetailArray] = useState([
    { packageName: "", uom: "", quantity: "", note: "" },
  ]);
  const [isPackageDetailEmpty, setIsPackageDetailEmpty] = useState(false);

  const getUserList = async () => {
    try {
      const {
        results: { data },
      } = await viewUserActive();
      setRecipientOptions(
        data?.map(({ userId, name, department }) => ({
          id: userId,
          label: name,
          department,
        }))
      );
    } catch (err) {
      console.error(err);
    }
  };

  const getUomList = async () => {
    try {
      const {
        results: { data },
      } = await getAllUom({});
      setUomLists(data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getUserList();
    getUomList();

    setSenderName(profileData?.name);
    setSenderLocation(profileData?.department?.location);

    if (recipientId !== null) {
      setRecipientLocation(recipientId?.department?.location);
      setRecipientName(recipientId?.label);
    } else {
      setRecipientLocation("");
      setRecipientName("");
    }
  }, [profileData, recipientId]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    let name;
    let uomId;
    let quantity;

    for (const data of packageDetailArray) {
      name = data.packageName;
      uomId = data.uom;
      quantity = data.quantity;
    }

    if (
      senderLocation !== "" &&
      recipientLocation !== "" &&
      senderName !== "" &&
      recipientName !== "" &&
      priorityLevel !== "" &&
      date !== "" &&
      recipientId !== null &&
      name !== "" &&
      uomId !== "" &&
      quantity !== "" &&
      packageDetailArray.length > 0
    ) {
      try {
        setIsLoading(true);

        const data = {
          senderName: senderName,
          senderLocation,
          recipientId: recipientId?.id,
          recipientName: recipientName,
          recipientLocation,
          eta: dayjs(date).format("YYYY-MM-DD"),
          priority: priorityLevel,
          confidential: confidential === true ? 1 : 0,
          description,
        };

        const res = await createPackage(data);

        if (res?.status === 201) {
          const packageId = res?.result?.packageId;

          for (const data of packageDetailArray) {
            const packageData = {
              name: data.packageName,
              quantity: data.quantity,
              uomId: data.uom,
              notes: data.note,
            };
            await handleSubmitPackageDetail(packageId, packageData);
          }
        }

        navigate("/package/lists");
      } catch (err) {
        console.error(err?.response?.status);
      } finally {
        setIsLoading(false);
      }
    } else {
      senderLocation !== ""
        ? setIsSenderLocationError(false)
        : setIsSenderLocationError(true);
      recipientLocation !== ""
        ? setIsRecipientLocationError(false)
        : setIsRecipientLocationError(true);
      senderName !== ""
        ? setIsSenderNameError(false)
        : setIsSenderNameError(true);
      recipientName !== ""
        ? setIsRecipientNameError(false)
        : setIsRecipientNameError(true);
      priorityLevel !== ""
        ? setIsPriorityLevelError(false)
        : setIsPriorityLevelError(true);
      date !== "" ? setIsDateError(false) : setIsDateError(true);
      recipientId !== null
        ? setIsRecipientIdError(false)
        : setIsRecipientIdError(true);
      name !== "" ? setIsPackageNameError(false) : setIsPackageNameError(true);
      uomId !== "" ? setIsUomError(false) : setIsUomError(true);
      quantity !== "" ? setIsQuantityError(false) : setIsQuantityError(true);
      packageDetailArray.length > 0
        ? setIsPackageDetailEmpty(false)
        : setIsPackageDetailEmpty(true);
    }
  };

  const handleSubmitPackageDetail = async (detailPackage, packageId) => {
    try {
      const res = await createPackageDetail(detailPackage, packageId);

      if (res?.status !== 201) {
        console.error("Failed to submit package detail");
      }
    } catch (err) {
      console.error("Error :", err);
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", zIndex: 10, top: 0, bgcolor: "#fff" }}>
        <Header
          title="Add New Package"
          isNotificationsNeed={true}
          isHistoryNeed={true}
        />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate("/package/lists")} />
        </Box>
      </Box>

      <Box component="form" onSubmit={handleSubmit} sx={{ p: 2 }}>
        <Box
          sx={{
            border: "1px solid #bdbdbd",
            p: 2,
            borderRadius: "10px",
          }}
        >
          <div className="step">
            <div className="v-stepper">
              <div className="circle"></div>
              <div className="line"></div>
            </div>

            <div className="content" style={{ marginBottom: "4px" }}>
              <Typography
                sx={{ fontSize: "0.875rem", color: "#bdbdbd", fontWeight: 600 }}
              >
                From
              </Typography>
              <TextField
                value={senderLocation || ""}
                onChange={(e) => setSenderLocation(e.target.value)}
                fullWidth
                variant="standard"
                error={!senderLocation && isSenderLocationError}
                InputProps={{
                  disableUnderline: true,
                  style: {
                    fontSize: "14px",
                    color: isSenderLocationError && "#B80000",
                  },
                }}
                sx={{
                  mt: -0.5,
                }}
                placeholder="Input your location"
              />
            </div>
          </div>

          <Divider sx={{ ml: 2.8, mb: 1 }} />

          <div className="step">
            <div className="v-stepper">
              <div className="circle-grey"></div>
              <div className="line"></div>
            </div>

            <div className="content">
              <Typography
                sx={{ fontSize: "0.875rem", color: "#bdbdbd", fontWeight: 600 }}
              >
                To
              </Typography>
              <TextField
                value={recipientLocation || ""}
                onChange={(e) => setRecipientLocation(e.target.value)}
                fullWidth
                variant="standard"
                InputProps={{
                  disableUnderline: true,
                  style: {
                    fontSize: "14px",
                    color: isRecipientLocationError && "#B80000",
                    // fontWeight: 600,
                  },
                }}
                disabled={isDisabled}
                sx={{
                  mt: -0.5,
                }}
                placeholder="Input your destination"
              />
            </div>
          </div>
        </Box>

        <Box
          sx={{
            mt: 2,
            border: "1px solid #bdbdbd",
            borderRadius: "10px",
            p: 2,
          }}
        >
          <Box sx={{ mt: 2 }}>
            <Typography>
              Sender Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <CustomInput
              value={senderName || ""}
              placeholder="Enter the sender name"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isSenderNameError}
              isHelperText={true}
              onChange={(e) => {
                setSenderName(e.target.value);

                if (senderName) {
                  setIsSenderNameError(false);
                }
              }}
              helperText={
                isSenderNameError && (
                  <FormHelperText
                    sx={{
                      ml: 0,
                      fontSize: "11px",
                      color: "#78251E",
                    }}
                  >
                    Please input sender name
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Recipient <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <Autocomplete
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  borderRadius: "8px",
                  fontSize: "0.875rem",
                  padding: "5px 4px 7.5px 9px",
                },
              }}
              fullWidth
              value={recipientId}
              onChange={(event, newValue) => {
                setRecipientId(newValue);

                if (newValue) {
                  setIsDisabled(false);
                  setIsRecipientIdError(false);
                } else {
                  setIsDisabled(true);
                  setIsRecipientIdError(true);
                }
              }}
              options={recipientOptions}
              renderOption={(props, option) => (
                <MenuItem {...props} key={option.id}>
                  {option.label}
                </MenuItem>
              )}
              isOptionEqualToValue={(option, value) => option.id === value.id}
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Please input recipient"
                  error={!recipientId && isRecipientIdError}
                  sx={{
                    "& .MuiFormHelperText-root": {
                      marginLeft: 0,
                    },
                  }}
                  helperText={
                    isRecipientIdError && (
                      <span sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}>
                        Please input recipient
                      </span>
                    )
                  }
                />
              )}
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Recipient Name <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <CustomInput
              value={recipientName || ""}
              placeholder="Enter the recipient name"
              sx={{ fontSize: "0.875rem" }}
              isValueError={isRecipientNameError}
              isHelperText={true}
              disabled={isDisabled}
              onChange={(e) => {
                setRecipientName(e.target.value);

                if (recipientName) {
                  setIsRecipientNameError(false);
                }
              }}
              helperText={
                isRecipientNameError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                  >
                    Please input recipient name
                  </FormHelperText>
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Priority <span style={{ color: "#B80000" }}>*</span>
            </Typography>

            <FormControl
              fullWidth
              sx={{
                mt: 0.5,
                "& .MuiOutlinedInput-root": {
                  height: "44px",
                  // width: "7.188rem",
                  borderRadius: "8px",
                  fontSize: "0.875rem",
                },
              }}
            >
              <Select
                displayEmpty
                value={priorityLevel}
                onChange={(e) => {
                  setPriorityLevel(e.target.value);

                  if (priorityLevel) {
                    setIsPriorityLevelError(false);
                  }
                }}
                input={
                  <OutlinedInput
                    error={!priorityLevel && isPriorityLevelError}
                  />
                }
              >
                <MenuItem disabled value="">
                  <Typography sx={{ fontSize: "0.875rem", color: "#bdbdbd" }}>
                    Select the priority level
                  </Typography>
                </MenuItem>
                {enumPriority?.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.value}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.label}
                  </MenuItem>
                ))}
              </Select>
              {isPriorityLevelError && (
                <FormHelperText
                  sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                >
                  Please select the priority level
                </FormHelperText>
              )}
            </FormControl>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Typography>
              Estimate Time of Arrival{" "}
              <span style={{ color: "#B80000" }}>*</span>
            </Typography>
            <DemoContainer
              sx={{
                "& .MuiStack-root": {
                  overflow: "hidden !important",
                },
              }}
              components={["MobileDatePicker"]}
            >
              <DemoItem>
                <MobileDatePicker
                  fullWidth
                  sx={{
                    mt: 0.5,

                    "& .MuiOutlinedInput-root": {
                      height: "44px",
                      borderRadius: "8px",
                      fontSize: "0.875rem",
                    },
                  }}
                  value={dayjs(date)}
                  onChange={(newValue) => {
                    const formattedDate = newValue
                      ? dayjs(newValue).format("MM/DD/YYYY")
                      : "";
                    setDate(formattedDate);
                  }}
                />
              </DemoItem>
            </DemoContainer>
            {isDateError && (
              <Typography sx={{ fontSize: "11px", color: "#d50000" }}>
                Please select the date
              </Typography>
            )}
          </Box>

          <Box
            sx={{
              mt: 2,
            }}
          >
            <Typography>Description</Typography>
            <CustomDescriptionField
              placeholder="Write your description"
              sx={{ mt: 0.5 }}
              setValue={setDescription}
              value={description}
            />
          </Box>

          <Box
            sx={{
              mt: 2,
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Typography>Confidential</Typography>
            <FormGroup
              sx={{
                "& .MuiFormControlLabel-root": {
                  marginRight: 0,
                },
              }}
            >
              <FormControlLabel
                labelPlacement="end"
                label={
                  confidential === true ? (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Active
                    </Typography>
                  ) : (
                    <Typography sx={{ fontSize: "14px", ml: 1 }}>
                      Non Active
                    </Typography>
                  )
                }
                control={
                  <CustomSwitch
                    value={confidential}
                    setValue={setConfidential}
                  />
                }
              />
            </FormGroup>
          </Box>
        </Box>

        <AddNewPackageDetail
          packageDetailArray={packageDetailArray}
          setPackageDetailArray={setPackageDetailArray}
          uomLists={uomLists}
          isPackageNameError={isPackageNameError}
          setIsPackageNameError={setIsPackageNameError}
          isUomError={isUomError}
          setIsUomError={setIsUomError}
          isQuantityError={isQuantityError}
          setIsQuantityError={setIsQuantityError}
          isNoteError={isNoteError}
          setIsNoteError={setIsNoteError}
          isPackageDetailEmpty={isPackageDetailEmpty}
        />

        <CustomButtonSave isLoading={isLoading} content="Save" />
      </Box>
    </Box>
  );
};

export default AddNewPackage;
