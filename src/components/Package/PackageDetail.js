import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Typography,
  Button,
  DialogContent,
  Fade,
  Snackbar,
  Alert,
  Skeleton,
} from "@mui/material";
import {
  CloseRounded as CloseRoundedIcon,
  KeyboardArrowRightRounded as KeyboardArrowRightRoundedIcon,
  ContentCopyRounded as ContentCopyRoundedIcon,
} from "@mui/icons-material";
import BackButton from "../../shared/BackButton";
import Header from "../../shared/Header";
import PackageImage from "../../assets/package-icon.png";
import ImageIcon from "../../assets/image-icon.png";
import {
  cancelPackage,
  deletePackage,
  showConsignment,
  showPackage,
  showPackageAdmin,
  showPackageCourier,
  showPackageDetail,
  showPackageDetailCourier,
  showSettlement,
  submitPackage,
  viewPackageDetail,
  viewPackageDetailAdmin,
} from "../../api/package";
import NotFound from "../NotFound";
import dayjs from "dayjs";
import CustomDialog from "../../shared/CustomDialog";
import ShowDetailPackage from "./ShowDetailPackage";
import VerificationDialog from "./VerificationDialog";
import CopyToClipboard from "react-copy-to-clipboard";
import CollectedDialog from "./CollectedDialog";
import SettlementDialog from "./SettlementDialog";
import PackageStatus from "./PackageStatus";
import ConfirmDialog from "./ConfirmDialog";
import { AuthContext } from "../../contexts/AuthContext";
import { useReactToPrint } from "react-to-print";
import { PrintFile } from "./PrintFile";

const OrderDetail = ({ permissions, desktopView }) => {
  const { profileData, componentRef } = useContext(AuthContext);
  const navigate = useNavigate();
  const { id } = useParams();

  const [openSnackbar, setOpenSnackbar] = useState({
    openAlert: false,
    Transition: Fade,
    vertical: "top",
    horizontal: "center",
  });
  const { vertical, horizontal, openAlert, Transition } = openSnackbar;

  const handleClickSnackbar = (newState) => {
    setOpenSnackbar({ ...newState, openAlert: true, Transition });
  };

  const handleCloseSnackbar = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar({ ...openSnackbar, openAlert: false });
  };

  const [packageData, setPackageData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error404, setError404] = useState(false);
  const [open, setOpen] = useState(false);
  const [packageDetail, setPackagetDetail] = useState([]);

  const [openDialogDetailPackage, setOpenDialogDetailPackage] = useState(false);
  const [packageDetailId, setPackageDetailId] = useState(null);
  const [packageDetailData, setPackageDetailData] = useState(null);

  const [openDialogVerification, setOpenDialogVerification] = useState(false);
  const [openSubmit, setOpenSubmit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openCancel, setOpenCancel] = useState(false);
  const [permissionAllow, setPermissionAllow] = useState(false);

  const [openDialogCollected, setOpenDialogCollected] = useState(false);
  const [openDialogSettlement, setOpenDialogSettlement] = useState(false);
  const [openDialogConfirm, setOpenDialogConfirm] = useState(false);

  const getPackage = async (id) => {
    try {
      setIsLoading(true);

      const {
        result: {
          trackingId,
          eta: { date },
          createdAt: { date: createdDate },
          priority: {
            label: priorityLabel,
            icon: priorityIcon,
            color: priorityColor,
          },
          confidential,
          description,
          status: { label: statusLabel, icon: iconLabel, color: colorLabel },
          senderName,
          senderLocation,
          recipientName,
          recipientLocation,
          consignmentId,
          recipientId,
          packageId,
          senderId,
        },
      } = permissions.includes("package.admin")
        ? await showPackageAdmin(id)
        : permissions.includes("consign.courier")
        ? await showPackageCourier(id)
        : await showPackage(id);
      setPackageData({
        trackingId,
        date,
        priorityLabel,
        priorityIcon,
        priorityColor,
        confidential,
        description,
        statusLabel,
        iconLabel,
        colorLabel,
        senderName,
        senderLocation,
        recipientName,
        recipientLocation,
        consignmentId,
        recipientId,
        packageId,
        senderId,
        createdDate,
      });

      const res = permissions.includes("package.admin")
        ? await viewPackageDetailAdmin(id)
        : permissions.includes("consign.courier")
        ? await showPackageDetailCourier(id)
        : await viewPackageDetail(id);
      setPackagetDetail(res?.results?.data);
    } catch (err) {
      const errResponse = err?.response?.status;

      if (errResponse === 404) {
        setError404(true);
      }
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getPackage(id);
  }, [id]);

  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const handleDeletePackage = async () => {
    setIsLoadingDelete(true);
    await deletePackage(id)
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoadingDelete(false);
        setOpenDialogVerification(false);
        navigate("/package/lists");
      });
  };

  const [note, setNote] = useState("");
  const [isNoteError, setIsNoteError] = useState(false);
  const [isLoadingSubmitted, setIsLoadingSubmitted] = useState(false);

  const [isLoadingCancel, setIsLoadingCancel] = useState(false);

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,

    documentTitle: "Receipt",
    removeAfterPrint: true,
    onPrintError: (error) => console.error("Print Error:", error),
  });

  const handleSubmittedPackage = async () => {
    try {
      setIsLoadingSubmitted(true);

      const data = {
        notes: note,
        packageId: id,
      };
      await submitPackage(data);
      setIsLoadingSubmitted(false);
      setOpenDialogVerification(false);
      getPackage(id);
      setNote("");
      handlePrint();
    } catch (err) {
      console.error();
    }
  };

  const handleCancelPackage = async () => {
    if (note !== "") {
      setIsLoadingCancel(true);
      const data = {
        notes: note,
        packageId: id,
      };

      await cancelPackage(data)
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          setIsLoadingCancel(false);
          getPackage(id);
          setOpenDialogVerification(false);
          setNote("");
          setIsNoteError(false);
        });
    } else {
      note !== "" ? setIsNoteError(false) : setIsNoteError(true);
    }
  };

  const [consignmentData, setConsignmentData] = useState(null);
  const [settlementData, setSettlementData] = useState(null);

  useEffect(() => {
    if (packageData !== null && packageData?.consignmentId !== null) {
      const getShowConsignment = async () => {
        try {
          const { result } = await showConsignment(packageData?.consignmentId);
          setConsignmentData(result || null);
        } catch (err) {
          console.error(err);
        }
      };

      getShowConsignment(packageData?.consignmentId);
    }
  }, [packageData]);

  useEffect(() => {
    if (consignmentData !== null && consignmentData?.settlementId !== null) {
      const getShowSettlement = async () => {
        try {
          const { result } = await showSettlement(
            consignmentData?.settlementId
          );
          setSettlementData(result);
        } catch (err) {
          console.error(err);
        }
      };

      getShowSettlement();
    }
  }, [consignmentData]);

  return error404 ? (
    <NotFound />
  ) : (
    <Box sx={{ mb: 1 }}>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header
          title="Package Detail"
          isNotificationsNeed={true}
          isHistoryNeed={true}
        />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate(-1)} />
        </Box>
      </Box>

      {isLoading && (
        <Box sx={{ p: 2 }}>
          <Skeleton variant="rounded" height="500px" animation="wave" />
        </Box>
      )}

      {!isLoading && (
        <Box sx={{ p: 2 }}>
          <Box
            sx={{
              border: "1px solid #bdbdbd",
              py: 3,
              px: 2,
              borderRadius: "10px",
            }}
          >
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Box
                sx={{
                  border: `1px solid #${packageData?.priorityColor}`,
                  py: 0.5,
                  px: 1,
                  borderRadius: "50px",
                  fontWeight: 600,
                  display: "flex",
                  alignItems: "center",
                  gap: 1,
                }}
              >
                <i
                  className={packageData?.priorityIcon}
                  style={{
                    fontSize: "12px",
                    color: `#${packageData?.priorityColor}`,
                  }}
                ></i>
                <Typography
                  sx={{
                    fontSize: "0.75rem",

                    color: `#${packageData?.priorityColor}`,
                  }}
                >
                  {packageData?.priorityLabel}
                </Typography>
              </Box>
            </Box>

            <Box
              sx={{ display: "flex", gap: 2, alignItems: "flex-start", mt: 1 }}
            >
              <Box component="img" src={PackageImage} />
              <Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                  <Typography sx={{ fontWeight: 600 }}>
                    {packageData?.trackingId}
                  </Typography>
                  <CopyToClipboard text={packageData?.trackingId}>
                    <ContentCopyRoundedIcon
                      sx={{ fontSize: "1rem", color: "#bdbdbd" }}
                      onClick={() =>
                        handleClickSnackbar({
                          vertical: "top",
                          horizontal: "center",
                        })
                      }
                    />
                  </CopyToClipboard>

                  <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={openAlert}
                    onClose={handleCloseSnackbar}
                    message="I love snacks"
                    key={vertical + horizontal}
                    autoHideDuration={1000}
                  >
                    <Alert
                      onClose={handleCloseSnackbar}
                      severity="success"
                      variant="filled"
                      sx={{ width: "100%" }}
                    >
                      Copied
                    </Alert>
                  </Snackbar>
                </Box>

                <Typography sx={{ fontSize: "0.75rem" }}>
                  {dayjs(packageData?.createdDate).format("DD MMM YYYY")} -{" "}
                  {dayjs(packageData?.date).format("DD MMM YYYY")}
                </Typography>
              </Box>
            </Box>

            <Box sx={{ mt: 2, ml: 4 }}>
              <div className="step">
                <div className="v-stepper">
                  <div className="circle"></div>
                  <div className="line"></div>
                </div>

                <div className="content" style={{ marginBottom: "25px" }}>
                  <div
                    style={{
                      color: "#bdbdbd",
                      fontWeight: 600,
                      fontSize: "0.875rem",
                    }}
                  >
                    From
                  </div>
                  <div
                    style={{
                      color: "#000",
                      fontWeight: 600,
                      fontSize: "0.875rem",
                    }}
                  >
                    {packageData?.senderName}
                  </div>
                  <div
                    style={{
                      color: "#000",
                      fontWeight: 500,
                      fontSize: "0.75rem",
                    }}
                  >
                    {packageData?.senderLocation}
                  </div>
                </div>
              </div>

              <div className="step">
                <div className="v-stepper">
                  <div className="circle-grey"></div>
                  <div className="line"></div>
                </div>

                <div
                  className="content"
                  style={{ marginBottom: "25px", marginTop: "4px" }}
                >
                  <div
                    style={{
                      color: "#bdbdbd",
                      fontWeight: 600,
                      fontSize: "0.875rem",
                    }}
                  >
                    Shipper To
                  </div>
                  <div
                    style={{
                      color: "#000",
                      fontWeight: 600,
                      fontSize: "0.875rem",
                    }}
                  >
                    {packageData?.recipientId === profileData?.userId
                      ? "You"
                      : packageData?.recipientName}
                  </div>
                  <div
                    style={{
                      color: "#000",
                      fontWeight: 500,
                      fontSize: "0.75rem",
                    }}
                  >
                    {packageData?.recipientLocation}
                  </div>
                </div>
              </div>
            </Box>

            <PackageStatus
              packageData={packageData}
              consignmentData={consignmentData}
              settlementData={settlementData}
            />

            <Box sx={{ mt: 2 }}>
              <Typography sx={{ fontWeight: 600 }}>Package Detail</Typography>

              {packageDetail?.length === 0 ? (
                <Typography sx={{ fontSize: "0.75rem" }}>
                  This package marked as CONFIDENTIAL. You're not allowed to see
                  the package
                </Typography>
              ) : (
                <Box
                  sx={{
                    mt: 1,
                    display: "flex",
                    flexDirection: "column",
                    gap: 1,
                  }}
                >
                  {packageDetail?.map((item, index) => (
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        gap: 2,
                        border: "1px solid #bdbdbd",
                        p: 1,
                        borderRadius: "8px",
                      }}
                      key={index}
                      onClick={() => {
                        if (!permissions.includes("consign.courier")) {
                          setOpenDialogDetailPackage(true);
                          setPackageDetailId(item?.packageDetailId);
                        }
                      }}
                    >
                      <Box>
                        <Typography sx={{ fontSize: "0.875rem" }}>
                          {item?.name}
                        </Typography>
                        <Typography
                          sx={{ fontSize: "0.75rem", color: "#bdbdbd" }}
                        >
                          {item?.quantity} {item?.uom?.name}
                        </Typography>
                      </Box>

                      {!permissions.includes("consign.courier") && (
                        <KeyboardArrowRightRoundedIcon
                          sx={{ color: "#78251E" }}
                        />
                      )}
                    </Box>
                  ))}
                </Box>
              )}
            </Box>
          </Box>

          {(packageData?.statusLabel === "Created" ||
            packageData?.statusLabel === "Edited") &&
            permissions.includes("package.update") &&
            packageData?.senderId === profileData?.userId && (
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 2,
                  mt: 2,
                }}
              >
                <Button
                  sx={{
                    width: "100%",
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                    borderRadius: "8px",

                    ":hover": {
                      bgcolor: "#78251E",
                      border: `1px solid #78251E`,
                    },
                    color: "#fff",
                    textTransform: "capitalize",
                  }}
                  onClick={() => navigate(`/package/edit/${id}`)}
                >
                  Edit
                </Button>

                <Button
                  sx={{
                    width: "100%",
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                    borderRadius: "8px",

                    ":hover": {
                      bgcolor: "#78251E",
                      border: `1px solid #78251E`,
                    },
                    color: "#fff",
                    textTransform: "capitalize",
                  }}
                  onClick={() => {
                    setOpenDialogVerification(true);
                    setOpenSubmit(true);
                    setOpenDelete(false);
                    setOpenCancel(false);
                  }}
                >
                  Submit
                </Button>

                <Button
                  sx={{
                    width: "100%",
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                    borderRadius: "8px",

                    ":hover": {
                      bgcolor: "#78251E",
                      border: `1px solid #78251E`,
                    },
                    color: "#fff",
                    textTransform: "capitalize",
                  }}
                  onClick={() => {
                    setOpenDialogVerification(true);
                    setOpenDelete(false);
                    setOpenSubmit(false);
                    setOpenCancel(true);
                  }}
                >
                  Cancel
                </Button>
              </Box>
            )}

          {(packageData?.statusLabel === "Undelivered" ||
            packageData?.statusLabel === "Confirmed" ||
            packageData?.statusLabel === "Cancelled") &&
            permissions.includes("package.admin") && (
              <Button
                sx={{
                  width: "100%",
                  bgcolor: "#78251E",
                  border: `1px solid #78251E`,
                  mt: 2,
                  borderRadius: "8px",

                  ":hover": {
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                  },
                  color: "#fff",
                  textTransform: "capitalize",
                }}
                onClick={() => {
                  setOpenDialogVerification(true);
                  if (permissions.includes("package.admin")) {
                    setOpenDelete(true);
                    setPermissionAllow(false);
                  } else {
                    setPermissionAllow(true);
                    setOpenDelete(false);
                  }
                  setOpenSubmit(false);
                  setOpenCancel(false);
                }}
              >
                Delete
              </Button>
            )}

          {permissions.includes("consign.courier") &&
            packageData?.statusLabel === "Collected" && (
              <Button
                sx={{
                  width: "100%",
                  bgcolor: "#78251E",
                  border: `1px solid #78251E`,
                  mt: 2,
                  borderRadius: "8px",
                  ":hover": {
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                  },
                  color: "#fff",
                  textTransform: "capitalize",
                }}
                onClick={() => setOpenDialogSettlement(true)}
              >
                Delivered
              </Button>
            )}

          {permissions.includes("consign.courier") &&
            packageData?.statusLabel === "Submitted" && (
              <Button
                sx={{
                  width: "100%",
                  bgcolor: "#78251E",
                  border: `1px solid #78251E`,
                  mt: 2,
                  borderRadius: "8px",
                  ":hover": {
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                  },
                  color: "#fff",
                  textTransform: "capitalize",
                }}
                onClick={() => setOpenDialogCollected(true)}
              >
                Collected
              </Button>
            )}

          {permissions.includes("package.update") &&
            packageData?.statusLabel === "Delivered" &&
            profileData?.userId === packageData?.recipientId && (
              <Button
                sx={{
                  width: "100%",
                  bgcolor: "#78251E",
                  border: `1px solid #78251E`,
                  mt: 2,
                  borderRadius: "8px",
                  ":hover": {
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                  },
                  color: "#fff",
                  textTransform: "capitalize",
                }}
                onClick={() => setOpenDialogConfirm(true)}
              >
                Confirm
              </Button>
            )}
        </Box>
      )}

      <CustomDialog open={open}>
        <Box
          onClick={() => setOpen(false)}
          sx={{ display: "flex", justifyContent: "flex-end", pr: 2, pt: 2 }}
        >
          <CloseRoundedIcon />
        </Box>
        <DialogContent>
          <Box
            noValidate
            component="form"
            sx={{
              display: "flex",
              flexDirection: "column",
              m: "auto",
              width: "fit-content",
            }}
          >
            <Box component="img" src={ImageIcon} />
          </Box>
        </DialogContent>
      </CustomDialog>

      <VerificationDialog
        open={openDialogVerification}
        setOpen={setOpenDialogVerification}
        openSubmit={openSubmit}
        openDelete={openDelete}
        handleDeletePackage={handleDeletePackage}
        note={note}
        setNote={setNote}
        handleSubmittedPackage={handleSubmittedPackage}
        openCancel={openCancel}
        handleCancelPackage={handleCancelPackage}
        isNoteError={isNoteError}
        setIsNoteError={setIsNoteError}
        permissionAllow={permissionAllow}
        isLoadingSubmitted={isLoadingSubmitted}
        isLoadingCancel={isLoadingCancel}
        isLoadingDelete={isLoadingDelete}
      />

      <ShowDetailPackage
        openDialogDetailPackage={openDialogDetailPackage}
        setOpenDialogDetailPackage={setOpenDialogDetailPackage}
        setPackageDetailId={setPackageDetailId}
        packageDetailData={packageDetailData}
        packageDetailId={packageDetailId}
        showPackageDetail={showPackageDetail}
        setPackageDetailData={setPackageDetailData}
        id={id}
        permissions={permissions}
      />

      <CollectedDialog
        open={openDialogCollected}
        setOpen={setOpenDialogCollected}
        id={id}
        trackingId={packageData?.trackingId}
        getPackage={getPackage}
        desktopView={desktopView}
        setIsLoadingPackage={setIsLoading}
      />

      <SettlementDialog
        open={openDialogSettlement}
        setOpen={setOpenDialogSettlement}
        trackingId={packageData?.trackingId}
        consignmentId={packageData?.consignmentId}
        getPackage={getPackage}
        id={id}
        desktopView={desktopView}
        setIsLoadingPackage={setIsLoading}
      />

      <ConfirmDialog
        open={openDialogConfirm}
        setOpen={setOpenDialogConfirm}
        consignmentId={packageData?.consignmentId}
        getPackage={getPackage}
        settlementId={consignmentData?.settlementId}
        id={id}
      />

      <div style={{ display: "none" }}>
        <div ref={componentRef}>
          <PrintFile packageData={packageData} />
        </div>
      </div>
    </Box>
  );
};

export default OrderDetail;
