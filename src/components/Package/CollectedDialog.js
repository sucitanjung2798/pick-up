import React, { useEffect, useRef, useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Typography,
} from "@mui/material";
import {
  CloseRounded as CloseRoundedIcon,
  AddRounded as AddRoundedIcon,
} from "@mui/icons-material";
import CustomDialog from "../../shared/CustomDialog";
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";
import CustomDescriptionField from "../../shared/CustomDescriptionField";
import axios from "axios";
import Compressor from "compressorjs";

const CollectedDialog = ({
  open,
  setOpen,
  id,
  trackingId,
  getPackage,
  desktopView,
}) => {
  const inputRef = useRef();
  const [photo, setPhoto] = useState(null);
  const [blobImage, setBlobImage] = useState(null);

  const [description, setDescription] = useState("");

  const [isLoading, setIsLoading] = useState(false);
  const [isPhotoEmpty, setIsPhotoEmpty] = useState(false);
  const [errMessage, setErrMessage] = useState(null);

  const [openOptionCamera, setOpenOptionCamera] = useState(false);

  const handleOpenGalery = (e) => {
    setBlobImage(e.target.files[0]);
    setPhoto(e.target.files[0]);
  };

  const handleUploadClick = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  const handleInputChange = (event) => {
    handleOpenGalery(event);
  };

  const handleImage = async (image) => {
    const response = await fetch(image.url);
    const blob = await response.blob();
    setBlobImage(
      new Blob([blob], { type: blob.type }),
      `image${trackingId}.${blob.type.split("/")[1]}`
    );
  };

  const takePhoto = async () => {
    try {
      const image = await Camera.getPhoto({
        quality: 10,
        allowEditing: true,
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
      });

      const response = await fetch(image.webPath);
      const blob = await response.blob();

      new Compressor(blob, {
        quality: 0.9,
        maxWidth: 1000,
        success(blob) {
          handleImage({
            url: URL.createObjectURL(blob),
            size: blob.size,
            type: blob.type,
          });
        },
      });

      setPhoto(blob);
    } catch (error) {
      if (error.name === "CameraCanceledError") {
        console.error("User cancelled the photo-taking process.");
      } else {
        console.error("Error taking photo:", error);
      }
    }
  };

  const handleSubmitCollected = async () => {
    if (photo !== null) {
      setIsLoading(true);

      const formData = new FormData();

      formData.append("proof", blobImage);
      formData.append("package_id", id);
      formData.append("description", description);

      try {
        await axios.post("/api/v1/consignments", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
          },
        });
        setOpen(false);
        getPackage(id);
      } catch (err) {
        const errStatus = err?.response?.status;

        if (errStatus === 400) {
          setErrMessage("Image too large");
        }
        setIsLoading(false);
      } finally {
        setIsLoading(false);
      }
    } else {
      photo !== null ? setIsPhotoEmpty(false) : setIsPhotoEmpty(true);
    }
  };

  useEffect(() => {
    if (photo) {
      setOpenOptionCamera(false);
    }
  }, [photo]);

  return (
    <CustomDialog open={open}>
      <Box
        onClick={() => {
          setOpen(false);
          setPhoto(null);
          setIsPhotoEmpty(false);
          setErrMessage(null);
          setOpenOptionCamera(false);
        }}
        sx={{ display: "flex", justifyContent: "flex-end", pr: 2, pt: 2 }}
      >
        <CloseRoundedIcon />
      </Box>
      <DialogContent>
        <Box noValidate component="form">
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              m: "auto",
              width: "fit-content",
            }}
          >
            {!openOptionCamera && !photo && (
              <Box
                sx={{
                  border: `2px dashed ${isPhotoEmpty ? "#B80000" : "#bdbdbd"}`,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "200px",
                  height: "200px",
                  borderRadius: "8px",
                }}
                onClick={() => setOpenOptionCamera(true)}
              >
                <AddRoundedIcon
                  sx={{
                    fontSize: "30px",
                    color: isPhotoEmpty ? "#B80000" : "#bdbdbd",
                  }}
                />
              </Box>
            )}

            {!openOptionCamera && photo && (
              <img
                src={photo && URL.createObjectURL(photo)}
                alt="Foto User"
                width="200px"
                height="200px"
                style={{ borderRadius: "10px" }}
                onClick={() => setOpenOptionCamera(true)}
              />
            )}

            {openOptionCamera && (
              <Box
                sx={{
                  border: `2px dashed ${isPhotoEmpty ? "#B80000" : "#bdbdbd"}`,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                  width: "200px",
                  height: "200px",
                  borderRadius: "8px",
                  backdropFilter: "blur(3px)",
                  backgroundColor: "rgba(0,0,30,0.4)",
                }}
              >
                <Box onClick={() => setOpenOptionCamera(false)}>
                  <CloseRoundedIcon
                    sx={{
                      fontSize: "30px",
                      color: "#fff",
                      position: "absolute",
                      top: 5,
                      right: 5,
                    }}
                  />
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                    gap: 2,
                  }}
                >
                  <Button
                    sx={{
                      textTransform: "capitalize",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                      color: "#fff",
                      width: "100px",
                    }}
                    onClick={() => {
                      takePhoto();
                    }}
                    disabled={desktopView ? false : true}
                  >
                    Camera
                  </Button>
                  <input
                    ref={inputRef}
                    type="file"
                    onChange={handleInputChange}
                    accept="image/*"
                    style={{ display: "none" }}
                  />
                  <Button
                    sx={{
                      textTransform: "capitalize",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                      color: "#fff",
                      width: "100px",
                    }}
                    onClick={() => {
                      handleUploadClick();
                    }}
                  >
                    Upload
                  </Button>
                </Box>
              </Box>
            )}

            {isPhotoEmpty && (
              <Typography sx={{ color: "#B80000", mt: 1, textAlign: "center" }}>
                Photo is required
              </Typography>
            )}

            {errMessage && (
              <Typography
                sx={{
                  color: "#B80000",
                  mt: 1,
                  textAlign: "center",
                  fontSize: "0.75rem",
                }}
              >
                {errMessage}
                <br /> Max 4MB
              </Typography>
            )}
          </Box>

          <Box
            sx={{
              mt: 2,
            }}
          >
            <Typography>Description</Typography>
            <CustomDescriptionField
              placeholder="Write your description"
              sx={{ mt: 0.5 }}
              setValue={setDescription}
              value={description}
            />
          </Box>
        </Box>

        <Button
          sx={{
            backgroundColor: "#78251E",
            border: "1px solid #78251E",
            color: "#fff",
            textTransform: "capitalize",
            px: 2,
            borderRadius: "8px",
            display: "flex",
            gap: 1,
            mt: 2,
            alignItems: "center",
            ":hover": {
              bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
              color: "#ffffff",
              border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
            },
          }}
          disabled={isLoading ? true : false}
          autoFocus
          onClick={handleSubmitCollected}
          fullWidth
        >
          {isLoading && (
            <CircularProgress
              sx={{ color: "grey" }}
              thickness={4}
              size={20}
              disableShrink
            />
          )}
          <Typography sx={{ fontSize: "0.875rem" }}>Save</Typography>
        </Button>
      </DialogContent>
    </CustomDialog>
  );
};

export default CollectedDialog;
