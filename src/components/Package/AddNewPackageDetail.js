import React from "react";
import {
  Box,
  IconButton,
  MenuItem,
  Typography,
  FormHelperText,
} from "@mui/material";
import {
  AddRounded as AddRoundedIcon,
  DeleteOutlineOutlined as DeleteOutlineOutlinedIcon,
} from "@mui/icons-material";
import CustomInput from "../../shared/CustomInput";
import CustomSelect from "../../shared/CustomSelect";

const AddNewPackageDetail = ({
  packageDetailArray,
  setPackageDetailArray,
  uomLists,
  isPackageNameError,
  setIsPackageNameError,
  isUomError,
  setIsUomError,
  isQuantityError,
  setIsQuantityError,
  isNoteError,
  setIsNoteError,
  isPackageDetailEmpty,
}) => {
  const handleChangePackageDetail = (index, e) => {
    const { name, value } = e.target;
    const newPackageDetail = [...packageDetailArray];
    packageDetailArray[index][name] = value;
    setPackageDetailArray(newPackageDetail);
  };

  const handleAddInput = () => {
    setPackageDetailArray([
      ...packageDetailArray,
      { packageName: "", uom: "", quantity: "", note: "" },
    ]);
  };

  const handleRemoveData = (index) => {
    const newPackageDetail = [...packageDetailArray];
    newPackageDetail.splice(index, 1);
    setPackageDetailArray(newPackageDetail);
  };

  const renderInputs = () => {
    return packageDetailArray.map((item, index) => {
      return (
        <Box key={index}>
          <Box
            sx={{ display: "flex", justifyContent: "flex-end" }}
            onClick={() => handleRemoveData(index)}
          >
            <DeleteOutlineOutlinedIcon sx={{ color: "#78251E", mb: 1 }} />
          </Box>
          <Box
            sx={{
              border: "1px solid #bdbdbd",
              mb: 1,
              borderRadius: "10px",
              p: 2,
            }}
          >
            <Box>
              <Typography>
                Package Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={item.packageName || ""}
                placeholder="Enter the package name"
                sx={{ fontSize: "0.875rem" }}
                isValueError={isPackageNameError}
                name="packageName"
                isHelperText={true}
                onChange={(e) => {
                  handleChangePackageDetail(index, e);

                  if (item.packageName) {
                    setIsPackageNameError(false);
                  }
                }}
                helperText={
                  isPackageNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input package name
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 1 }}>
              <Typography>
                UOM <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomSelect
                value={item.uom || ""}
                isValueError={isUomError}
                isHelperText={true}
                name="uom"
                onChange={(e) => {
                  handleChangePackageDetail(index, e);

                  if (item.uom) {
                    setIsUomError(false);
                  }
                }}
                data={uomLists.map((option, index) => (
                  <MenuItem
                    key={index}
                    value={option.uomId}
                    sx={{ fontSize: "0.875rem" }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
                placeholder="Please select the uom"
                helperText={
                  isUomError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please select the uom name
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 1 }}>
              <Typography>
                Quantity <span style={{ color: "#B80000" }}>*</span>
              </Typography>

              <CustomInput
                value={item.quantity || ""}
                placeholder="Please input the quantity"
                sx={{ fontSize: "0.875rem" }}
                onChange={(e) => {
                  handleChangePackageDetail(index, e);

                  if (item.quantity) {
                    setIsQuantityError(false);
                  }
                }}
                name="quantity"
                type="number"
                isValueError={isQuantityError}
                isHelperText={true}
                helperText={
                  isQuantityError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input quantity
                    </FormHelperText>
                  )
                }
              />
            </Box>

            <Box sx={{ mt: 1 }}>
              <Typography>Note</Typography>

              <CustomInput
                value={item.note || ""}
                placeholder="Please input the note"
                sx={{ fontSize: "0.875rem" }}
                onChange={(e) => {
                  handleChangePackageDetail(index, e);

                  if (item.note) {
                    setIsNoteError(false);
                  }
                }}
                name="note"
                type="text"
                isValueError={isNoteError}
                isHelperText={true}
                helperText={
                  isNoteError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#78251E" }}
                    >
                      Please input the note
                    </FormHelperText>
                  )
                }
              />
            </Box>
          </Box>
        </Box>
      );
    });
  };

  return (
    <Box sx={{ mt: 2 }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          gap: 0.5,
          justifyContent: "space-between",
        }}
      >
        <Typography>Package Detail</Typography>
        <IconButton
          size="small"
          sx={{ border: "1px solid #bdbdbd", borderRadius: "8px" }}
          onClick={handleAddInput}
        >
          <AddRoundedIcon
            sx={{ width: "15px", height: "15px", color: "#000000" }}
          />
        </IconButton>
      </Box>

      {packageDetailArray?.length === 0 && !isPackageDetailEmpty && (
        <Typography sx={{ mt: 1, fontSize: "0.875rem" }}>
          Package detail is empty
        </Typography>
      )}

      {isPackageDetailEmpty && (
        <Typography sx={{ mt: 1, fontSize: "0.875rem", color: "#d50000" }}>
          Package detail cannot be empty
        </Typography>
      )}

      {packageDetailArray?.length > 0 && (
        <Box
          sx={{
            border: "1px solid #bdbdbd",
            mt: 1,
            borderRadius: "10px",
            p: 2,
          }}
        >
          {renderInputs()}
        </Box>
      )}
    </Box>
  );
};

export default AddNewPackageDetail;
