import React, { useState } from "react";
import {
  Button,
  CircularProgress,
  DialogActions,
  DialogContent,
  Typography,
} from "@mui/material";
import CustomDialog from "../../shared/CustomDialog";
import CustomDescriptionField from "../../shared/CustomDescriptionField";

const VerificationDialog = ({
  open,
  setOpen,
  openSubmit,
  openDelete,
  handleDeletePackage,
  note,
  setNote,
  handleSubmittedPackage,
  openCancel,
  handleCancelPackage,
  isNoteError,
  setIsNoteError,
  permissionAllow,
  isLoadingSubmitted,
  isLoadingDelete,
  isLoadingCancel,
}) => {
  const [isNoteAdded, setIsNoteAdded] = useState(false);

  return (
    <CustomDialog open={open}>
      {permissionAllow && (
        <>
          <DialogContent sx={{ mt: 1 }}>
            <Typography sx={{ fontSize: "0.875rem", textAlign: "left" }}>
              You don't have permissions to delete this package
            </Typography>
          </DialogContent>

          <DialogActions sx={{ px: 2 }}>
            <Button
              sx={{
                backgroundColor: "#78251E",
                border: "1px solid #78251E",
                color: "#fff",
                textTransform: "capitalize",
                px: 2,
                borderRadius: "8px",
                display: "flex",
                gap: 1,
                alignItems: "center",
                ":hover": {
                  bgcolor: "#78251E",
                  color: "#ffffff",
                  border: `1px solid #78251E`,
                },
              }}
              autoFocus
              size="small"
              onClick={() => setOpen(false)}
            >
              Close
            </Button>
          </DialogActions>
        </>
      )}

      {openSubmit && (
        <DialogContent sx={{ mt: 1 }}>
          <Typography sx={{ fontSize: "0.875rem", textAlign: "justify" }}>
            Are you sure want to submit this package? If you submit this
            package, you cannot edit this package anymore
          </Typography>

          <Typography
            onClick={() => setIsNoteAdded(true)}
            sx={{ color: "#1769aa", mt: 1, fontSize: "0.875rem" }}
          >
            + Add Note
          </Typography>

          {isNoteAdded && (
            <CustomDescriptionField
              value={note}
              setValue={setNote}
              sx={{ mt: 0.5 }}
            />
          )}
        </DialogContent>
      )}

      {openDelete && (
        <DialogContent sx={{ mt: 1 }}>
          <Typography sx={{ fontSize: "0.875rem", textAlign: "justify" }}>
            Are you sure want to delete this package?
          </Typography>
        </DialogContent>
      )}

      {openCancel && (
        <DialogContent sx={{ mt: 1 }}>
          <Typography sx={{ fontSize: "0.875rem", textAlign: "justify" }}>
            Are you sure want to cancel this package?
          </Typography>

          <Typography
            sx={{ fontSize: "0.875rem", textAlign: "justify", mt: 1 }}
          >
            Please write your reason why you canceled the delivery of this
            package
          </Typography>

          <CustomDescriptionField
            value={note}
            setValue={setNote}
            sx={{ mt: 0.5 }}
            placeholder="Please input the reason"
            onChange={() => {
              if (note) {
                setIsNoteError(false);
              } else {
                setIsNoteError(true);
              }
            }}
          />
          {isNoteError && (
            <span style={{ fontSize: "0.75rem", color: "#B80000" }}>
              Please input the note
            </span>
          )}
        </DialogContent>
      )}

      {openSubmit && (
        <DialogActions sx={{ px: 2 }}>
          <Button
            sx={{
              border: "1px solid #78251E",
              textTransform: "capitalize",
              color: "#000000",
              px: 2,
              borderRadius: "8px",
              mr: 1,
            }}
            size="small"
            onClick={() => {
              setOpen(false);
              setIsNoteAdded(false);
              setNote("");
            }}
          >
            Cancel
          </Button>
          <Button
            sx={{
              backgroundColor: "#78251E",
              border: "1px solid #78251E",
              color: "#fff",
              textTransform: "capitalize",
              px: 2,
              borderRadius: "8px",
              display: "flex",
              gap: 1,
              alignItems: "center",
              ":hover": {
                bgcolor: !isLoadingSubmitted ? "#78251E" : "#bdbdbd",
                color: "#ffffff",
                border: `1px solid ${
                  !isLoadingSubmitted ? "#78251E" : "#bdbdbd"
                } `,
              },
            }}
            disabled={isLoadingSubmitted ? true : false}
            autoFocus
            size="small"
            onClick={handleSubmittedPackage}
          >
            {isLoadingSubmitted && (
              <CircularProgress
                sx={{ color: "grey" }}
                thickness={4}
                size={20}
                disableShrink
              />
            )}
            <Typography sx={{ fontSize: "0.875rem" }}>Submit</Typography>
          </Button>
        </DialogActions>
      )}

      {openDelete && (
        <DialogActions sx={{ px: 2 }}>
          <Button
            sx={{
              border: "1px solid #78251E",
              textTransform: "capitalize",
              color: "#000000",
              px: 2,
              borderRadius: "8px",
              mr: 1,
            }}
            size="small"
            onClick={() => {
              setOpen(false);
            }}
          >
            Cancel
          </Button>
          <Button
            sx={{
              backgroundColor: "#78251E",
              border: "1px solid #78251E",
              color: "#fff",
              textTransform: "capitalize",
              px: 2,
              borderRadius: "8px",
              display: "flex",
              gap: 1,
              alignItems: "center",
              ":hover": {
                bgcolor: !isLoadingDelete ? "#78251E" : "#bdbdbd",
                color: "#ffffff",
                border: `1px solid ${
                  !isLoadingDelete ? "#78251E" : "#bdbdbd"
                } `,
              },
            }}
            disabled={isLoadingDelete ? true : false}
            autoFocus
            size="small"
            onClick={handleDeletePackage}
          >
            {isLoadingDelete && (
              <CircularProgress
                sx={{ color: "grey" }}
                thickness={4}
                size={20}
                disableShrink
              />
            )}
            <Typography sx={{ fontSize: "0.875rem" }}>Delete</Typography>
          </Button>
        </DialogActions>
      )}

      {openCancel && (
        <DialogActions sx={{ px: 2 }}>
          <Button
            sx={{
              border: "1px solid #78251E",
              textTransform: "capitalize",
              color: "#000000",
              px: 2,
              borderRadius: "8px",
              mr: 1,
            }}
            size="small"
            onClick={() => {
              setOpen(false);
              setNote("");
              setIsNoteError(false);
            }}
          >
            Close
          </Button>
          <Button
            sx={{
              backgroundColor: "#78251E",
              border: "1px solid #78251E",
              color: "#fff",
              textTransform: "capitalize",
              px: 2,
              borderRadius: "8px",
              display: "flex",
              gap: 1,
              alignItems: "center",
              ":hover": {
                bgcolor: !isLoadingCancel ? "#78251E" : "#bdbdbd",
                color: "#ffffff",
                border: `1px solid ${
                  !isLoadingCancel ? "#78251E" : "#bdbdbd"
                } `,
              },
            }}
            disabled={isLoadingCancel ? true : false}
            autoFocus
            size="small"
            onClick={handleCancelPackage}
          >
            {isLoadingCancel && (
              <CircularProgress
                sx={{ color: "grey" }}
                thickness={4}
                size={20}
                disableShrink
              />
            )}
            <Typography sx={{ fontSize: "0.875rem" }}>Cancel</Typography>
          </Button>
        </DialogActions>
      )}
    </CustomDialog>
  );
};

export default VerificationDialog;
