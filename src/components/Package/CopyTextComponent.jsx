import React, { useState } from "react";
import { Button } from "@mui/material";

function CopyTextComponent({ text }) {
  const [copied, setCopied] = useState(false);

  const copyToClipboard = (text) => {
    navigator.clipboard.writeText(text);
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 1500);
  };

  return (
    <Button
      variant="contained"
      color="primary"
      onClick={() => copyToClipboard("Halo, this is me")}
    >
      {copied ? "Copied!" : "Copy"}
    </Button>
  );
}

function App() {
  return <CopyTextComponent text="Text yang ingin di copy" />;
}

export default App;
