import React, { useEffect, useState } from "react";
import { Box, Drawer, Typography, Button } from "@mui/material";
import {
  CloseRounded as CloseRoundedIcon,
  TuneRounded as TuneRoundedIcon,
} from "@mui/icons-material";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { MobileDatePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import ListStatusDelivery from "../../shared/ListStatusDelivery";
import { packageStatusList, priorityLevel } from "../../api/enum";

const FilterDialog = ({
  setStartDateSave,
  setEndDateSave,
  setStatusSave,
  status,
  setStatus,
  priority,
  setPriority,
  setPrioritySave,
  setStartCreatedDateSave,
  setEndCreatedDateSave,
}) => {
  const [filterDialog, setFilterDialog] = useState({ bottom: false });
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [startCreatedDate, setStartCreatedDate] = useState(null);
  const [endCreatedDate, setEndCreatedDate] = useState(null);

  const toggleDrawer = (anchor, open) => () => {
    setFilterDialog({ ...filterDialog, [anchor]: open });
  };

  const [listStatus, setListStatus] = useState([]);
  const [listPriority, setListPriority] = useState([]);

  const getListPackageStatus = async () => {
    try {
      const { results } = await packageStatusList();
      setListStatus(results);
    } catch (err) {
      console.error(err);
    }
  };

  const getListPriority = async () => {
    try {
      const { results } = await priorityLevel();
      setListPriority(results);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getListPackageStatus();
    getListPriority();
  }, []);

  return (
    <>
      <Box
        sx={{
          border: "1px solid #bdbdbd",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          px: 0.8,
          py: 0.8,
          borderRadius: 2,
        }}
        onClick={toggleDrawer("bottom", true)}
      >
        <TuneRoundedIcon />
      </Box>

      <Drawer anchor={"bottom"} open={filterDialog["bottom"]}>
        <Box sx={{ width: "auto" }} role="presentation">
          <Box sx={{ p: 2 }}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography sx={{ fontWeight: 600 }}>Filter</Typography>
              <Box>
                <CloseRoundedIcon
                  sx={{ fontSize: "18px" }}
                  onClick={toggleDrawer("bottom", false)}
                />
              </Box>
            </Box>

            <Box sx={{ mt: 1 }}>
              <Box>
                <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>
                  Created Date
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 2,
                  }}
                >
                  <DemoContainer
                    sx={{
                      "& .MuiStack-root": {
                        overflow: "hidden !important",
                      },
                    }}
                    components={["MobileDatePicker"]}
                  >
                    <DemoItem>
                      <MobileDatePicker
                        fullWidth
                        sx={{
                          mt: 0.5,

                          "& .MuiOutlinedInput-root": {
                            height: "40px",
                            borderRadius: "8px",
                            fontSize: "0.875rem",
                          },
                        }}
                        format="YYYY-MM-DD"
                        value={dayjs(startCreatedDate)}
                        onAccept={(newValue) => {
                          const formattedDate = newValue
                            ? dayjs(newValue).format("YYYY-MM-DD")
                            : "";
                          setStartCreatedDate(formattedDate);
                        }}
                        onClick={(e) => e.stopPropagation()}
                      />
                    </DemoItem>
                  </DemoContainer>

                  <span>-</span>

                  <DemoContainer
                    sx={{
                      "& .MuiStack-root": {
                        overflow: "hidden !important",
                      },
                    }}
                    components={["MobileDatePicker"]}
                  >
                    <DemoItem>
                      <MobileDatePicker
                        fullWidth
                        sx={{
                          mt: 0.5,

                          "& .MuiOutlinedInput-root": {
                            height: "40px",
                            borderRadius: "8px",
                            fontSize: "0.875rem",
                          },
                        }}
                        format="YYYY-MM-DD"
                        value={dayjs(endCreatedDate)}
                        onAccept={(newValue) => {
                          const formattedDate = newValue
                            ? dayjs(newValue).format("YYYY-MM-DD")
                            : "";
                          setEndCreatedDate(formattedDate);
                        }}
                      />
                    </DemoItem>
                  </DemoContainer>
                </Box>
              </Box>

              <Box sx={{ mt: 1 }}>
                <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>
                  Estimate Time Arrive
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 2,
                  }}
                >
                  <DemoContainer
                    sx={{
                      "& .MuiStack-root": {
                        overflow: "hidden !important",
                      },
                    }}
                    components={["MobileDatePicker"]}
                  >
                    <DemoItem>
                      <MobileDatePicker
                        fullWidth
                        sx={{
                          mt: 0.5,

                          "& .MuiOutlinedInput-root": {
                            height: "40px",
                            borderRadius: "8px",
                            fontSize: "0.875rem",
                          },
                        }}
                        format="YYYY-MM-DD"
                        value={dayjs(startDate)}
                        onAccept={(newValue) => {
                          const formattedDate = newValue
                            ? dayjs(newValue).format("YYYY-MM-DD")
                            : "";
                          setStartDate(formattedDate);
                        }}
                        onClick={(e) => e.stopPropagation()}
                      />
                    </DemoItem>
                  </DemoContainer>

                  <span>-</span>

                  <DemoContainer
                    sx={{
                      "& .MuiStack-root": {
                        overflow: "hidden !important",
                      },
                    }}
                    components={["MobileDatePicker"]}
                  >
                    <DemoItem>
                      <MobileDatePicker
                        fullWidth
                        sx={{
                          mt: 0.5,

                          "& .MuiOutlinedInput-root": {
                            height: "40px",
                            borderRadius: "8px",
                            fontSize: "0.875rem",
                          },
                        }}
                        format="YYYY-MM-DD"
                        value={dayjs(endDate)}
                        onAccept={(newValue) => {
                          const formattedDate = newValue
                            ? dayjs(newValue).format("YYYY-MM-DD")
                            : "";
                          setEndDate(formattedDate);
                        }}
                      />
                    </DemoItem>
                  </DemoContainer>
                </Box>
              </Box>

              <Box sx={{ mt: 1 }}>
                <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>
                  Status
                </Typography>

                <ListStatusDelivery
                  listStatus={listStatus}
                  isDefault={true}
                  defaultText={"All"}
                  selectedFile={status}
                  setSelectedFile={setStatus}
                />
              </Box>

              <Box sx={{ mt: 1 }}>
                <Typography sx={{ fontWeight: 600, fontSize: "14px" }}>
                  Priority
                </Typography>

                <ListStatusDelivery
                  listStatus={listPriority}
                  isDefault={true}
                  defaultText={"All"}
                  selectedFile={priority}
                  setSelectedFile={setPriority}
                />
              </Box>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                mt: 2,
                gap: 2,
              }}
            >
              <Box
                onClick={toggleDrawer("bottom", false)}
                sx={{ width: "100%" }}
              >
                <Button
                  sx={{
                    width: "100%",
                    bgcolor: "#fff",
                    border: `1px solid #78251E`,
                    mt: 2,
                    color: "#78251E",
                    borderRadius: "8px",
                    ":hover": {
                      bgcolor: "#fff",
                      border: `1px solid #78251E`,
                      color: "#78251E",
                    },
                    textTransform: "capitalize",
                  }}
                  onClick={() => {
                    setStatusSave("All");
                    setStartDateSave(null);
                    setEndDateSave(null);
                    setStatus("All");
                    setStartDate(null);
                    setEndDate(null);
                    setPrioritySave("All");
                    setPriority("All");
                    setEndCreatedDate(null);
                    setEndCreatedDateSave(null);
                    setStartCreatedDate(null);
                    setStartCreatedDateSave(null);
                  }}
                >
                  Reset
                </Button>
              </Box>
              <Box
                onClick={toggleDrawer("bottom", false)}
                sx={{ width: "100%" }}
              >
                <Button
                  sx={{
                    width: "100%",
                    bgcolor: "#78251E",
                    border: `1px solid #78251E`,
                    mt: 2,
                    borderRadius: "8px",
                    ":hover": {
                      bgcolor: "#78251E",
                      border: `1px solid #78251E`,
                    },
                    color: "#fff",
                    textTransform: "capitalize",
                  }}
                  onClick={() => {
                    setStartDateSave(startDate);
                    setEndDateSave(endDate);
                    setStatusSave(status);
                    setPrioritySave(priority);
                    setStartCreatedDateSave(startCreatedDate);
                    setEndCreatedDateSave(endCreatedDate);
                  }}
                >
                  Done
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </Drawer>
    </>
  );
};

export default FilterDialog;
