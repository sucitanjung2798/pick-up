import React from "react";
import { Box } from "@mui/material";
import QRCode from "react-qr-code";
import dayjs from "dayjs";

export const PrintFile = ({ packageData }) => {
  return (
    <div className="print-file">
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <QRCode value={`${packageData?.trackingId}`} size={80} />
        </div>

        <div
          style={{
            textAlign: "center",
            marginTop: "16px",
            fontWeight: 600,
            fontSize: "10px",
          }}
        >
          {packageData?.trackingId}
        </div>

        <Box sx={{ mt: 2, textAlign: "center" }}>
          <div style={{ fontSize: "12px", fontWeight: 600 }}>Sender</div>

          <div style={{ fontSize: "12px" }}>{packageData?.senderName}</div>
          <div style={{ fontSize: "12px" }}>{packageData?.senderLocation}</div>
        </Box>

        <Box sx={{ mt: 2, textAlign: "center" }}>
          <div style={{ fontSize: "12px", fontWeight: 600 }}>Recipient</div>

          <div style={{ fontSize: "12px" }}>{packageData?.recipientName}</div>
          <div style={{ fontSize: "12px" }}>
            {packageData?.recipientLocation}
          </div>
        </Box>

        <div
          style={{
            fontSize: "12px",
            fontWeight: 600,
            textAlign: "center",
            marginTop: "20px",
          }}
        >
          {dayjs(packageData?.createdDate).format("DD/MM/YYYY")}-
          {dayjs(packageData?.date).format("DD/MM/YYYY")}
        </div>

        <div
          style={{
            marginTop: "16px",
            textTransform: "uppercase",
            backgroundColor: "#000",
            color: "#fff",
            fontSize: "16px",
            fontWeight: 600,
          }}
        >
          {packageData?.confidential ? "confidential" : ""}
        </div>
      </div>
    </div>
  );
};
