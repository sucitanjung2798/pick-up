import React, { useState } from "react";
import { Box, Typography } from "@mui/material";
import CustomDialog from "../../shared/CustomDialog";
import { CloseRounded as CloseRoundedIcon } from "@mui/icons-material";

const PackageStatus = ({ packageData, consignmentData, settlementData }) => {
  const [openImage, setOpenImage] = useState(false);
  const [consignmentImage, setConsignmentImage] = useState(null);
  const [settlementImage, setSettlementImage] = useState(null);

  return (
    <Box>
      <Box>
        <Typography
          sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
        >
          Status :
        </Typography>
        <Typography
          sx={{
            fontSize: "0.75rem",
            fontWeight: 600,
            lineHeight: 1.3,
            color: `#${packageData?.colorLabel}`,
          }}
        >
          {packageData?.statusLabel}
        </Typography>
      </Box>

      {packageData?.statusLabel === "Collected" && (
        <Box sx={{ mt: 1 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Image :
          </Typography>
          <Box
            component="img"
            src={consignmentData?.proof}
            sx={{ width: "80px", borderRadius: "10px", mt: 0.5 }}
            onClick={() => {
              setOpenImage(true);
              setConsignmentImage(consignmentData?.proof);
              setSettlementImage(null);
            }}
          />
        </Box>
      )}

      {(packageData?.statusLabel === "Delivered" ||
        packageData?.statusLabel === "Undelivered") && (
        <Box sx={{ mt: 1 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Image :
          </Typography>
          <Box
            component="img"
            src={settlementData?.proof}
            sx={{ width: "80px", borderRadius: "10px", mt: 0.5 }}
            onClick={() => {
              setOpenImage(true);
              setSettlementImage(settlementData?.proof);
              setConsignmentImage(null);
            }}
          />
        </Box>
      )}

      {(packageData?.statusLabel === "Created" ||
        packageData?.statusLabel === "Edited") && (
        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Description :
          </Typography>
          <Typography
            sx={{ fontSize: "0.75rem", fontWeight: 600, lineHeight: 1.3 }}
          >
            {packageData?.description || "-"}
          </Typography>
        </Box>
      )}

      {packageData?.statusLabel === "Collected" && (
        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Description :
          </Typography>
          <Typography
            sx={{ fontSize: "0.75rem", fontWeight: 600, lineHeight: 1.3 }}
          >
            {consignmentData?.description || "-"}
          </Typography>
        </Box>
      )}

      {(packageData?.statusLabel === "Delivered" ||
        packageData?.statusLabel === "Confirmed") && (
        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Recipient Notes :
          </Typography>
          <Typography
            sx={{ fontSize: "0.75rem", fontWeight: 600, lineHeight: 1.3 }}
          >
            {settlementData?.recipientNotes || "-"}
          </Typography>
        </Box>
      )}

      {(packageData?.statusLabel === "Collected" ||
        packageData?.statusLabel === "Delivered" ||
        packageData?.statusLabel === "Confirmed") && (
        <Box sx={{ mt: 2 }}>
          <Typography
            sx={{ fontSize: "0.75rem", color: "#bdbdbd", fontWeight: 500 }}
          >
            Courier Name :
          </Typography>
          <Typography
            sx={{ fontSize: "0.75rem", fontWeight: 600, lineHeight: 1.3 }}
          >
            {consignmentData?.courierName || "-"}
          </Typography>
        </Box>
      )}

      <CustomDialog open={openImage}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center",
            p: 1,
          }}
          onClick={() => setOpenImage(false)}
        >
          <CloseRoundedIcon />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            m: "auto",
            width: "fit-content",
            alignContent: "center",
          }}
        >
          {packageData?.statusLabel === "Collected" && (
            <Box
              component="img"
              src={consignmentImage}
              sx={{ width: "250px", borderRadius: "10px", mt: 0.5 }}
              onClick={() => setOpenImage(true)}
            />
          )}

          {(packageData?.statusLabel === "Delivered" ||
            packageData?.statusLabel === "Undelivered") && (
            <Box
              component="img"
              src={settlementImage}
              sx={{ width: "250px", borderRadius: "10px", mt: 0.5 }}
              onClick={() => setOpenImage(true)}
            />
          )}
        </Box>
      </CustomDialog>
    </Box>
  );
};

export default PackageStatus;
