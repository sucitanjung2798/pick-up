import React, { useEffect, useRef, useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Typography,
  FormHelperText,
  MenuItem,
} from "@mui/material";
import {
  CloseRounded as CloseRoundedIcon,
  AddRounded as AddRoundedIcon,
} from "@mui/icons-material";
import CustomDialog from "../../shared/CustomDialog";
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";
import CustomDescriptionField from "../../shared/CustomDescriptionField";
import axios from "axios";
import Compressor from "compressorjs";
import CustomSelect from "../../shared/CustomSelect";
import { settlementStatus } from "../../api/enum";

const SettlementDialog = ({
  open,
  setOpen,
  trackingId,
  consignmentId,
  getPackage,
  desktopView,
  id,
}) => {
  const inputRef = useRef();
  const [photo, setPhoto] = useState(null);
  const [blobImage, setBlobImage] = useState(null);

  const [description, setDescription] = useState("");
  const [packageStatus, setPackageStatus] = useState("");
  const [packageStatusData, setPackageStatusData] = useState([]);

  const [isLoading, setIsLoading] = useState(false);
  const [isPhotoEmpty, setIsPhotoEmpty] = useState(false);
  const [isPackageStatusEmpty, setIsPackageStatusEmpty] = useState(false);
  const [isPhotoSize, setIsPhotoSize] = useState(false);

  const [openOptionCamera, setOpenOptionCamera] = useState(false);

  const getPackageStatus = async () => {
    try {
      const { results } = await settlementStatus();
      const filterStatus = results.filter((item) => item.value > 1);
      setPackageStatusData(filterStatus);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getPackageStatus();
  }, []);

  const handleOpenGalery = (e) => {
    setBlobImage(e.target.files[0]);
    setPhoto(e.target.files[0]);
  };

  const handleUploadClick = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  const handleInputChange = (event) => {
    handleOpenGalery(event);
  };

  const handleImage = async (image) => {
    const response = await fetch(image.url);
    const blob = await response.blob();
    setBlobImage(
      new Blob([blob], { type: blob.type }),
      `image${trackingId}.${blob.type.split("/")[1]}`
    );
  };

  const takePhoto = async () => {
    try {
      const image = await Camera.getPhoto({
        quality: 10,
        allowEditing: true,
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
      });

      const response = await fetch(image.webPath);
      const blob = await response.blob();

      new Compressor(blob, {
        quality: 0.9,
        maxWidth: 1000,
        success(blob) {
          handleImage({
            url: URL.createObjectURL(blob),
            size: blob.size,
            type: blob.type,
          });
        },
      });

      setPhoto(blob);
    } catch (error) {
      if (error.name === "CameraCanceledError") {
        console.error("User cancelled the photo-taking process.");
      } else {
        console.error("Error taking photo:", error);
      }
    }
  };

  const handleSubmitSettlement = async () => {
    if (
      blobImage !== null &&
      blobImage?.size < 4000000 &&
      packageStatus !== ""
    ) {
      setIsLoading(true);

      const formData = new FormData();

      formData.append("proof", blobImage);
      formData.append("consignment_id", consignmentId);
      formData.append("courier_notes", description);
      formData.append("status", packageStatus);
      formData.append("package_id", id);

      try {
        await axios.post("/api/v1/settlements", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
          },
        });
        setOpen(false);
        getPackage(id);
      } catch (err) {
        console.error(err);

        setIsLoading(false);
      } finally {
        setIsLoading(false);
      }
    } else {
      blobImage !== null ? setIsPhotoEmpty(false) : setIsPhotoEmpty(true);
      blobImage.size < 4000000 ? setIsPhotoSize(false) : setIsPhotoSize(true);
      packageStatus !== ""
        ? setIsPackageStatusEmpty(false)
        : setIsPackageStatusEmpty(true);
    }
  };

  useEffect(() => {
    if (blobImage) {
      setOpenOptionCamera(false);
    }

    if (blobImage?.size < 4000000) {
      setIsPhotoSize(false);
    }
  }, [blobImage]);

  return (
    <CustomDialog open={open}>
      <Box
        onClick={() => {
          setOpen(false);
          setPhoto(null);
          setIsPhotoEmpty(false);
          setIsPackageStatusEmpty(null);
          setIsPhotoSize(false);
          setPackageStatus("");
        }}
        sx={{ display: "flex", justifyContent: "flex-end", pr: 2, pt: 2 }}
      >
        <CloseRoundedIcon />
      </Box>
      <DialogContent>
        <Box noValidate component="form">
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              m: "auto",
              width: "fit-content",
            }}
          >
            {!openOptionCamera && !photo && (
              <Box
                sx={{
                  border: `2px dashed ${isPhotoEmpty ? "#B80000" : "#bdbdbd"}`,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "200px",
                  height: "200px",
                  borderRadius: "8px",
                }}
                onClick={() => setOpenOptionCamera(true)}
              >
                <AddRoundedIcon
                  sx={{
                    fontSize: "30px",
                    color: isPhotoEmpty ? "#B80000" : "#bdbdbd",
                  }}
                />
              </Box>
            )}

            {!openOptionCamera && photo && (
              <img
                src={photo && URL.createObjectURL(photo)}
                alt="Foto User"
                width="200px"
                height="200px"
                style={{ borderRadius: "10px" }}
                onClick={() => setOpenOptionCamera(true)}
              />
            )}

            {openOptionCamera && (
              <Box
                sx={{
                  border: `2px dashed ${isPhotoEmpty ? "#B80000" : "#bdbdbd"}`,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                  width: "200px",
                  height: "200px",
                  borderRadius: "8px",
                  backdropFilter: "blur(3px)",
                  backgroundColor: "rgba(0,0,30,0.4)",
                }}
              >
                <Box onClick={() => setOpenOptionCamera(false)}>
                  <CloseRoundedIcon
                    sx={{
                      fontSize: "30px",
                      color: "#fff",
                      position: "absolute",
                      top: 5,
                      right: 5,
                    }}
                  />
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                    gap: 2,
                  }}
                >
                  <Button
                    sx={{
                      textTransform: "capitalize",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                      color: "#fff",
                      width: "100px",
                    }}
                    onClick={() => {
                      takePhoto();
                    }}
                    disabled={desktopView ? false : true}
                  >
                    Camera
                  </Button>
                  <input
                    ref={inputRef}
                    type="file"
                    onChange={handleInputChange}
                    accept="image/*"
                    style={{ display: "none" }}
                  />
                  <Button
                    sx={{
                      textTransform: "capitalize",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                      color: "#fff",
                      width: "100px",
                    }}
                    onClick={() => {
                      handleUploadClick();
                    }}
                  >
                    Upload
                  </Button>
                </Box>
              </Box>
            )}
            {isPhotoEmpty && (
              <Typography sx={{ color: "#B80000", mt: 1, textAlign: "center" }}>
                Photo is required
              </Typography>
            )}

            {isPhotoSize === true && (
              <Typography
                sx={{
                  color: "#B80000",
                  mt: 1,
                  textAlign: "center",
                  fontSize: "0.75rem",
                }}
              >
                Image too large <br /> Max 4MB
              </Typography>
            )}
          </Box>

          <Box
            sx={{
              mt: 2,
            }}
          >
            <Typography>Status</Typography>
            <CustomSelect
              value={packageStatus}
              onChange={(e) => {
                setPackageStatus(e.target.value);

                if (packageStatus) {
                  setIsPackageStatusEmpty(false);
                }
              }}
              isValueError={isPackageStatusEmpty}
              data={packageStatusData.map((option, index) => (
                <MenuItem
                  key={index}
                  value={option.value}
                  sx={{ fontSize: "0.875rem" }}
                >
                  {option.label}
                </MenuItem>
              ))}
              isHelperText={true}
              helperText={
                isPackageStatusEmpty && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please select package status
                  </FormHelperText>
                )
              }
              placeholder="Please select package status"
            />
          </Box>

          <Box
            sx={{
              mt: 2,
            }}
          >
            <Typography>Courier Note</Typography>
            <CustomDescriptionField
              placeholder="Write your description"
              sx={{ mt: 0.5 }}
              setValue={setDescription}
              value={description}
            />
          </Box>
        </Box>

        <Button
          sx={{
            backgroundColor: "#78251E",
            border: "1px solid #78251E",
            color: "#fff",
            textTransform: "capitalize",
            px: 2,
            borderRadius: "8px",
            display: "flex",
            gap: 1,
            mt: 2,
            alignItems: "center",
            ":hover": {
              bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
              color: "#ffffff",
              border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
            },
          }}
          disabled={isLoading ? true : false}
          autoFocus
          onClick={handleSubmitSettlement}
          fullWidth
        >
          {isLoading && (
            <CircularProgress
              sx={{ color: "grey" }}
              thickness={4}
              size={20}
              disableShrink
            />
          )}
          <Typography sx={{ fontSize: "0.875rem" }}>Save</Typography>
        </Button>
      </DialogContent>
    </CustomDialog>
  );
};

export default SettlementDialog;
