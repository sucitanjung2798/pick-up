import React, { useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Typography,
} from "@mui/material";
import { CloseRounded as CloseRoundedIcon } from "@mui/icons-material";
import CustomDescriptionField from "../../shared/CustomDescriptionField";
import CustomDialog from "../../shared/CustomDialog";
import { confirmPackage } from "../../api/package";

const ConfirmDialog = ({ open, setOpen, getPackage, settlementId, id }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [recipientNotes, setRecipientNotes] = useState("");

  const handleSubmitConfirm = async () => {
    try {
      setIsLoading(true);
      const data = {
        recipientNotes,
      };
      await confirmPackage(settlementId, data);
      setOpen(false);
      getPackage(id);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CustomDialog open={open}>
      <Box
        onClick={() => {
          setOpen(false);
        }}
        sx={{ display: "flex", justifyContent: "flex-end", pr: 2, pt: 2 }}
      >
        <CloseRoundedIcon />
      </Box>
      <DialogContent>
        <Box noValidate component="form">
          <Box>
            <Typography>Recipient Note</Typography>
            <CustomDescriptionField
              placeholder="Write your description"
              sx={{ mt: 0.5 }}
              setValue={setRecipientNotes}
              value={recipientNotes}
            />
          </Box>
        </Box>

        <Button
          sx={{
            backgroundColor: "#78251E",
            border: "1px solid #78251E",
            color: "#fff",
            textTransform: "capitalize",
            px: 2,
            borderRadius: "8px",
            display: "flex",
            gap: 1,
            mt: 2,
            alignItems: "center",
            ":hover": {
              bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
              color: "#ffffff",
              border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
            },
          }}
          onClick={handleSubmitConfirm}
          disabled={isLoading ? true : false}
          autoFocus
          fullWidth
        >
          {isLoading && (
            <CircularProgress
              sx={{ color: "grey" }}
              thickness={4}
              size={20}
              disableShrink
            />
          )}
          <Typography sx={{ fontSize: "0.875rem" }}>Save</Typography>
        </Button>
      </DialogContent>
    </CustomDialog>
  );
};

export default ConfirmDialog;
