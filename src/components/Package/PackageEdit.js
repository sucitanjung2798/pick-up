import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { viewUserActive } from "../../api/users";
import { AuthContext } from "../../contexts/AuthContext";
import dayjs from "dayjs";
import {
  createPackageDetail,
  editPackage,
  editPackageDetail,
  showPackage,
  viewPackageDetail,
} from "../../api/package";
import { getAllUom } from "../../api/uom";
import PackageEditContent from "./PackageEditContent";

const PackageEdit = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { enumPriority } = useContext(AuthContext);

  const [senderLocation, setSenderLocation] = useState("");
  const [recipientLocation, setRecipientLocation] = useState("");
  const [senderName, setSenderName] = useState("");
  const [recipientName, setRecipientName] = useState("");
  const [date, setDate] = useState("");
  const [confidential, setConfidential] = useState(false);
  const [priorityLevel, setPriorityLevel] = useState("");
  const [recipientId, setRecipientId] = useState(null);
  const [description, setDescription] = useState("");

  const [isRecipientDataError, setIsRecipientDataError] = useState(false);
  const [isSenderLocationError, setIsSenderLocationError] = useState(false);
  const [isRecipientLocationError, setIsRecipientLocationError] =
    useState(false);
  const [isSenderNameError, setIsSenderNameError] = useState(false);
  const [isRecipientNameError, setIsRecipientNameError] = useState(false);
  const [isPriorityLevelError, setIsPriorityLevelError] = useState(false);
  const [isDateError, setIsDateError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingEdit, setIsLoadingEdit] = useState(false);

  const [userLists, setUserLists] = useState([]);
  const [recipientOptions, setRecipientOptions] = useState([]);
  const [recipientData, setRecipientData] = useState(null);
  const [isDisabled, setIsDisabled] = useState(false);

  const [packageDetailArray, setPackageDetailArray] = useState([
    {
      packageName: "",
      uom: "",
      quantity: "",
      note: "",
      packageDetailId: null,
    },
  ]);
  const [isPackageDetailEmpty, setIsPackageDetailEmpty] = useState(false);
  const [uomLists, setUomLists] = useState([]);

  const [isPackageNameError, setIsPackageNameError] = useState(false);
  const [isUomError, setIsUomError] = useState(false);
  const [isQuantityError, setIsQuantityError] = useState(false);
  const [isNoteError, setIsNoteError] = useState(false);

  const getPackageDetail = async (id) => {
    try {
      setIsLoading(true);

      const {
        result: {
          senderName,
          recipientId,
          recipientName,
          senderLocation,
          recipientLocation,
          priority: { value: priorityValue },
          confidential,
          eta: { date },
          description,
        },
      } = await showPackage(id);
      setSenderName(senderName);
      setRecipientId(recipientId);
      setRecipientName(recipientName);
      setSenderLocation(senderLocation);
      setRecipientLocation(recipientLocation);
      setPriorityLevel(priorityValue);
      setConfidential(confidential);
      setDate(date);
      setDescription(description);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  const getViewPackageDetail = async (id) => {
    try {
      const {
        results: { data },
      } = await viewPackageDetail(id);
      setPackageDetailArray(
        data.map(({ name, uom: { id }, quantity, notes, packageDetailId }) => ({
          packageName: name,
          uom: id,
          quantity,
          note: notes,
          packageDetailId,
        }))
      );
    } catch (err) {
      console.error(err);
    }
  };

  const getUomList = async () => {
    try {
      const {
        results: { data },
      } = await getAllUom({});
      setUomLists(data);
    } catch (err) {
      console.error(err);
    }
  };

  const getUserList = async () => {
    try {
      const {
        results: { data },
      } = await viewUserActive();
      setRecipientOptions(
        data?.map(({ userId, name, department }) => ({
          id: userId,
          label: name,
          department,
        }))
      );
      setUserLists(data);
    } catch (err) {
      console.error(err);
    }
  };

  const getUserById = userLists.find((item) => item.userId === recipientId);

  useEffect(() => {
    getUserList();
    getPackageDetail(id);
    getUomList();
    getViewPackageDetail(id);
  }, [id]);

  useEffect(() => {
    setRecipientData({
      id: getUserById?.userId,
      label: getUserById?.name,
      department: getUserById?.department,
    });
  }, [getUserById]);

  useEffect(() => {
    if (recipientData !== null) {
      setRecipientLocation(recipientData?.department?.location);
      setRecipientName(recipientData?.label);
    } else {
      setRecipientLocation("");
      setRecipientName("");
    }
  }, [recipientData]);

  const tempPackageDetailArray = [...packageDetailArray];
  const getNewPackageDetail = tempPackageDetailArray.filter(
    (item) => item.packageDetailId === undefined
  );
  const getOldPackageDetail = tempPackageDetailArray.filter(
    (item) => item.packageDetailId !== undefined
  );

  const handleSubmit = async (e) => {
    e.preventDefault();

    let name;
    let uomId;
    let quantity;

    for (const data of packageDetailArray) {
      name = data.packageName;
      uomId = data.uom;
      quantity = data.quantity;
    }

    if (
      senderLocation !== "" &&
      recipientLocation !== "" &&
      senderName !== "" &&
      recipientName !== "" &&
      priorityLevel !== "" &&
      date !== "" &&
      recipientData !== null &&
      name !== "" &&
      uomId !== "" &&
      quantity !== "" &&
      packageDetailArray.length > 0
    ) {
      try {
        setIsLoadingEdit(true);

        const data = {
          senderName: senderName,
          senderLocation,
          recipientId: recipientData?.id,
          recipientName: recipientName,
          recipientLocation,
          eta: dayjs(date).format("YYYY-MM-DD"),
          priority: priorityLevel,
          confidential: confidential === true ? 1 : 0,
          description,
        };

        const res = await editPackage(id, data);

        if (res.status === 200) {
          for (const data of getOldPackageDetail) {
            const packageData = {
              name: data.packageName,
              quantity: data.quantity,
              uomId: data.uom,
              notes: data.note,
              packageDetailId: data.packageDetailId,
            };
            await handleEditPackageDetail(
              id,
              packageData.packageDetailId,
              packageData
            );
          }

          for (const detailData of getNewPackageDetail) {
            const packageData = {
              name: detailData.packageName,
              quantity: detailData.quantity,
              uomId: detailData.uom,
              notes: detailData.note,
            };
            await handleSubmitPackageDetail(id, packageData);
          }
        }

        navigate("/package/lists");
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoadingEdit(false);
      }
    } else {
      senderLocation !== ""
        ? setIsSenderLocationError(false)
        : setIsSenderLocationError(true);
      recipientLocation !== ""
        ? setIsRecipientLocationError(false)
        : setIsRecipientLocationError(true);
      senderName !== ""
        ? setIsSenderNameError(false)
        : setIsSenderNameError(true);
      recipientName !== ""
        ? setIsRecipientNameError(false)
        : setIsRecipientNameError(true);
      priorityLevel !== ""
        ? setIsPriorityLevelError(false)
        : setIsPriorityLevelError(true);
      date !== "" ? setIsDateError(false) : setIsDateError(true);
      recipientData !== null
        ? setIsRecipientDataError(false)
        : setIsRecipientDataError(true);
      name !== "" ? setIsPackageNameError(false) : setIsPackageNameError(true);
      uomId !== "" ? setIsUomError(false) : setIsUomError(true);
      quantity !== "" ? setIsQuantityError(false) : setIsQuantityError(true);
      packageDetailArray.length > 0
        ? setIsPackageDetailEmpty(false)
        : setIsPackageDetailEmpty(true);
    }
  };

  const handleEditPackageDetail = async (
    packageId,
    detailId,
    detailPackage
  ) => {
    try {
      const res = await editPackageDetail(packageId, detailId, detailPackage);

      if (res?.status !== 200) {
        console.error("Failed to submit package detail");
      }
    } catch (err) {
      console.error("Error :", err);
    }
  };

  const handleSubmitPackageDetail = async (packageId, detailPackage) => {
    try {
      const res = await createPackageDetail(packageId, detailPackage);

      if (res?.status !== 200) {
        console.error("Failed to submit package detail");
      }
    } catch (err) {
      console.error("Error :", err);
    }
  };

  return (
    <PackageEditContent
      isLoading={isLoading}
      handleSubmit={handleSubmit}
      senderLocation={senderLocation}
      setSenderLocation={setSenderLocation}
      isSenderLocationError={isSenderLocationError}
      recipientLocation={recipientLocation}
      setRecipientLocation={setRecipientLocation}
      isRecipientLocationError={isRecipientLocationError}
      isDisabled={isDisabled}
      senderName={senderName}
      setSenderName={setSenderName}
      isSenderNameError={isSenderNameError}
      setIsSenderNameError={setIsSenderNameError}
      recipientData={recipientData}
      setRecipientData={setRecipientData}
      setIsDisabled={setIsDisabled}
      setIsRecipientDataError={setIsRecipientDataError}
      recipientOptions={recipientOptions}
      isRecipientDataError={isRecipientDataError}
      recipientName={recipientName}
      isRecipientNameError={isRecipientNameError}
      setRecipientName={setRecipientName}
      setIsRecipientNameError={setIsRecipientNameError}
      priorityLevel={priorityLevel}
      setPriorityLevel={setPriorityLevel}
      setIsPriorityLevelError={setIsPriorityLevelError}
      isPriorityLevelError={isPriorityLevelError}
      enumPriority={enumPriority}
      date={date}
      setDate={setDate}
      isDateError={isDateError}
      confidential={confidential}
      setConfidential={setConfidential}
      packageDetailArray={packageDetailArray}
      setPackageDetailArray={setPackageDetailArray}
      uomLists={uomLists}
      isPackageNameError={isPackageNameError}
      setIsPackageNameError={setIsPackageNameError}
      isUomError={isUomError}
      setIsUomError={setIsUomError}
      isQuantityError={isQuantityError}
      setIsQuantityError={setIsQuantityError}
      isNoteError={isNoteError}
      setIsNoteError={setIsNoteError}
      isLoadingEdit={isLoadingEdit}
      isPackageDetailEmpty={isPackageDetailEmpty}
      description={description}
      setDescription={setDescription}
    />
  );
};

export default PackageEdit;
