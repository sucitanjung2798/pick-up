import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import { Box, Skeleton, Typography } from "@mui/material";
import PackageImage from "../../assets/package-icon.png";
import {
  viewPackageCourier,
  viewPackages,
  viewPackagesAdmin,
} from "../../api/package";
import InfiniteScroll from "react-infinite-scroll-component";

const PackageCard = ({
  packageLists,
  hasMore,
  setHasMore,
  setPackageLists,
  permissions,
  startDate,
  endDate,
}) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMorePackage = async ({ page, search, priority }) => {
    const {
      results: { data: newPackage },
      pages: { currentPage, totalPages },
    } = permissions?.includes("package.admin")
      ? await viewPackagesAdmin({
          page,
          search,
          etaBeginDate: startDate,
          etaEndDate: endDate,
          priority,
        })
      : permissions.includes("consign.courier")
      ? await viewPackageCourier({
          page,
          search,
        })
      : viewPackages({
          page,
          search,
          etaBeginDate: startDate,
          etaEndDate: endDate,
          priority,
        });
    setPackageLists([...packageLists, ...newPackage]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  return (
    <Box
      sx={{
        px: 2,
      }}
    >
      <InfiniteScroll
        dataLength={packageLists?.length || 0}
        next={() => fetchMorePackage({ page })}
        hasMore={hasMore}
        loader={
          packageLists?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2.5, mt: 1 }}>
          {packageLists?.map((item, index) => (
            <Box
              sx={{
                boxShadow: 3,
                borderRadius: "10px",
                px: 2,
                py: 1,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
              key={index}
              onClick={() => navigate(`/package/detail/${item.packageId}`)}
            >
              <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                <img src={PackageImage} alt="Package" />
                <Box>
                  <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                    {item?.senderName}
                  </Typography>
                  <Typography sx={{ fontSize: "0.625rem" }}>
                    {dayjs(item?.eta?.date).format("DD-MM-YYYY")}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem", color: "#bdbdbd" }}>
                    Sending To : {item?.recipientName}
                  </Typography>
                </Box>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  alignItems: "center",
                  border: `1px solid #${item?.status?.color}`,
                  px: 1,
                  py: 0.5,
                  borderRadius: "8px",
                  color: `#${item?.status?.color}`,
                }}
              >
                <i
                  className={item?.status?.icon}
                  style={{ fontSize: "15px" }}
                />
                <Typography
                  sx={{
                    fontSize: "0.75rem",
                    fontWeight: 600,
                  }}
                >
                  {item?.status?.label}
                </Typography>
              </Box>
            </Box>
          ))}
        </Box>
      </InfiniteScroll>
    </Box>
  );
};

export default PackageCard;
