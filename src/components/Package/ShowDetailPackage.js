import React, { useEffect, useState } from "react";
import CustomDialog from "../../shared/CustomDialog";
import { CloseRounded as CloseRoundedIcon } from "@mui/icons-material";
import { Box, DialogContent, Skeleton, Typography } from "@mui/material";
import { showPackageDetailAdmin } from "../../api/package";

const ShowDetailPackage = ({
  openDialogDetailPackage,
  setOpenDialogDetailPackage,
  setPackageDetailId,
  packageDetailData,
  packageDetailId,
  showPackageDetail,
  setPackageDetailData,
  id,
  permissions,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const getPackageDetail = async () => {
    if (packageDetailId) {
      try {
        setIsLoading(true);
        const { result } = permissions.includes("package.admin")
          ? await showPackageDetailAdmin(id, packageDetailId)
          : await showPackageDetail(id, packageDetailId);
        setPackageDetailData(result);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    getPackageDetail(id, packageDetailId);
  }, [id, packageDetailId]);

  return (
    <CustomDialog
      sx={{
        "& .MuiDialogContent-root": {
          padding: "10px 24px",
        },
      }}
      open={openDialogDetailPackage}
    >
      <Box
        onClick={() => {
          setOpenDialogDetailPackage(false);
          setPackageDetailId(null);
        }}
        sx={{ display: "flex", justifyContent: "flex-end", pr: 2, pt: 2 }}
      >
        <CloseRoundedIcon sx={{ fontSize: "1.25rem" }} />
      </Box>
      <DialogContent>
        {isLoading && (
          <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
            <Skeleton variant="text" sx={{ fontSize: "1rem" }} />
            <Skeleton variant="text" sx={{ fontSize: "1rem" }} />
            <Skeleton variant="text" sx={{ fontSize: "1rem" }} />
            <Skeleton variant="text" sx={{ fontSize: "1rem" }} />
          </Box>
        )}

        {!isLoading && (
          <Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
                mb: 1,
              }}
            >
              <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                Package Name
              </Typography>
              <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
                {packageDetailData?.name}
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
                mb: 1,
              }}
            >
              <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                Quantity
              </Typography>
              <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
                {packageDetailData?.quantity}
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
                mb: 1,
              }}
            >
              <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                UOM
              </Typography>
              <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
                {packageDetailData?.uom?.name}
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent:
                  packageDetailData?.notes?.length < 20 && "space-between",
                alignItems: packageDetailData?.notes?.length < 20 && "center",
                flexDirection:
                  packageDetailData?.notes?.length > 20 && "column",
                gap: packageDetailData?.notes?.length < 20 && 2,
                mb: 1,
              }}
            >
              <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                Notes
              </Typography>
              <Typography
                sx={{
                  fontSize: "0.875rem",
                  textAlign:
                    packageDetailData?.notes?.length > 20 ? "justify" : "right",
                }}
              >
                {packageDetailData?.notes || "-"}
              </Typography>
            </Box>
          </Box>
        )}
      </DialogContent>
    </CustomDialog>
  );
};

export default ShowDetailPackage;
