import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import { deleteDepartment, getAllDepartments } from "../../api/departments";
import ScrollOnTop from "../../shared/ScrollOnTop";
import SearchWithDebounce from "../../shared/SearchWithDebounce";
import DepartmentCards from "./DepartmentCards";
import ListStatusDelivery from "../../shared/ListStatusDelivery";

const statusLists = [
  { value: 1, label: "Active", color: "00FF00" },
  { value: 2, label: "Inactive", color: "FF0000" },
];

const Departments = ({ permissions }) => {
  const navigate = useNavigate();

  const [departmentData, setDepartmentData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [openDialog, setOpenDialog] = useState(false);
  const [idDepartment, setIdDepartment] = useState(null);

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  const [hasMore, setHasMore] = useState(false);
  const [status, setStatus] = useState("All");

  const getDepartments = async ({ search, status }) => {
    setIsLoading(true);
    try {
      const res = await getAllDepartments({ search, status });
      setDepartmentData(res?.results?.data || []);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getDepartments({ search: savedSearch || "", status });
  }, [savedSearch, status]);

  const handleDeleteDepartment = async () => {
    setIsLoading(true);
    await deleteDepartment(idDepartment)
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoading(false);
        getDepartments();
        setOpenDialog(false);
      });
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Departments" isNotificationsNeed={true} />

        <Box
          sx={{
            px: 2,
            pt: 2,
            display: "flex",
            flexDirection: "column",
            gap: 2,
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            <SearchWithDebounce
              search={search}
              setSearch={setSearch}
              setSearchParams={setSearchParams}
              updatedSearchParams={updatedSearchParams}
              setSavedSearch={setSavedSearch}
            />
            <Button
              sx={{
                border: "1px solid #78251E",
                borderRadius: "10px",
                px: 3,
                height: "40px",
                textTransform: "capitalize",
                color: "#000",
                fontWeight: 600,
              }}
              startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
              onClick={() => navigate("/add-new-department")}
            >
              New
            </Button>
          </Box>
          <Box>
            <ListStatusDelivery
              defaultText={"All"}
              listStatus={statusLists}
              selectedFile={status}
              setSelectedFile={setStatus}
              isDefault={true}
              textTransform="capitalize"
            />
          </Box>
        </Box>
      </Box>

      <ScrollOnTop />

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}>
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              variant="rounded"
              height="50px"
              animation="wave"
              key={index}
              sx={{ mt: 2 }}
            />
          ))}

        {!isLoading && departmentData?.length === 0 && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <Typography sx={{ textAlign: "center" }}>
              The Department is empty
            </Typography>
          </Box>
        )}

        <ScrollOnTop />

        {!isLoading && departmentData?.length > 0 && (
          <DepartmentCards
            departmentData={departmentData}
            setDepartmentData={setDepartmentData}
            setHasMore={setHasMore}
            hasMore={hasMore}
            setOpenDialog={setOpenDialog}
            setIdDepartment={setIdDepartment}
            handleDeleteDepartment={handleDeleteDepartment}
            openDialog={openDialog}
            idDepartment={idDepartment}
            permissions={permissions}
          />
        )}
      </Box>
    </Box>
  );
};

export default Departments;
