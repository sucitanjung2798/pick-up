import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Typography, Button } from "@mui/material";
import BackButton from "../../shared/BackButton";
import Header from "../../shared/Header";
import DeleteModal from "../../shared/DeleteModal";

const DepartmentDetail = ({ permissions }) => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [isOpen, setIsOpen] = useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setIsOpen({ ...isOpen, [anchor]: open });
  };

  return (
    <>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Department Detail" isNotificationsNeed={true} />
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          p: 2,
        }}
      >
        <BackButton onClick={() => navigate("/department/lists")} />
        <Box
          sx={{
            mt: 2,
            display: "flex",
            flexDirection: "column",
            gap: 0.5,
            border: "1px solid #bdbdbd",
            p: 2,
            borderRadius: "10px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              Department Name
            </Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>IT</Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>Location</Typography>
            <Typography sx={{ fontSize: "0.875rem" }}>
              Head Office Multatuli
            </Typography>
          </Box>
        </Box>

        <Box
          sx={{
            width: "100%",
            position: "relative",
            display: "flex",
            gap: 2,
            mt: 2,
          }}
        >
          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",

              color: "#ffffff",
              fontSize: "18px",
              bgcolor: "#78251E",
              height: "45px",
              borderRadius: "10px",
              ":hover": {
                bgcolor: "#78251E",
                color: "#ffffff",
              },
            }}
            onClick={() => navigate(`/department/edit/${id}`)}
            fullWidth
          >
            Edit
          </Button>

          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",

              color: "#78251E",
              fontSize: "18px",
              bgcolor: "#ffffff",
              border: "1px solid #78251E",
              height: "45px",
              borderRadius: "10px",
              ":hover": {
                bgcolor: "#ffffff",
                color: "#78251E",
                border: "1px solid #78251E",
              },
            }}
            fullWidth
            onClick={toggleDrawer("bottom", true)}
          >
            Delete
          </Button>
        </Box>
      </Box>

      <DeleteModal
        isOpen={isOpen}
        toggleDrawer={toggleDrawer}
        title="Delete Department"
        path="department/lists"
        message="Are you sure wan't to delete this department?"
      />
    </>
  );
};

export default DepartmentDetail;
