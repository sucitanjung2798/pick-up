import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Skeleton,
  Typography,
} from "@mui/material";
import InfiniteScroll from "react-infinite-scroll-component";
import MenuComponent from "../../shared/MenuComponent";
import { getAllDepartments } from "../../api/departments";
import CustomDialog from "../../shared/CustomDialog";

const DepartmentCards = ({
  departmentData,
  setDepartmentData,
  setHasMore,
  hasMore,
  setOpenDialog,
  setIdDepartment,
  handleDeleteDepartment,
  openDialog,
  idDepartment,
  permissions,
}) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMoreDepartment = async ({ page, search, status }) => {
    const {
      results: { data: newDepartment },
      pages: { currentPage, totalPages },
    } = await getAllDepartments({ page, search, status });

    setDepartmentData([...departmentData, ...newDepartment]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };
  return (
    <Box>
      <InfiniteScroll
        dataLength={departmentData?.length || 0}
        next={() => fetchMoreDepartment({ page })}
        hasMore={hasMore}
        loader={
          departmentData?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ mt: 2, display: "flex", flexDirection: "column", gap: 2 }}>
          {departmentData?.map((item, index) => {
            const menuLists = [
              {
                name: "Edit Department",
                clickHandler: () => {
                  navigate(`/department/edit/${item.departmentId}`);
                },
              },
              {
                name: "Delete Department",
                clickHandler: () => {
                  setOpenDialog(true);
                  setIdDepartment(item.departmentId);
                },
              },
            ];
            return (
              <Box
                sx={{
                  boxShadow: 3,
                  p: 1.5,
                  borderRadius: "10px",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
                key={index}
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {item.name}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item.location}
                  </Typography>
                </Box>

                <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: 1,
                      border: `1px solid #${item?.status?.color}`,
                      color: `#${item?.status?.color}`,
                      py: 0.5,
                      px: 1,
                      borderRadius: "50px",
                    }}
                  >
                    <i className={`${item?.status?.icon}`} />
                    <Typography
                      sx={{
                        fontSize: "0.75rem",
                      }}
                    >
                      {item.status?.label}
                    </Typography>
                  </Box>

                  <MenuComponent menuLists={menuLists} id={item.departmentId} />
                </Box>
              </Box>
            );
          })}
        </Box>
      </InfiniteScroll>

      <CustomDialog open={openDialog} handleClose={() => setOpenDialog(false)}>
        {permissions.includes("dept.delete") ? (
          <>
            <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
              Delete Department
            </DialogTitle>
            <DialogContent>
              <DialogContentText
                id="alert-dialog-description"
                sx={{ color: "#000000" }}
              >
                Are you sure want to delete this department?
              </DialogContentText>
            </DialogContent>
            <DialogActions sx={{ px: 2 }}>
              <Button
                sx={{
                  border: "1px solid #78251E",
                  textTransform: "capitalize",
                  color: "#000000",
                  px: 2,
                  borderRadius: "8px",
                  mr: 1,
                }}
                size="small"
                onClick={() => setOpenDialog(false)}
              >
                Cancel
              </Button>
              <Button
                sx={{
                  backgroundColor: "#78251E",
                  border: "1px solid #78251E",
                  color: "#fff",
                  textTransform: "capitalize",
                  px: 2,
                  borderRadius: "8px",
                }}
                onClick={() => handleDeleteDepartment(idDepartment)}
                autoFocus
                size="small"
              >
                Delete
              </Button>
            </DialogActions>
          </>
        ) : (
          <>
            {" "}
            <DialogContent>
              <DialogContentText
                id="alert-dialog-description"
                sx={{ color: "#000000" }}
              >
                You are not permitted to access this action
              </DialogContentText>
            </DialogContent>
            <DialogActions sx={{ px: 2 }}>
              <Button
                sx={{
                  backgroundColor: "#78251E",
                  border: "1px solid #78251E",
                  color: "#fff",
                  textTransform: "capitalize",
                  px: 2,
                  borderRadius: "8px",
                }}
                onClick={() => setOpenDialog(false)}
                autoFocus
                size="small"
              >
                Close
              </Button>
            </DialogActions>
          </>
        )}
      </CustomDialog>
    </Box>
  );
};

export default DepartmentCards;
