import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  Typography,
  FormControl,
  OutlinedInput,
  FormGroup,
  FormControlLabel,
  TextField,
  FormHelperText,
  styled,
  Switch,
  Skeleton,
} from "@mui/material";
import BackButton from "../../shared/BackButton";
import Header from "../../shared/Header";
import CustomButtonSave from "../../shared/CustomButtonSave";
import CheckIcon from "../../assets/check-icon.png";
import CheckIconInactive from "../../assets/check-icon-inactive.png";
import { editDepartment, showDepartment } from "../../api/departments";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const DepartmentEdit = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [departmentName, setDepartmentName] = useState("");
  const [location, setLocation] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState(false);

  const [isDepartmentNameError, setIsDepartmentNameError] = useState(false);
  const [isLocationError, setIsLocationError] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingEdit, setIsLoadingEdit] = useState(false);

  const getDepartmentShow = async (id) => {
    try {
      setIsLoading(true);

      const {
        result: { name, description, location, status },
      } = await showDepartment(id);
      setDepartmentName(name);
      setLocation(location);
      setDescription(description);
      setStatus(status === 0 ? false : true);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getDepartmentShow(id);
  }, [id]);

  const handleEdit = async (event) => {
    event.preventDefault();
    try {
      setIsLoadingEdit(true);

      const data = {
        id,
        name: departmentName,
        location,
        description,
        status: status === false ? 0 : 1,
      };

      await editDepartment(data, id);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoadingEdit(false);
      navigate("/department/lists");
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Department Edit" isNotificationsNeed={true} />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate("/department/lists")} />
        </Box>
      </Box>

      <Box sx={{ p: 2 }}>
        {isLoading && (
          <Skeleton
            sx={{ mt: 2 }}
            variant="rounded"
            height="400px"
            animation="wave"
          />
        )}

        {!isLoading && (
          <Box
            sx={{
              border: "1px solid #bdbdbd",
              p: 2,
              borderRadius: "10px",
            }}
            component="form"
            onSubmit={handleEdit}
          >
            <Box>
              <Box>
                <Typography sx={{ fontSize: "0.875rem" }}>
                  Department Name
                </Typography>
                <FormControl
                  variant="outlined"
                  sx={{
                    mt: 0.5,
                    "& .MuiOutlinedInput-root": {
                      height: "44px",
                      borderRadius: "8px",
                      display: "flex",
                      alignItems: "center",
                    },
                  }}
                  fullWidth
                >
                  <OutlinedInput
                    placeholder="Enter your department name"
                    sx={{ fontSize: "0.875rem" }}
                    value={departmentName}
                    error={!departmentName && isDepartmentNameError}
                    onChange={(e) => {
                      setDepartmentName(e.target.value);

                      if (departmentName) {
                        setIsDepartmentNameError(false);
                      }
                    }}
                  />

                  {isDepartmentNameError && (
                    <FormHelperText
                      sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                    >
                      Please input the department name
                    </FormHelperText>
                  )}
                </FormControl>
              </Box>

              <Box sx={{ mt: 2 }}>
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    Location
                  </Typography>
                  <FormControl
                    variant="outlined"
                    sx={{
                      mt: 0.5,
                      "& .MuiOutlinedInput-root": {
                        height: "44px",
                        borderRadius: "8px",
                        display: "flex",
                        alignItems: "center",
                      },
                    }}
                    fullWidth
                  >
                    <OutlinedInput
                      placeholder="Enter your location"
                      sx={{ fontSize: "0.875rem" }}
                      value={location}
                      error={!location && isLocationError}
                      onChange={(e) => {
                        setLocation(e.target.value);

                        if (location) {
                          setIsLocationError(false);
                        }
                      }}
                    />
                    {isLocationError && (
                      <FormHelperText
                        sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                      >
                        Please input the location
                      </FormHelperText>
                    )}
                  </FormControl>
                </Box>
              </Box>

              <Box sx={{ mt: 2 }}>
                <Typography sx={{ fontSize: "0.875rem" }}>
                  Description
                </Typography>
                <Box
                  sx={{
                    mt: 0.5,
                    borderRadius: "10px",
                    "& .MuiTextField-root": { width: "100%" },
                    "& .MuiOutlinedInput-root": {
                      borderColor: "#bdbdbd",
                      borderRadius: "10px",
                      fontSize: "0.875rem",
                    },
                  }}
                  noValidate
                  autoComplete="off"
                  placeholder="Input your description"
                >
                  <TextField
                    multiline
                    rows={4}
                    placeholder="Input your description"
                    value={description || ""}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Box>
              </Box>

              <Box
                sx={{
                  mt: 2,
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 2,
                }}
              >
                <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
                <FormGroup
                  sx={{
                    "& .MuiFormControlLabel-root": {
                      marginRight: 0,
                    },
                  }}
                >
                  <FormControlLabel
                    labelPlacement="end"
                    label={
                      status === true ? (
                        <Typography sx={{ fontSize: "14px", ml: 1 }}>
                          Active
                        </Typography>
                      ) : (
                        <Typography sx={{ fontSize: "14px", ml: 1 }}>
                          Non Active
                        </Typography>
                      )
                    }
                    control={
                      <CustomSwitch
                        checked={status}
                        onChange={() => setStatus(!status)}
                      />
                    }
                  />
                </FormGroup>
              </Box>
            </Box>

            <Box>
              <CustomButtonSave content="Save" isLoading={isLoadingEdit} />
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default DepartmentEdit;
