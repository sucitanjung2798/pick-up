import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Typography,
  Box,
  FormControl,
  OutlinedInput,
  InputAdornment,
  Button,
  FormHelperText,
  CircularProgress,
} from "@mui/material";
import {
  EmailRounded as EmailRoundedIcon,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import TakeAway from "../assets/Take Away Image.png";
import qs from "qs";
import { useAuth } from "../hooks/useAuth";
import { viewAccess } from "../helpers/enums";
import { postLogin } from "../api/auth";

const Login = () => {
  const navigate = useNavigate();
  const { setAccessToken, setPermissions, permissions, accessToken } =
    useAuth();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isEmailError, setIsEmailError] = useState(false);
  const [isPasswordError, setIsPasswordError] = useState(false);
  const [error, setError] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const handleClickShowPassword = () => setIsShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const adminPermissions = Object.freeze(viewAccess);
  const routes = [
    "config.view",
    "config.show",
    "config.create",
    "config.update",
    "config.delete",
    "role.view",
    "role.show",
    "role.create",
    "role.update",
    "role.delete",
    "perm.view",
    "perm.show",
    "perm.create",
    "perm.update",
    "perm.delete",
    "access.show",
    "access.update",
    "dept.view",
    "dept.show",
    "dept.create",
    "dept.update",
    "dept.delete",
    "user.view",
    "user.show",
    "user.create",
    "user.update",
    "user.delete",
    "user.reset",
    "uom.view",
    "uom.show",
    "uom.create",
    "uom.update",
    "uom.delete",
    "package.admin",
    "package.view",
    "package.show",
    "package.create",
    "package.update",
    "package.delete",
    "consign.courier",
    "consign.admin",
    "consign.view",
    "consign.show",
    "consign.create",
    "consign.update",
    "consign.delete",
    "settle.admin",
    "settle.view",
    "settle.show",
    "settle.create",
    "settle.update",
    "settle.delete",
    "report.view",
    "util.view",
    "util.sess_remove",
    "util.db_import",
    "util.db_export",
    "util.db_clear",
  ];

  const handlePermissions = () => {
    for (let i = 0; i < adminPermissions.length; i++) {
      if (permissions.includes(adminPermissions[i])) {
        navigate(routes[i], { replace: true });
        break;
      }
    }
  };

  useEffect(() => {
    if (accessToken && permissions.length) handlePermissions();
  }, [accessToken, permissions]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (email !== "" && password !== "") {
      try {
        const body = {
          email,
          password,
        };

        setIsLoading(true);

        const {
          data: {
            result: { user_token, user_access },
          },
        } = await postLogin(qs.stringify(body));

        setAccessToken(user_token);
        window.location.replace("/dashboard");
        setPermissions(user_access);
        localStorage.setItem("accessToken", user_token);
        localStorage.setItem("permissions", user_access);
      } catch (err) {
        const errStatus = err?.response?.status;

        if (errStatus === 400) {
          setError("Wrong email or password");
        } else if (errStatus === 500) {
          setError("Internal Server Error");
        } else if (errStatus === 403) {
          setError("User Inactive");
        } else {
          setError("error");
        }
      } finally {
        setIsLoading(false);
      }
    } else {
      email !== "" ? setIsEmailError(false) : setIsEmailError(true);
      password !== "" ? setIsPasswordError(false) : setIsPasswordError(true);
    }
  };

  return (
    <Box
      sx={{ textAlign: "left", p: 2 }}
      component="form"
      onSubmit={handleSubmit}
    >
      <Typography sx={{ fontSize: "20px" }}>Welcome back,</Typography>
      <Typography sx={{ color: "#bdbdbd", fontSize: "12px" }}>
        Log in now to continue
      </Typography>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          mt: 4,
        }}
      >
        <img src={TakeAway} alt="Take Away" />
      </Box>
      <Box sx={{ mt: 5 }}>
        {error && (
          <Typography
            sx={{
              mb: 2,
              bgcolor: "#ef9a9a",
              color: "#d50000",
              p: 1,
              fontSize: "0.875rem",
              borderRadius: "3px",
            }}
          >
            {error}
          </Typography>
        )}

        <Typography sx={{ fontSize: "12px", color: "#BDBDBD" }}>
          Email
        </Typography>
        <FormControl
          variant="outlined"
          onChange={(e) => {
            setEmail(e.target.value);

            if (email) {
              setIsEmailError(false);
            }
          }}
          error={!email && isEmailError}
          sx={{
            mt: 0.5,
            "& .MuiOutlinedInput-root": {
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
            },
          }}
          fullWidth
        >
          <OutlinedInput
            placeholder="Enter your email address"
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": { width: "0.75em", margin: "-1px" },
                }}
              >
                <EmailRoundedIcon />
              </InputAdornment>
            }
            sx={{ fontSize: "14px" }}
          />

          {isEmailError && (
            <FormHelperText sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}>
              Please input your email
            </FormHelperText>
          )}
        </FormControl>
      </Box>

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ fontSize: "12px", color: "#BDBDBD" }}>
          Password
        </Typography>
        <FormControl
          variant="outlined"
          sx={{
            mt: 0.5,
            "& .MuiOutlinedInput-root": {
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              alignItems: "center",
            },
          }}
          fullWidth
        >
          <OutlinedInput
            placeholder="Enter your password"
            type={isShowPassword ? "text" : "password"}
            onChange={(e) => {
              setPassword(e.target.value);
              if (password) {
                setIsPasswordError(false);
              }
            }}
            error={!password && isPasswordError}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowPassword ? <Visibility /> : <VisibilityOff />}
                </Box>
              </InputAdornment>
            }
            sx={{ fontSize: "14px" }}
          />
          {isPasswordError ? (
            <FormHelperText sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}>
              Please input your password
            </FormHelperText>
          ) : (
            ""
          )}
        </FormControl>
      </Box>

      <Button
        disabled={isLoading ? true : false}
        sx={{
          mt: 3,
          textTransform: "capitalize",
          color: "#ffffff",
          fontSize: "18px",
          bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
          height: "45px",
          borderRadius: "8px",
          display: "flex",
          gap: 1,
          justifyContent: "center",
          alignItems: "center",
          ":hover": {
            bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
            color: "#ffffff",
          },
        }}
        type="submit"
        fullWidth
      >
        {isLoading ? (
          <CircularProgress
            sx={{ color: "grey" }}
            thickness={4}
            size={20}
            disableShrink
          />
        ) : (
          ""
        )}
        Login
      </Button>

      <Typography
        sx={{
          fontSize: "0.85rem",
          color: "#000000",
          fontWeight: 500,
          textAlign: "center",
          mt: 2,
        }}
      >
        Forget password?{" "}
        <span
          style={{ fontWeight: 600 }}
          onClick={() => navigate("/recovery-password")}
        >
          Click here!
        </span>
      </Typography>
    </Box>
  );
};

export default Login;
