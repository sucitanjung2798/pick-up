import React, { useEffect, useState } from "react";
import {
  useNavigate,
  useSearchParams,
  createSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import { deletePermission, getPermissions } from "../../api/permissions";
import PermissionCard from "./PermissionCard";
import ScrollOnTop from "../../shared/ScrollOnTop";
import SearchWithDebounce from "../../shared/SearchWithDebounce";

const Permissions = ({ permissions }) => {
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  const [permissionsData, setPermissionsData] = useState([] || null);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const [idPermission, setIdPermission] = useState(null);

  const [openDialog, setOpenDialog] = useState(false);

  const [hasMore, setHasMore] = useState(false);

  const fetchPermissions = async ({ search }) => {
    setIsLoading(true);
    try {
      const res = await getPermissions({ search });
      setPermissionsData(res?.results?.data);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchPermissions({ search: savedSearch || "" });
  }, [savedSearch]);

  const handleDeletePermission = async (idPermission) => {
    try {
      setIsLoadingDelete(true);
      await deletePermission(idPermission);
      updatedSearchParams.delete("search");
      setSearch("");
      fetchPermissions({ search: savedSearch || "" });
      setOpenDialog(false);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoadingDelete(false);
    }
  };

  return (
    <Box sx={{ pb: 4 }}>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Permissions" isNotificationsNeed={true} />

        <Box sx={{ px: 2, pt: 2 }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            <SearchWithDebounce
              search={search}
              setSearch={setSearch}
              setSearchParams={setSearchParams}
              updatedSearchParams={updatedSearchParams}
              setSavedSearch={setSavedSearch}
            />
            <Button
              sx={{
                border: "1px solid #78251E",
                borderRadius: "10px",
                px: 3,
                height: "40px",
                textTransform: "capitalize",
                color: "#000",
                fontWeight: 600,
              }}
              startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
              onClick={() => navigate("/add-new-permission")}
            >
              New
            </Button>
          </Box>
        </Box>
      </Box>

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}>
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              variant="rounded"
              height="50px"
              animation="wave"
              key={index}
              sx={{ mt: 2 }}
            />
          ))}

        {!isLoading && permissionsData?.length === 0 && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <Typography sx={{ textAlign: "center" }}>
              The Permission is empty
            </Typography>
          </Box>
        )}

        <ScrollOnTop />

        {!isLoading && permissionsData?.length > 0 && (
          <PermissionCard
            permissionsData={permissionsData}
            setOpenDialog={setOpenDialog}
            setIdPermission={setIdPermission}
            openDialog={openDialog}
            handleDeletePermission={handleDeletePermission}
            idPermission={idPermission}
            setPermissionsData={setPermissionsData}
            hasMore={hasMore}
            setHasMore={setHasMore}
            search={savedSearch}
            permissions={permissions}
            isLoadingDelete={isLoadingDelete}
          />
        )}
      </Box>
    </Box>
  );
};

export default Permissions;
