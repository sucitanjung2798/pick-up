import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Skeleton,
  Typography,
} from "@mui/material";
import MenuComponent from "../../shared/MenuComponent";
import UserRole from "../../assets/user-role.png";
import CustomDialog from "../../shared/CustomDialog";
import { getPermissions } from "../../api/permissions";
import InfiniteScroll from "react-infinite-scroll-component";

const PermissionCard = ({
  permissionsData,
  setOpenDialog,
  setIdPermission,
  openDialog,
  handleDeletePermission,
  idPermission,
  setPermissionsData,
  hasMore,
  setHasMore,
  search,
  permissions,
  isLoadingDelete,
}) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMorePermissions = async ({ page, search }) => {
    const {
      results: {
        data: newPermission,
        pages: { currentPage, totalPages },
      },
    } = await getPermissions({
      page,
      search,
    });

    setPermissionsData([...permissionsData, ...newPermission]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  return (
    <Box>
      <InfiniteScroll
        dataLength={permissionsData?.length || 0}
        next={() => fetchMorePermissions({ page, search })}
        hasMore={hasMore}
        loader={
          permissionsData?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
        // endMessage={<Typography>End</Typography>}
      >
        <Box sx={{ mt: 2, display: "flex", flexDirection: "column", gap: 2 }}>
          {permissionsData?.map((permission, index) => {
            const menuLists = [
              {
                name: "Edit Permission",
                clickHandler: () => {
                  navigate(`/permission/edit/${permission.uac}`);
                },
              },
              {
                name: "Delete Permission",
                clickHandler: () => {
                  setOpenDialog(true);
                  setIdPermission(permission.uac);
                },
              },
            ];
            return (
              <Box
                sx={{
                  boxShadow: 3,
                  p: 2,
                  borderRadius: "10px",
                }}
                key={index}
              >
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 1,
                  }}
                >
                  <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                    <Box
                      component="img"
                      src={UserRole}
                      sx={{ width: "20px", height: "20px" }}
                    />
                    <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
                      {permission.section}
                    </Typography>
                  </Box>

                  <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                    <Typography
                      sx={{
                        fontSize: "0.75rem",
                        bgcolor: "#2196F3",
                        // permission.status === "Active"
                        //   ? "#2196F3"
                        //   : "#B80000",
                        color: "#ffffff",
                        py: 0.5,
                        px: 1,
                        borderRadius: "50px",
                      }}
                    >
                      {permission.operation}
                    </Typography>
                    <MenuComponent
                      menuLists={menuLists}
                      id={permission.accessControl}
                    />
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
      </InfiniteScroll>

      <CustomDialog open={openDialog} handleClose={() => setOpenDialog(false)}>
        {!permissions.includes("perm.delete") ? (
          <>
            <DialogContent>
              <DialogContentText
                id="alert-dialog-description"
                sx={{ color: "#000000" }}
              >
                You are not permitted to access this action
              </DialogContentText>
            </DialogContent>
            <DialogActions sx={{ px: 2 }}>
              <Button
                sx={{
                  backgroundColor: "#78251E",
                  border: "1px solid #78251E",
                  color: "#fff",
                  textTransform: "capitalize",
                  px: 2,
                  borderRadius: "8px",
                }}
                onClick={() => setOpenDialog(false)}
                autoFocus
                size="small"
              >
                Close
              </Button>
            </DialogActions>{" "}
          </>
        ) : (
          <>
            <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
              Delete Permission
            </DialogTitle>
            <DialogContent>
              <DialogContentText
                id="alert-dialog-description"
                sx={{ color: "#000000" }}
              >
                Are you sure want to delete this permission?
              </DialogContentText>
            </DialogContent>
            <DialogActions sx={{ px: 2 }}>
              <Button
                sx={{
                  border: "1px solid #78251E",
                  textTransform: "capitalize",
                  color: "#000000",
                  px: 2,
                  borderRadius: "8px",
                  mr: 1,
                }}
                size="small"
                onClick={() => setOpenDialog(false)}
              >
                Cancel
              </Button>
              <Button
                disabled={isLoadingDelete ? true : false}
                sx={{
                  bgcolor: !isLoadingDelete ? "#78251E" : "#bdbdbd",
                  border: "1px solid #78251E",
                  color: "#fff",
                  textTransform: "capitalize",
                  px: 2,
                  borderRadius: "8px",
                  ":hover": {
                    bgcolor: !isLoadingDelete ? "#78251E" : "#bdbdbd",
                    color: "#ffffff",
                  },
                }}
                onClick={() => handleDeletePermission(idPermission)}
                autoFocus
                size="small"
              >
                {isLoadingDelete ? (
                  <CircularProgress
                    sx={{ color: "grey" }}
                    thickness={4}
                    size={20}
                    disableShrink
                  />
                ) : (
                  ""
                )}
                Delete
              </Button>
            </DialogActions>
          </>
        )}
      </CustomDialog>
    </Box>
  );
};

export default PermissionCard;
