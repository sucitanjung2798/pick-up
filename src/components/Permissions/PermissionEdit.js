import React, { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Box,
  FormControl,
  FormHelperText,
  OutlinedInput,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import { useState } from "react";
import { editPermission, getDetailPermission } from "../../api/permissions";
import CustomButtonSave from "../../shared/CustomButtonSave";

const PermissionEdit = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [accessControl, setAccessControl] = useState("");
  const [description, setDescription] = useState("");
  const [section, setSection] = useState("");
  const [operation, setOperation] = useState("show");

  const [isAccessControlError, setIsAccessControlError] = useState(false);
  const [isSectionError, setIsSectionError] = useState(false);
  const [isOperationError, setIsOperationError] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getPermissionDetail = async () => {
      try {
        setIsLoading(true);
        const {
          result: { section, uac, description, operation },
        } = await getDetailPermission(id);
        setAccessControl(uac);
        setDescription(description);
        setOperation(operation);
        setSection(section);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    };

    getPermissionDetail(id);
  }, [id]);

  const [isLoadingEdit, setIsLoadingEdit] = useState(false);

  const handleEdit = async (event) => {
    event.preventDefault();

    if (section !== "" && accessControl !== "" && operation !== "") {
      setIsLoadingEdit(true);

      const data = {
        operation,
        section,
        description,
      };

      await editPermission({ data, id })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          setIsLoadingEdit(true);
          navigate("/permissions");
        });
    } else {
      accessControl !== ""
        ? setIsAccessControlError(false)
        : setIsAccessControlError(true);
      section !== "" ? setIsSectionError(false) : setIsSectionError(true);
      operation !== "" ? setIsOperationError(false) : setIsOperationError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Add New Permission" isNotificationsNeed={true} />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate("/permissions")} />
        </Box>
      </Box>

      <Box component="form" onSubmit={handleEdit} sx={{ p: 2 }}>
        {isLoading && (
          <Skeleton
            // sx={{ mt: 2 }}
            variant="rounded"
            height="300px"
            animation="wave"
          />
        )}

        {!isLoading && (
          <Box
            sx={{
              border: "1px solid #bdbdbd",

              p: 2,
              borderRadius: "10px",
            }}
          >
            <Box>
              <Typography>
                Access Control <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter access control"
                  sx={{ fontSize: "0.875rem" }}
                  value={accessControl}
                  error={!accessControl && isAccessControlError}
                  onChange={(e) => {
                    setAccessControl(e.target.value);

                    if (accessControl) {
                      setIsAccessControlError(false);
                    }
                  }}
                />

                {isAccessControlError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please input access control
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Section <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter your permission"
                  sx={{ fontSize: "0.875rem" }}
                  value={section}
                  error={!section && isSectionError}
                  onChange={(e) => {
                    setSection(e.target.value);

                    if (section) {
                      setIsSectionError(false);
                    }
                  }}
                />

                {isSectionError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please input section
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>
                Operation <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter your permission"
                  sx={{ fontSize: "0.875rem" }}
                  value={operation}
                  error={!operation && isOperationError}
                  onChange={(e) => {
                    setOperation(e.target.value);

                    if (operation) {
                      setIsOperationError(false);
                    }
                  }}
                />

                {isOperationError && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please input operation
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>Description</Typography>
              <Box
                sx={{
                  mt: 0.5,
                  borderRadius: "10px",
                  "& .MuiTextField-root": { width: "100%" },
                  "& .MuiOutlinedInput-root": {
                    borderColor: "#bdbdbd",
                    borderRadius: "10px",
                    fontSize: "0.875rem",
                  },
                }}
                noValidate
                autoComplete="off"
                placeholder="Input your description"
              >
                <TextField
                  multiline
                  rows={4}
                  placeholder="Input your description"
                  value={description || ""}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Box>
            </Box>
          </Box>
        )}

        {isLoading && (
          <Skeleton
            sx={{ mt: 2 }}
            variant="rounded"
            height="50px"
            animation="wave"
          />
        )}

        {!isLoading && (
          <Box sx={{ mt: 2 }}>
            <CustomButtonSave isLoading={isLoadingEdit} content="Save" />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default PermissionEdit;
