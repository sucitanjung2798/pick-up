import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import AuthorizationImage from "../assets/authorization.svg";

const Unauthorized = () => {
  const navigate = useNavigate();

  return (
    <Box>
      <Box component="img" src={AuthorizationImage} />

      <Box sx={{ textAlign: "center", p: 2 }}>
        <Typography sx={{ fontWeight: 600, fontSize: "20px" }}>
          We're Sorry
        </Typography>

        <Box sx={{ mt: 2 }}>
          <Typography sx={{ fontSize: "14px", color: "#757575" }}>
            You don't have permission to access this page.
          </Typography>
          <Typography sx={{ fontSize: "14px", color: "#757575" }}>
            We suggest you to go back to your home page!
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button
            sx={{
              mt: 3,
              textTransform: "capitalize",
              color: "#ffffff",
              fontSize: "18px",
              bgcolor: "#78251E",
              height: "45px",
              borderRadius: "8px",
              display: "flex",
              width: 200,
              gap: 1,
              justifyContent: "center",
              alignItems: "center",
              ":hover": {
                bgcolor: "#78251E",
                color: "#ffffff",
              },
            }}
            fullWidth
            onClick={() => navigate("/dashboard")}
          >
            Go Back Home
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default Unauthorized;
