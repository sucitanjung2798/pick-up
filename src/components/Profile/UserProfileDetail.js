import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";
import { AuthContext } from "../../contexts/AuthContext";

const UserProfileDetail = () => {
  const navigate = useNavigate();
  const { profileData } = useContext(AuthContext);

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
          p: 2,
          color: "#fff",
          bgcolor: "#78251E",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: 0.5 }}>
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            User Profile Detail
          </Typography>
        </Box>
      </Box>

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 1 }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Email
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.emailAddress || "-"}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Name
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.name || "-"}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Employee ID
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.employeeId || "-"}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Phone Number
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.phoneNumber || "-"}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Status
          </Typography>
          <Typography
            sx={{
              fontSize: "0.875rem",
              color: `#${profileData?.status?.color}`,
            }}
          >
            {profileData?.status?.label || "-"}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Role
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.role?.name || "-"}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Department
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.department?.name || "-"}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
            Location
          </Typography>
          <Typography sx={{ fontSize: "0.875rem" }}>
            {profileData?.department?.location || "-"}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default UserProfileDetail;
