import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";

const HelpCenterDetail = () => {
  const navigate = useNavigate();

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
          p: 2,
          color: "#fff",
          bgcolor: "#78251E",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: 0.5 }}>
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Help Center
          </Typography>
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "50vh",
        }}
      >
        Coming Soon
      </Box>
    </Box>
  );
};

export default HelpCenterDetail;
