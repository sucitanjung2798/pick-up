import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, FormHelperText, InputAdornment, Typography } from "@mui/material";
import {
  ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import CustomInput from "../../shared/CustomInput";
import CustomButtonSave from "../../shared/CustomButtonSave";
import { changePassword } from "../../api/profile";

const ChangePasswordProfile = () => {
  const navigate = useNavigate();

  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [isOldPasswordError, setIsOldPassword] = useState(false);
  const [isNewPasswordError, setIsNewPassword] = useState(false);
  const [isConfirmPasswordError, setIsConfirmPassword] = useState(false);

  const [isShowOldPassword, setIsShowOldPassword] = useState(false);
  const [isShowNewPassword, setIsShowNewPassword] = useState(false);
  const [isShowConfirmPassword, setIsShowConfirmPassword] = useState(false);
  const [isNewPasswordLengthValid, setIsNewPasswordLengthValid] =
    useState(false);

  const handleClickShowOldPassword = () =>
    setIsShowOldPassword((show) => !show);
  const handleClickShowNewPassword = () =>
    setIsShowNewPassword((show) => !show);
  const handleClickShowConfirmPassword = () =>
    setIsShowConfirmPassword((show) => !show);

  const handleMouseDownOldPassword = (event) => {
    event.preventDefault();
  };
  const handleMouseDownNewPassword = (event) => {
    event.preventDefault();
  };
  const handleMouseDownConfirmPassword = (event) => {
    event.preventDefault();
  };

  const [isLoading, setIsLoading] = useState(false);

  const handleSubmitChangePassword = async (e) => {
    e.preventDefault();
    if (
      oldPassword !== "" &&
      newPassword !== "" &&
      confirmPassword !== "" &&
      newPassword.length >= 8
    ) {
      setIsLoading(true);
      try {
        const data = {
          password: oldPassword,
          newPassword,
          confirmNewPassword: confirmPassword,
        };
        await changePassword(data);
      } catch (err) {
        console.error(err?.response?.data?.errors);
      } finally {
        setIsLoading(false);
        navigate("/user-profile");
      }
    } else {
      oldPassword !== "" ? setIsOldPassword(false) : setIsOldPassword(true);
      newPassword !== "" ? setIsNewPassword(false) : setIsNewPassword(true);
      confirmPassword !== ""
        ? setIsConfirmPassword(false)
        : setIsConfirmPassword(true);
      newPassword.length >= 8
        ? setIsNewPasswordLengthValid(false)
        : setIsNewPasswordLengthValid(true);
    }
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
          p: 2,
          color: "#fff",
          bgcolor: "#78251E",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: 0.5 }}>
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Change Password
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}
        component="form"
        onSubmit={handleSubmitChangePassword}
      >
        <Box>
          <Typography>
            Old Password <span style={{ color: "#B80000" }}>*</span>
          </Typography>

          <CustomInput
            value={oldPassword || ""}
            placeholder="Enter the old password"
            type={isShowOldPassword ? "text" : "password"}
            sx={{ fontSize: "0.875rem" }}
            isValueError={isOldPasswordError}
            isHelperText={true}
            onChange={(e) => {
              setOldPassword(e.target.value);

              if (oldPassword) {
                setIsOldPassword(false);
              }
            }}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowOldPassword}
                  onMouseDown={handleMouseDownOldPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowOldPassword ? <Visibility /> : <VisibilityOff />}
                </Box>
              </InputAdornment>
            }
            helperText={
              isOldPasswordError && (
                <FormHelperText
                  sx={{
                    ml: 0,
                    fontSize: "11px",
                    color: "#78251E",
                  }}
                >
                  Please input your old password
                </FormHelperText>
              )
            }
          />
        </Box>

        <Box>
          <Typography>
            New Password <span style={{ color: "#B80000" }}>*</span>
          </Typography>

          <CustomInput
            value={newPassword || ""}
            placeholder="Enter the new password"
            type={isShowNewPassword ? "text" : "password"}
            sx={{ fontSize: "0.875rem" }}
            isValueError={isNewPasswordError}
            isHelperText={true}
            onChange={(e) => {
              setNewPassword(e.target.value);

              if (newPassword) {
                setIsNewPassword(false);
              }

              if (newPassword.length >= 8) {
                setIsNewPasswordLengthValid(false);
              } else {
                setIsNewPasswordLengthValid(true);
              }
            }}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowNewPassword}
                  onMouseDown={handleMouseDownNewPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowNewPassword ? <Visibility /> : <VisibilityOff />}
                </Box>
              </InputAdornment>
            }
            helperText={
              isNewPasswordError && (
                <FormHelperText
                  sx={{
                    ml: 0,
                    fontSize: "11px",
                    color: "#78251E",
                  }}
                >
                  Please input your new password
                </FormHelperText>
              )
            }
          />
          {isNewPasswordLengthValid && (
            <span style={{ fontSize: "11px", color: "#78251E" }}>
              The New Password field must be at least 8 characters in length
            </span>
          )}
        </Box>

        <Box>
          <Typography>
            Confirm New Password <span style={{ color: "#B80000" }}>*</span>
          </Typography>

          <CustomInput
            value={confirmPassword || ""}
            placeholder="Enter the confirm new password"
            type={isShowConfirmPassword ? "text" : "password"}
            sx={{ fontSize: "0.875rem" }}
            isValueError={isConfirmPasswordError}
            isHelperText={true}
            onChange={(e) => {
              setConfirmPassword(e.target.value);

              if (confirmPassword) {
                setIsConfirmPassword(false);
              }
            }}
            startAdornment={
              <InputAdornment
                position="start"
                sx={{
                  "& .MuiSvgIcon-root": {
                    width: "0.75em",
                    p: 0,
                  },
                }}
              >
                <Box
                  onClick={handleClickShowConfirmPassword}
                  onMouseDown={handleMouseDownConfirmPassword}
                  sx={{ mt: 0.5 }}
                >
                  {!isShowConfirmPassword ? <Visibility /> : <VisibilityOff />}
                </Box>
              </InputAdornment>
            }
            helperText={
              isConfirmPasswordError && (
                <FormHelperText
                  sx={{
                    ml: 0,
                    fontSize: "11px",
                    color: "#78251E",
                  }}
                >
                  Please input your confirm new password
                </FormHelperText>
              )
            }
          />
        </Box>
        <CustomButtonSave content="Save" isLoading={isLoading} />
      </Box>
    </Box>
  );
};

export default ChangePasswordProfile;
