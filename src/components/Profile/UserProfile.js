import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  DialogActions,
  DialogContent,
  Typography,
  Button,
  CircularProgress,
} from "@mui/material";
import {
  // ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon,
  SettingsOutlined as SettingsOutlinedIcon,
  HelpOutlineOutlined as HelpOutlineOutlinedIcon,
  ArrowForwardIosRounded as ArrowForwardIosRoundedIcon,
  ChangeCircleOutlined as ChangeCircleOutlinedIcon,
  RestartAltRounded as RestartAltRoundedIcon,
} from "@mui/icons-material";
import { ReactComponent as LogoutIcon } from "../../assets/logout-icon.svg";
import { AuthContext } from "../../contexts/AuthContext";
import Loading from "../../shared/Loading";
import CustomDialog from "../../shared/CustomDialog";
import { resetPassword } from "../../api/auth";

const UserProfile = () => {
  const navigate = useNavigate();
  const { profileData, isProfileLoading, handleLogout } =
    useContext(AuthContext);

  const [name, setName] = useState([]);

  useEffect(() => {
    const separateEachName = profileData?.name?.split(" ");

    const createFirstLetter = separateEachName?.map((name) => name.charAt(0));
    setName(createFirstLetter);
  }, [profileData?.name]);

  const [openDialog, setOpenDialog] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleResetPassword = async () => {
    setIsLoading(true);
    try {
      const data = {
        id: profileData?.userId,
      };
      await resetPassword(data);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
          p: 2,
          color: "#fff",
          bgcolor: "#78251E",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: 0.5 }}>
          {/* <ArrowBackIosNewRoundedIcon onClick={() => navigate("/dashboard")} /> */}
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            User Profile
          </Typography>
        </Box>

        <Box sx={{ display: "flex", alignItems: "center" }}>
          <SettingsOutlinedIcon onClick={() => navigate("/settings")} />
        </Box>
      </Box>

      {isProfileLoading && <Loading />}

      {!isProfileLoading && (
        <Box sx={{ p: 2 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 1,
              p: 2,
              boxShadow: 2,
              bgcolor: "#fff",

              borderRadius: "6px",
            }}
            onClick={() => navigate("/user-profile-detail")}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography
                sx={{
                  bgcolor: "rgb(247, 249, 250)",
                  border: "2px solid rgb(255, 255, 255)",
                  color: "rgb(104, 113, 118)",
                  width: "64px",
                  height: "64px",
                  borderRadius: "25px",
                  fontSize: "20px",
                  display: "flex",
                  alignItems: "center",
                  flexDirection: "column",
                  justifyContent: "center",
                  boxShadow: "0px 1px 2px rgba(3,18,26,0.20)",
                }}
              >
                {name?.map((item) => item)}
              </Typography>

              <Box>
                <Typography sx={{ fontWeight: 600 }}>
                  {profileData?.name}
                </Typography>
                <Typography
                  sx={{ color: "rgb(104, 113, 118)", fontSize: "0.75rem" }}
                >
                  {profileData?.emailAddress}
                </Typography>
                <Typography
                  sx={{ fontSize: "0.875rem", color: "rgb(104, 113, 118)" }}
                >
                  {profileData?.department?.name}
                </Typography>
                <Typography
                  sx={{ fontSize: "0.875rem", color: "rgb(104, 113, 118)" }}
                >
                  {profileData?.employeeId}
                </Typography>
              </Box>
            </Box>

            <ArrowForwardIosRoundedIcon
              sx={{ fontSize: "18px", color: "#78251E" }}
            />
          </Box>

          <Box sx={{ mt: 3 }}>
            <Box
              sx={{
                boxShadow: 2,
                bgcolor: "#fff",
                p: 2,
                borderRadius: "6px",
                display: "flex",
                alignItems: "center",
                gap: 2,
                justifyContent: "space-between",
              }}
              onClick={() => navigate("/help-center-detail")}
            >
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <HelpOutlineOutlinedIcon sx={{ fontSize: "24px" }} />
                <Box>
                  <Typography sx={{ fontSize: "14px", fontWeight: 600 }}>
                    FAQ
                  </Typography>
                  <Typography
                    sx={{ fontSize: "14px", color: "rgba(104,113,118,1.00)" }}
                  >
                    Find the best answer to your questions
                  </Typography>
                </Box>
              </Box>

              <ArrowForwardIosRoundedIcon
                sx={{ fontSize: "18px", color: "#78251E" }}
              />
            </Box>

            <Box
              sx={{
                boxShadow: 2,
                bgcolor: "#fff",
                p: 2,
                borderRadius: "6px",
                display: "flex",
                alignItems: "center",
                gap: 2,
                justifyContent: "space-between",
                mt: 2,
              }}
              onClick={() => navigate("/change-password")}
            >
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <ChangeCircleOutlinedIcon />
                <Box>
                  <Typography sx={{ fontSize: "14px", fontWeight: 600 }}>
                    Change Password
                  </Typography>
                  <Typography
                    sx={{ fontSize: "14px", color: "rgba(104,113,118,1.00)" }}
                  >
                    Change your password into the strongest one
                  </Typography>
                </Box>
              </Box>

              <ArrowForwardIosRoundedIcon
                sx={{ fontSize: "18px", color: "#78251E" }}
              />
            </Box>

            <Box
              sx={{
                boxShadow: 2,
                bgcolor: "#fff",
                p: 2,
                borderRadius: "6px",
                display: "flex",
                alignItems: "center",
                gap: 2,
                justifyContent: "space-between",
                mt: 2,
              }}
              onClick={() => setOpenDialog(true)}
            >
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <RestartAltRoundedIcon />
                <Box>
                  <Typography sx={{ fontSize: "14px", fontWeight: 600 }}>
                    Reset Password
                  </Typography>
                </Box>
              </Box>

              <ArrowForwardIosRoundedIcon
                sx={{ fontSize: "18px", color: "#78251E" }}
              />
            </Box>

            <Box
              sx={{
                boxShadow: 2,
                bgcolor: "#fff",
                p: 2,
                borderRadius: "6px",
                display: "flex",
                alignItems: "center",
                gap: 2,
                justifyContent: "space-between",
                mt: 2,
              }}
              onClick={handleLogout}
            >
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <LogoutIcon />
                <Box>
                  <Typography sx={{ fontSize: "14px", fontWeight: 600 }}>
                    Logout
                  </Typography>
                </Box>
              </Box>

              <ArrowForwardIosRoundedIcon
                sx={{ fontSize: "18px", color: "#78251E" }}
              />
            </Box>

            {/* <Box
              sx={{
                position: "absolute",
                bottom: 70,
                textAlign: "center",
                left: 170,
              }}
            >
              <Typography>Halo</Typography>
            </Box> */}
          </Box>
        </Box>
      )}

      <CustomDialog open={openDialog} handleClose={() => setOpenDialog(false)}>
        <DialogContent>
          <Box>Are you sure want to reset the password?</Box>
        </DialogContent>
        <DialogActions sx={{ px: 3 }}>
          <Button
            sx={{
              border: "1px solid #78251E",
              color: "#78251E",
              textTransform: "capitalize",
              ":hover": {
                border: "1px solid #78251E",
              },
            }}
            onClick={() => setOpenDialog(false)}
            size="small"
          >
            Cancel
          </Button>
          <Button
            sx={{
              bgcolor: isLoading ? "#bdbdbd" : "#78251E",
              border: `1px solid ${isLoading ? "#bdbdbd" : "#78251E"}`,
              textTransform: "capitalize",
              color: "#fff",
              ":hover": {
                bgcolor: isLoading ? "#bdbdbd" : "#78251E",
                border: `1px solid ${isLoading ? "#bdbdbd" : "#78251E"}`,
              },
            }}
            disabled={isLoading ? true : false}
            onClick={handleResetPassword}
            size="small"
          >
            {isLoading ? (
              <CircularProgress
                sx={{ color: "grey" }}
                thickness={4}
                size={20}
                disableShrink
              />
            ) : (
              "Reset"
            )}
          </Button>
        </DialogActions>
      </CustomDialog>
    </Box>
  );
};
export default UserProfile;
