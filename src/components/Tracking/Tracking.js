import React, { useState } from "react";
import { Close as CloseIcon } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import Header from "../../shared/Header";
import { trackingPackage } from "../../api/package";
import dayjs from "dayjs";

const Tracking = () => {
  const [trackingValue, setTrackingValue] = useState([]);
  const [isTrackingValueError, setIsTrackingValueError] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const handleInputChange = (event) => {
    const newValue = event.target.value;

    if (newValue.length > 0) {
      setTrackingValue(newValue.split(",").map((item) => item.trim()));
    } else {
      setTrackingValue([]);
    }

    if (newValue.length > 0) {
      setIsTrackingValueError(false);
    } else {
      setIsTrackingValueError(true);
    }
  };

  const handleClearTracking = () => {
    setTrackingValue([]);
  };

  const [trackingData, setTrackingData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmitAwb = async (e) => {
    e.preventDefault();
    if (trackingValue.length > 0) {
      setIsLoading(true);
      try {
        setTrackingData([]);
        setErrorMessage(null);

        const dataTracking = {
          tracking_ids: trackingValue,
        };
        const res = await trackingPackage(dataTracking);

        const responseData = res.results.data;

        const arrayData = responseData.map(
          (item, index) =>
            Object.keys(responseData[index]).map((key) => {
              return {
                awb: key,
                data: item[key],
              };
            })[0]
        );

        setTrackingData(arrayData);
      } catch (err) {
        const errStatus = err?.response?.status;

        if (errStatus === 400) {
          setErrorMessage("The Tracking ID is empty or invalid");
        }
      } finally {
        setIsLoading(false);
      }
    } else {
      trackingValue.length > 0
        ? setIsTrackingValueError(false)
        : setIsTrackingValueError(true);
    }
  };

  return (
    <Box>
      <Box
        sx={{
          position: "sticky",
          top: 0,
          bgcolor: "#ffffff",
          zIndex: 10,
          width: "100%",
        }}
      >
        <Header title="Tracking Package" isNotificationsNeed={true} />

        <Typography
          sx={{
            textAlign: "center",
            mt: 4,
            fontSize: "28px",
            lineHeight: "36px",
            color: "#78251E",
            fontWeight: 500,
          }}
        >
          Pelacakan
        </Typography>

        <Typography sx={{ textAlign: "center", mt: 2 }}>
          Silahkan masukkan nomor resi anda
        </Typography>

        <Box sx={{ px: 2 }} component="form" onSubmit={handleSubmitAwb}>
          <TextField
            value={trackingValue.length > 0 ? trackingValue.join(", ") : ""}
            onChange={handleInputChange}
            fullWidth
            sx={{
              mt: 2,
              "& .MuiOutlinedInput-root": {
                borderRadius: "10px",
                fontSize: "0.875rem",
              },
            }}
            multiline
            maxLength={3}
            error={trackingValue.length === 0 && isTrackingValueError}
            placeholder="Masukkan nomor resi"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  {trackingValue.length > 0 && (
                    <CloseIcon
                      onClick={handleClearTracking}
                      sx={{ cursor: "pointer" }}
                    />
                  )}
                </InputAdornment>
              ),
            }}
          />
          {isTrackingValueError && (
            <span style={{ color: "#d50000", fontSize: "0.75rem" }}>
              Silahkan masukkan nomor resi anda
            </span>
          )}

          <Typography sx={{ mt: 3, fontSize: "10px" }}>
            Tersedia sampai 3 resi
          </Typography>

          <Box
            sx={{
              display: "flex",
              gap: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button
              sx={{
                backgroundColor: "#78251E",
                border: "1px solid #78251E",
                color: "#fff",
                textTransform: "capitalize",
                px: 4,
                borderRadius: "8px",
                display: "flex",
                gap: 1,
                height: "45px",
                fontSize: "18px",
                mt: 3,
                alignItems: "center",
                ":hover": {
                  bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
                  color: "#ffffff",
                  border: `1px solid ${!isLoading ? "#78251E" : "#bdbdbd"} `,
                },
              }}
              disabled={isLoading ? true : false}
              autoFocus
              type="submit"
            >
              {isLoading && (
                <CircularProgress
                  sx={{ color: "grey" }}
                  thickness={4}
                  size={20}
                  disableShrink
                />
              )}
              <Typography sx={{ fontSize: "18px" }}>Lacak Paket</Typography>
            </Button>
          </Box>
        </Box>

        <Box>
          <Box
            sx={{
              mt: 4,
              px: 2,
              display: "flex",
              flexDirection: "column",
              gap: 2,
            }}
          >
            <Typography sx={{ textAlign: "center", color: "#d50000" }}>
              {errorMessage}
            </Typography>
            {trackingData?.length > 0 &&
              trackingData?.map((dataTracking, index) => {
                const extractedAwb = dataTracking?.awb?.substring(7);
                return (
                  <Box
                    sx={{
                      border: "1px solid #bdbdbd",
                      px: 2,
                      pt: 2,
                      borderRadius: "25px",
                    }}
                    key={index}
                  >
                    <Typography
                      sx={{ fontSize: "0.875rem", mb: 1, fontWeight: 600 }}
                    >
                      AWB Number:{` BISA-AWB-${extractedAwb}`}
                    </Typography>
                    {dataTracking?.data?.map((item, index) => {
                      return (
                        <div className="step" key={index}>
                          <div
                            style={{
                              marginRight: "10px",
                              textAlign: "right",
                            }}
                          >
                            <div style={{ fontSize: "0.75rem" }}>
                              {dayjs(item?.updatedAt?.date).format(
                                "DD/MM/YYYY"
                              )}
                            </div>
                            <div style={{ fontSize: "0.75rem" }}>
                              {dayjs(item?.updatedAt?.date).format("HH:mm")}
                            </div>
                          </div>
                          <div className="v-stepper">
                            <div className="circle"></div>
                            <div className="line"></div>
                          </div>

                          <div
                            className="content"
                            style={{
                              marginBottom: "25px",
                            }}
                          >
                            <div
                              style={{
                                fontWeight: 500,
                                fontSize: "0.875rem",
                              }}
                            >
                              {item?.description}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </Box>
                );
              })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Tracking;
