import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Skeleton, Typography } from "@mui/material";
import PackageImage from "../../assets/package-icon.png";
import dayjs from "dayjs";
import { historyPackage } from "../../api/package";
import InfiniteScroll from "react-infinite-scroll-component";

const HistoryCard = ({
  historyLists,
  setHistoryLists,
  hasMore,
  setHasMore,
}) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMoreHistory = async ({ page, search, activity }) => {
    const {
      results: {
        data: newHistory,
        pages: { totalPages, currentPage },
      },
    } = await historyPackage({ page, search, activity });
    setHistoryLists([...historyLists, ...newHistory]);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  return (
    <Box>
      <InfiniteScroll
        dataLength={historyLists?.length || 0}
        next={() => fetchMoreHistory({ page })}
        hasMore={hasMore}
        loader={
          historyLists?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2.5, mt: 1 }}>
          {historyLists?.map((item, index) => (
            <Box
              sx={{
                border: "1px solid #bdbdbd",
                borderRadius: "10px",
                px: 2,
                py: 1,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
              key={index}
              onClick={() => navigate(`/package/detail/${item.packageId}`)}
            >
              <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                <img src={PackageImage} alt="Package" />
                <Box>
                  <Typography sx={{ fontWeight: 600, fontSize: "0.875rem" }}>
                    {item?.senderName}
                  </Typography>
                  <Typography sx={{ fontSize: "0.625rem" }}>
                    {dayjs(item?.eta?.date).format("DD-MM-YYYY")}
                  </Typography>
                  <Typography sx={{ fontSize: "0.75rem", color: "#bdbdbd" }}>
                    Sending To : {item?.recipientName}
                  </Typography>
                </Box>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  alignItems: "center",
                  border: `1px solid #${item?.status?.color}`,
                  px: 1,
                  py: 0.5,
                  borderRadius: "8px",
                  color: `#${item?.status?.color}`,
                }}
              >
                <i
                  className={item?.status?.icon}
                  style={{ fontSize: "15px" }}
                />
                <Typography
                  sx={{
                    fontSize: "0.75rem",
                    fontWeight: 600,
                  }}
                >
                  {item?.status?.label}
                </Typography>
              </Box>
            </Box>
          ))}
        </Box>
      </InfiniteScroll>
    </Box>
  );
};

export default HistoryCard;
