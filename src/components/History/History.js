import React, { useEffect, useRef, useState } from "react";
import { createSearchParams, useSearchParams } from "react-router-dom";
import { Box, Skeleton, Typography } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import SearchBar from "../../shared/SearchBar";
import HistoryCard from "./HistoryCard";
import { historyPackage } from "../../api/package";
import debounce from "lodash.debounce";
import ListStatusDelivery from "../../shared/ListStatusDelivery";

const historyActivityLists = [
  {
    value: 1,
    label: "sender",
    color: "78251E",
  },
  {
    value: 2,
    label: "recipient",
    color: "78251E",
  },
];

const History = () => {
  const [historyLists, setHistoryLists] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [hasMore, setHasMore] = useState(false);
  const [historyActivity, setHistoryActivity] = useState("All");

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );
  const debounceSearch = useRef(
    debounce((nextValue) => {
      setSavedSearch(nextValue);
    }, 1000)
  ).current;

  const handleChangeSearch = (event) => {
    const { value } = event.target;
    value !== ""
      ? updatedSearchParams.set("search", value)
      : updatedSearchParams.delete("search");

    setSearchParams(updatedSearchParams, { replace: true });
    setSearch(value);
    debounceSearch(value);
  };

  const handleClearSearch = () => {
    updatedSearchParams.delete("search");
    setSearchParams(updatedSearchParams, { replace: true });
    setSearch("");
    debounceSearch("");
  };

  const getHistory = async ({ search, activity }) => {
    setIsLoading(true);

    try {
      const res = await historyPackage({ search, activity });
      setHistoryLists(res?.results?.data);

      const currentPage = res?.results?.pages?.currentPage;
      const totalPage = res?.results?.pages?.totalPages;

      if (totalPage - currentPage) {
        if (currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getHistory({ search: savedSearch, activity: historyActivity });
  }, [historyActivity, savedSearch]);

  return (
    <Box>
      <Box
        sx={{
          position: "sticky",
          top: 0,
          bgcolor: "#ffffff",
          zIndex: 10,
          width: "100%",
        }}
      >
        <Header title="History Package" isNotificationsNeed={true} />

        <Box sx={{ px: 2, pt: 2 }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            <SearchBar
              value={search}
              onChange={handleChangeSearch}
              placeholder="Search History"
              search={search}
              endAdornment={
                search !== "" && (
                  <CloseIcon
                    onClick={handleClearSearch}
                    sx={{ cursor: "pointer" }}
                  />
                )
              }
            />
          </Box>

          <Box sx={{ mt: 2 }}>
            <ListStatusDelivery
              defaultText={"All"}
              listStatus={historyActivityLists}
              selectedFile={historyActivity}
              setSelectedFile={setHistoryActivity}
              isDefault={true}
              textTransform="capitalize"
            />
          </Box>
        </Box>
      </Box>

      <Box sx={{ p: 2 }}>
        {isLoading && (
          <Skeleton sx={{ height: "100px", borderRadius: "10px" }} />
        )}

        {!isLoading && !historyLists && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <Typography sx={{ fontSize: "0.875rem" }}>
              There's no histories package
            </Typography>
          </Box>
        )}

        {!isLoading && historyLists?.length > 0 && (
          <HistoryCard
            historyLists={historyLists}
            setHistoryLists={setHistoryLists}
            hasMore={hasMore}
            setHasMore={setHasMore}
          />
        )}
      </Box>
    </Box>
  );
};

export default History;
