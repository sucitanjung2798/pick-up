import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Grid,
  Typography,
} from "@mui/material";
import {
  ExpandMore as ExpandMoreIcon,
  CheckRounded as CheckRoundedIcon,
} from "@mui/icons-material";
import { roleNamePermission } from "../../helpers/enums";

const PermissionData = ({ tempAccessData }) => {
  const configData = tempAccessData
    ?.filter((item) => item.includes("config"))[0]
    ?.includes("config");

  const roleData = tempAccessData
    ?.filter((item) => item.includes("role"))[0]
    ?.includes("role");

  const permData = tempAccessData
    ?.filter((item) => item.includes("perm"))[0]
    ?.includes("perm");

  const accessDataArray = tempAccessData
    ?.filter((item) => item.includes("access"))[0]
    ?.includes("access");

  const deptData = tempAccessData
    ?.filter((item) => item.includes("dept"))[0]
    ?.includes("dept");

  const userData = tempAccessData
    ?.filter((item) => item.includes("user"))[0]
    ?.includes("user");

  const uomData = tempAccessData
    ?.filter((item) => item.includes("uom"))[0]
    ?.includes("uom");

  const packageData = tempAccessData
    ?.filter((item) => item.includes("package"))[0]
    ?.includes("package");

  const consignData = tempAccessData
    ?.filter((item) => item.includes("consign"))[0]
    ?.includes("consign");

  const settleData = tempAccessData
    ?.filter((item) => item.includes("settle"))[0]
    ?.includes("settle");

  const reportData = tempAccessData
    ?.filter((item) => item.includes("report"))[0]
    ?.includes("report");

  const utilData = tempAccessData
    ?.filter((item) => item.includes("util"))[0]
    ?.includes("util");

  const getSpesificPermission = (perm) =>
    tempAccessData?.filter((item) => item.includes(perm));

  const getRoleNamePermissions = Object.freeze(roleNamePermission);

  return (
    <Box sx={{ mt: 1, border: "1px solid #f5f5f5", borderRadius: "8px" }}>
      {configData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Configuration
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("config").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  key={index}
                  item
                  xs={3}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}
      {roleData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Roles
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("role").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  key={index}
                  item
                  xs={3}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}
      {permData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Permissions
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("perm").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {accessDataArray && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Access
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("access").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {deptData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Department
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("dept").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {userData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            User
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("user").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {uomData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            UOM
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("uom").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {packageData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Package
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("package").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {consignData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Consign
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("consign").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {settleData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Settle
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("settle").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {reportData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Report
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("report").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={3}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}

      {utilData && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            Util
          </AccordionSummary>
          <AccordionDetails>
            <Grid container display="flex" alignItems="center" rowSpacing={1}>
              {getSpesificPermission("util").map((item, index) => (
                <Grid
                  gap={0.5}
                  display="flex"
                  alignItems="center"
                  item
                  xs={6}
                  key={index}
                >
                  <CheckRoundedIcon
                    sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                  />
                  <Typography sx={{ fontSize: "0.75rem" }}>
                    {item[1]}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          </AccordionDetails>
        </Accordion>
      )}
    </Box>
  );
};

export default PermissionData;
