import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Skeleton, Typography } from "@mui/material";
import UserRole from "../../assets/user-role.png";
import { getRoles } from "../../api/roles";
import InfiniteScroll from "react-infinite-scroll-component";

const RoleCard = ({ listRole, setListRole, setHasMore, search, hasMore }) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMoreRoles = async ({ page, search, status }) => {
    const {
      results: { data: newRoles },
      pages: { currentPage, totalPages },
    } = await getRoles({ page, search, status });

    setListRole([...listRole, ...newRoles]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  return (
    <Box>
      <InfiniteScroll
        dataLength={listRole?.length || 0}
        next={() => fetchMoreRoles({ page, search })}
        hasMore={hasMore}
        loader={
          listRole?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
          {listRole.map((role, index) => {
            return (
              <Box
                sx={{
                  p: 2,
                  borderRadius: "10px",
                  boxShadow: 3,
                }}
                key={index}
                onClick={() => navigate(`/role/detail/${role.roleId}`)}
              >
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 1,
                  }}
                >
                  <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                    <Box
                      component="img"
                      src={UserRole}
                      sx={{ width: "20px", height: "20px" }}
                    />
                    <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
                      {role.name}
                    </Typography>
                  </Box>

                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: 1,
                    }}
                  >
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: 1,
                        border: `1px solid #${role?.status?.color}`,
                        color: `#${role?.status?.color}`,
                        py: 0.5,
                        px: 1,
                        borderRadius: "50px",
                      }}
                    >
                      <i
                        className={role?.status?.icon}
                        style={{
                          fontSize: "15px",
                          color: role?.status?.color,
                        }}
                      ></i>
                      <Typography
                        sx={{
                          fontSize: "0.75rem",
                        }}
                      >
                        {role.status?.label}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
      </InfiniteScroll>
    </Box>
  );
};

export default RoleCard;
