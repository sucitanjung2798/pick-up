import React, { useEffect, useState } from "react";
import {
  createSearchParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Box, Button, Skeleton, Typography } from "@mui/material";
import { AddRounded as AddRoundedIcon } from "@mui/icons-material";
import Header from "../../shared/Header";
import { getRoles } from "../../api/roles";
import ScrollOnTop from "../../shared/ScrollOnTop";
import RoleCard from "./RoleCard";
import SearchWithDebounce from "../../shared/SearchWithDebounce";
import ListStatusDelivery from "../../shared/ListStatusDelivery";

const statusLists = [
  { value: 1, label: "Active", color: "00FF00" },
  { value: 2, label: "Inactive", color: "FF0000" },
];

const Roles = () => {
  const navigate = useNavigate();

  const [listRole, setListRole] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [searchParams, setSearchParams] = useSearchParams();
  const updatedSearchParams = createSearchParams(searchParams);

  const [search, setSearch] = useState(searchParams.get("search") || "");
  const [savedSearch, setSavedSearch] = useState(
    searchParams.get("search") || ""
  );

  const [hasMore, setHasMore] = useState(false);
  const [status, setStatus] = useState("All");

  const getAllRoles = async ({ search, status }) => {
    setIsLoading(true);
    try {
      const res = await getRoles({ search, status });

      setListRole(res?.results?.data || []);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getAllRoles({ search: savedSearch || "", status });
  }, [savedSearch, status]);

  return (
    <Box sx={{ pb: 4 }}>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Roles" isNotificationsNeed={true} />

        <Box
          sx={{
            px: 2,
            pt: 2,
            display: "flex",
            flexDirection: "column",
            gap: 2,
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              gap: 2,
            }}
          >
            <SearchWithDebounce
              search={search}
              setSearch={setSearch}
              setSearchParams={setSearchParams}
              updatedSearchParams={updatedSearchParams}
              setSavedSearch={setSavedSearch}
            />
            <Button
              sx={{
                border: "1px solid #78251E",
                borderRadius: "10px",
                px: 3,
                height: "40px",
                textTransform: "capitalize",
                color: "#000",
                fontWeight: 600,
              }}
              onClick={() => navigate("/add-new-role")}
              startIcon={<AddRoundedIcon sx={{ color: "#000" }} />}
            >
              New
            </Button>
          </Box>
          <Box>
            <ListStatusDelivery
              defaultText={"All"}
              listStatus={statusLists}
              selectedFile={status}
              setSelectedFile={setStatus}
              isDefault={true}
              textTransform="capitalize"
            />
          </Box>
        </Box>
      </Box>

      <ScrollOnTop />

      <Box sx={{ p: 2 }}>
        <Box sx={{ mt: 2, display: "flex", flexDirection: "column", gap: 2 }}>
          {isLoading &&
            [1, 2, 3].map((_, index) => (
              <Skeleton
                variant="rounded"
                height="50px"
                animation="wave"
                key={index}
              />
            ))}

          {!isLoading && listRole?.length === 0 && (
            <Box
              sx={{
                height: "50vh",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Typography>The role list is empty</Typography>
            </Box>
          )}

          {!isLoading && listRole?.length > 0 && (
            <RoleCard
              listRole={listRole}
              setListRole={setListRole}
              hasMore={hasMore}
              setHasMore={setHasMore}
              search={search}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default Roles;
