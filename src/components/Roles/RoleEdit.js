import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Checkbox,
  CircularProgress,
  FormControl,
  FormHelperText,
  Grid,
  OutlinedInput,
  Skeleton,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import {
  ExpandMore as ExpandMoreIcon,
  CheckRounded as CheckRoundedIcon,
} from "@mui/icons-material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import axios from "axios";
import { AuthContext } from "../../contexts/AuthContext";
import { showAccesses, updateAccess, updateRole } from "../../api/roles";
import CustomSwitch from "../../shared/CustomSwitch";
import { getPermissions } from "../../api/permissions";

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: 3,
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #78251E",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ebf1f5",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#78251E",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&::before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage:
      "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
      " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
      "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#78251E",
  },
});

function BpCheckbox(props) {
  return (
    <Checkbox
      sx={{
        "&:hover": { bgcolor: "transparent" },
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      inputProps={{ "aria-label": "Checkbox demo" }}
      {...props}
    />
  );
}

const RoleEdit = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { accessToken } = useContext(AuthContext);

  const [isLoading, setIsLoading] = useState(false);

  const [roleName, setRoleName] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState(false);
  const [accessData, setAccessData] = useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [permissionData, setPermissionData] = useState([]);
  const [checkedAccess, setCheckedAccess] = useState([]);

  const [isRoleNameError, setIsRoleNameError] = useState(false);

  const [isLoadingEdit, setIsLoadingEdit] = useState(false);

  const getRoleDetail = async () => {
    try {
      setIsLoading(true);

      const {
        data: {
          result: { name, description, status },
        },
      } = await axios.get(`/api/v1/roles/${id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      setRoleName(name);
      setDescription(description);
      setStatus(status?.value === 0 ? false : true);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  const getAccessRoleById = async () => {
    try {
      const { results } = await showAccesses(id);

      setAccessData(results || []);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getRoleDetail();
    getAccessRoleById();
  }, []);

  const getPermission = async ({ limit }) => {
    const {
      results: { data },
    } = await getPermissions({ limit });
    setPermissionData(data.map(({ uac, section }) => ({ uac, section })));
  };

  useEffect(() => {
    getPermission({ limit: 100 });
    setCheckedAccess(accessData?.map((item) => item));
  }, [accessData]);

  const accessDataBySection = permissionData.reduce((acc, curr) => {
    if (!acc[curr.section]) {
      acc[curr.section] = [];
    }
    acc[curr.section].push(curr);
    return acc;
  }, {});

  const isAllAccessChecked = () => {
    return permissionData.every((access) =>
      [...checkedAccess].includes(access.uac)
    );
  };

  const handleAccessChange = (access) => {
    const isChecked = checkedAccess.includes(access.uac);
    setCheckedAccess(
      isChecked
        ? checkedAccess.filter((item) => item !== access.uac)
        : [...checkedAccess, access.uac]
    );
    setIsCheckAll(!isAllAccessChecked());
  };

  const handleCheckAllBySection = (section) => {
    const accessesInSection = accessDataBySection[section];
    const allChecked = accessesInSection.every((access) =>
      checkedAccess.includes(access.uac)
    );

    setCheckedAccess(
      allChecked
        ? checkedAccess.filter(
            (item) => !accessesInSection.some((access) => access.uac === item)
          )
        : [
            ...new Set([
              ...checkedAccess,
              ...accessesInSection.map((access) => access.uac),
            ]),
          ]
    );
    setIsCheckAll(!isAllAccessChecked());
  };

  const handleCheckAll = () => {
    setIsCheckAll(!isCheckAll);
    if (
      !permissionData.every((item) => [...checkedAccess].includes(item.uac))
    ) {
      const allAccesses = permissionData.map((access) => access.uac);
      setCheckedAccess([...new Set([...checkedAccess, ...allAccesses])]);
    } else {
      setCheckedAccess([]);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (roleName !== "") {
      setIsLoadingEdit(true);
      try {
        const data = {
          id,
          name: roleName,
          description,
          status: status === true ? 1 : 0,
        };
        const res = await updateRole(id, data);

        if (res.status === 200) {
          const data = {
            permissions: checkedAccess,
          };
          await updateAccess(id, data);
        }
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoadingEdit(false);
        navigate(-1);
      }
    } else {
      roleName !== "" ? setIsRoleNameError(false) : setIsRoleNameError(true);
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Role Edit" />
      </Box>

      <Box sx={{ p: 2 }} component="form" onSubmit={handleSubmit}>
        <BackButton onClick={() => navigate("/roles")} />

        {isLoading && (
          <Skeleton
            sx={{ mt: 2 }}
            variant="rounded"
            height="200px"
            animation="wave"
          />
        )}

        <Box>
          {!isLoading && (
            <Box>
              <Typography sx={{ mt: 2 }}> Detail Role</Typography>
              <Box
                sx={{
                  border: "1px solid #bdbdbd",
                  mt: 0.5,
                  p: 2,
                  borderRadius: "10px",
                }}
              >
                <Box>
                  <Typography>
                    User Role Name <span style={{ color: "#B80000" }}>*</span>
                  </Typography>
                  <FormControl
                    variant="outlined"
                    sx={{
                      mt: 0.5,
                      "& .MuiOutlinedInput-root": {
                        height: "44px",
                        borderRadius: "8px",
                        display: "flex",
                        alignItems: "center",
                      },
                    }}
                    fullWidth
                  >
                    <OutlinedInput
                      placeholder="Enter your role"
                      sx={{ fontSize: "0.875rem" }}
                      value={roleName}
                      error={!roleName && isRoleNameError}
                      onChange={(e) => {
                        setRoleName(e.target.value);
                        if (roleName) {
                          setIsRoleNameError(false);
                        }
                      }}
                    />

                    {isRoleNameError && (
                      <FormHelperText
                        sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                      >
                        Please input role
                      </FormHelperText>
                    )}
                  </FormControl>
                </Box>

                <Box sx={{ mt: 2 }}>
                  <Typography>Description</Typography>
                  <Box
                    sx={{
                      mt: 0.5,
                      borderRadius: "10px",
                      "& .MuiTextField-root": { width: "100%" },
                      "& .MuiOutlinedInput-root": {
                        borderColor: "#bdbdbd",
                        borderRadius: "10px",
                        fontSize: "0.875rem",
                      },
                    }}
                    noValidate
                    autoComplete="off"
                    placeholder="Input your description"
                  >
                    <TextField
                      multiline
                      rows={4}
                      placeholder="Input your description"
                      value={description || ""}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                  </Box>
                </Box>

                <Box
                  sx={{
                    mt: 2,
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: 2,
                  }}
                >
                  <Typography>Status</Typography>
                  <CustomSwitch
                    label={
                      status === true ? (
                        <Typography sx={{ fontSize: "14px", ml: 1 }}>
                          Active
                        </Typography>
                      ) : (
                        <Typography sx={{ fontSize: "14px", ml: 1 }}>
                          Non Active
                        </Typography>
                      )
                    }
                    value={status}
                    setValue={setStatus}
                  />
                </Box>
              </Box>
            </Box>
          )}

          {isLoading && (
            <Skeleton
              sx={{ mt: 2 }}
              variant="rounded"
              height="200px"
              animation="wave"
            />
          )}

          {!isLoading && (
            <Box sx={{ mt: 2 }}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: 0.5,
                  justifyContent: "space-between",
                }}
              >
                <Typography>Detail Permission</Typography>
                <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                  <Typography>All</Typography>
                  <BpCheckbox
                    checked={permissionData.every((item) =>
                      [...checkedAccess].includes(item.uac)
                    )}
                    onChange={handleCheckAll}
                  />
                </Box>
              </Box>

              <Box sx={{ mt: 2 }}>
                {Object.entries(accessDataBySection)?.map(
                  ([section, accesses]) => {
                    return (
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          gap: 1,
                          mt: 1,
                        }}
                        key={section}
                      >
                        <Accordion sx={{ width: "100%" }}>
                          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Box
                              sx={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                gap: 0.5,
                              }}
                            >
                              <BpCheckbox
                                checked={accesses?.every((access) =>
                                  checkedAccess.includes(access.uac)
                                )}
                                onChange={() =>
                                  handleCheckAllBySection(section)
                                }
                                onClick={(e) => e.stopPropagation()}
                              />
                              <Typography>{section}</Typography>
                              {accesses.some((access) =>
                                [...checkedAccess].includes(access.uac)
                              ) && (
                                <CheckRoundedIcon
                                  sx={{ fontSize: "0.75rem", color: "#78251E" }}
                                />
                              )}
                            </Box>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Grid
                              container
                              display="flex"
                              flexDirection="column"
                              rowSpacing={1}
                              gap={1}
                            >
                              {accesses?.map((access, index) => {
                                return (
                                  <Grid
                                    gap={0.5}
                                    display="flex"
                                    alignItems="center"
                                    key={index}
                                    item
                                    xs="auto"
                                  >
                                    <BpCheckbox
                                      checked={checkedAccess.includes(
                                        access.uac
                                      )}
                                      onChange={() =>
                                        handleAccessChange(access)
                                      }
                                      value={access.uac}
                                    />

                                    <Typography sx={{ fontSize: "0.875rem" }}>
                                      {access?.uac?.split(".")[1]}
                                    </Typography>
                                  </Grid>
                                );
                              })}
                            </Grid>
                          </AccordionDetails>
                        </Accordion>
                      </Box>
                    );
                  }
                )}
              </Box>
            </Box>
          )}

          {isLoading && (
            <Skeleton
              sx={{ mt: 2 }}
              variant="rounded"
              height="50px"
              animation="wave"
            />
          )}

          {!isLoading && (
            <Box sx={{ mt: 2 }}>
              <Button
                disabled={isLoadingEdit ? true : false}
                sx={{
                  mt: 3,
                  textTransform: "capitalize",
                  color: "#ffffff",
                  fontSize: "18px",
                  bgcolor: !isLoadingEdit ? "#78251E" : "#bdbdbd",
                  height: "45px",
                  borderRadius: "8px",
                  display: "flex",
                  gap: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  ":hover": {
                    bgcolor: !isLoadingEdit ? "#78251E" : "#bdbdbd",
                    color: "#ffffff",
                  },
                }}
                type="submit"
                fullWidth
              >
                {isLoadingEdit ? (
                  <CircularProgress
                    sx={{ color: "grey" }}
                    thickness={4}
                    size={20}
                    disableShrink
                  />
                ) : (
                  ""
                )}
                Save
              </Button>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default RoleEdit;
