import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  OutlinedInput,
  Switch,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import CheckIcon from "../../assets/check-icon.png";
import CheckIconInactive from "../../assets/check-icon-inactive.png";
import { addNewRole } from "../../api/roles";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const AddNewRole = () => {
  const navigate = useNavigate();

  const [roleName, setRoleName] = useState("");
  const [isRoleNameEmpty, setIsRoleNameEmpty] = useState(false);

  const [status, setStatus] = useState(false);

  const [description, setDescription] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (roleName !== "") {
      setIsLoading(true);

      const data = {
        name: roleName,
        description,
        status: status === false ? 0 : 1,
      };

      await addNewRole(data)
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          setIsLoading(false);
          navigate("/roles");
        });
    } else {
      roleName !== "" ? setIsRoleNameEmpty(false) : setIsRoleNameEmpty(true);
    }
  };

  return (
    <Box>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Add New Role" />
      </Box>

      <Box sx={{ p: 2 }}>
        <BackButton onClick={() => navigate("/roles")} />

        <form onSubmit={handleSubmit}>
          <Box
            sx={{
              border: "1px solid #bdbdbd",
              mt: 2,
              p: 2,
              borderRadius: "10px",
            }}
          >
            <Box>
              <Typography>
                Role Name <span style={{ color: "#B80000" }}>*</span>
              </Typography>
              <FormControl
                variant="outlined"
                sx={{
                  mt: 0.5,
                  "& .MuiOutlinedInput-root": {
                    height: "44px",
                    borderRadius: "8px",
                    display: "flex",
                    alignItems: "center",
                  },
                }}
                value={roleName}
                error={!roleName && isRoleNameEmpty}
                onChange={(e) => {
                  setRoleName(e.target.value);
                  if (roleName) {
                    setIsRoleNameEmpty(false);
                  }
                }}
                fullWidth
              >
                <OutlinedInput
                  placeholder="Enter your role"
                  sx={{ fontSize: "0.875rem" }}
                />

                {isRoleNameEmpty && (
                  <FormHelperText
                    sx={{ ml: 0, fontSize: "11px", color: "#d50000" }}
                  >
                    Please input role
                  </FormHelperText>
                )}
              </FormControl>
            </Box>

            <Box sx={{ mt: 2 }}>
              <Typography>Description</Typography>
              <Box
                sx={{
                  mt: 0.5,
                  borderRadius: "10px",
                  "& .MuiTextField-root": { width: "100%" },
                  "& .MuiOutlinedInput-root": {
                    borderColor: "#bdbdbd",
                    borderRadius: "10px",
                    fontSize: "0.875rem",
                  },
                }}
                noValidate
                autoComplete="off"
                placeholder="Input your description"
              >
                <TextField
                  multiline
                  rows={4}
                  placeholder="Input your description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Box>
            </Box>

            <Box
              sx={{
                mt: 2,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography>Status</Typography>
              <FormGroup
                sx={{
                  "& .MuiFormControlLabel-root": {
                    marginRight: 0,
                  },
                }}
              >
                <FormControlLabel
                  labelPlacement="end"
                  label={
                    status === true ? (
                      <Typography sx={{ fontSize: "14px", ml: 1 }}>
                        Active
                      </Typography>
                    ) : (
                      <Typography sx={{ fontSize: "14px", ml: 1 }}>
                        Non Active
                      </Typography>
                    )
                  }
                  control={
                    <CustomSwitch
                      checked={status}
                      onChange={() => setStatus(!status)}
                    />
                  }
                />
              </FormGroup>
            </Box>
          </Box>

          <Box sx={{ mt: 2 }}>
            <Button
              disabled={isLoading ? true : false}
              sx={{
                mt: 3,
                textTransform: "capitalize",
                color: "#ffffff",
                fontSize: "18px",
                bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
                height: "45px",
                borderRadius: "8px",
                display: "flex",
                gap: 1,
                justifyContent: "center",
                alignItems: "center",
                ":hover": {
                  bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
                  color: "#ffffff",
                },
              }}
              type="submit"
              fullWidth
            >
              {isLoading ? (
                <CircularProgress
                  sx={{ color: "grey" }}
                  thickness={4}
                  size={20}
                  disableShrink
                />
              ) : (
                ""
              )}
              Save
            </Button>
          </Box>
        </form>
      </Box>
    </Box>
  );
};

export default AddNewRole;
