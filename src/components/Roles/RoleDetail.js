import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Grid,
  Skeleton,
  Typography,
} from "@mui/material";
import {
  CircleRounded as CircleRoundedIcon,
  ExpandMore as ExpandMoreIcon,
  CheckRounded as CheckRoundedIcon,
} from "@mui/icons-material";
import Header from "../../shared/Header";
import BackButton from "../../shared/BackButton";
import { showAccesses, showRole, showUsers } from "../../api/roles";
import dayjs from "dayjs";
import { getPermissions } from "../../api/permissions";
import DeleteModalVerification from "./DeleteModalVerification";

const RoleDetail = ({ permissions }) => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [roleData, setRoleData] = useState(null);
  const [accessData, setAccessData] = useState([]);
  const [userData, setUserData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [isPermissionLoading, setIsPermissionLoading] = useState(false);

  const showRoleById = async () => {
    try {
      setIsLoading(true);
      setIsPermissionLoading(true);

      const {
        result: {
          name,
          createdAt: { date },
          updatedAt: { date: updateDate },
          status,
          description,
        },
      } = await showRole(id);

      setRoleData({ name, date, updateDate, status, description });
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
      setIsPermissionLoading(false);
    }
  };

  const getAccessRoleById = async () => {
    setIsPermissionLoading(true);
    try {
      const { results } = await showAccesses(id);

      setAccessData(results?.map((item) => item.split(".")) || []);
    } catch (err) {
      console.error(err);
    } finally {
      setIsPermissionLoading(false);
    }
  };

  const getUsersById = async () => {
    try {
      const res = await showUsers(id);
      setUserData(res);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getAccessRoleById();
    getUsersById();
    showRoleById();
  }, []);

  const [permissionData, setPermissionData] = useState([]);

  const getPermission = async ({ limit }) => {
    setIsPermissionLoading(true);
    try {
      const {
        results: { data },
      } = await getPermissions({ limit });
      setPermissionData(data.map(({ uac, section }) => ({ uac, section })));
    } catch (err) {
      console.error(err);
    } finally {
      setIsPermissionLoading(false);
    }
  };

  useEffect(() => {
    getPermission({ limit: 100 });
  }, []);

  const tempAccessData = accessData?.map((item) => item);
  const joinValueAccessData = tempAccessData?.map((item) => item.join("."));

  const matchedObjectsAccessData = [];

  joinValueAccessData?.forEach((value) => {
    const matchedObject = permissionData?.find((obj) => obj.uac === value);
    if (matchedObject) {
      matchedObjectsAccessData.push(matchedObject);
    }
  });

  const getSectionValue = matchedObjectsAccessData.map((item) => item.section);
  const uniqueSectionValue = [...new Set(getSectionValue)];

  return (
    <Box sx={{ mb: 2 }}>
      <Box sx={{ position: "sticky", top: 0, bgcolor: "#ffffff", zIndex: 10 }}>
        <Header title="Role Detail" isNotificationsNeed={true} />

        <Box sx={{ px: 2, pt: 2 }}>
          <BackButton onClick={() => navigate("/roles")} />
        </Box>
      </Box>

      <Box sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}>
        {isLoading && (
          <Skeleton variant="rounded" height="200px" animation="wave" />
        )}

        {!isLoading && (
          <Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography
                sx={{ color: "#78251E", fontSize: "1rem", fontWeight: 600 }}
              >
                Detail Role
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: 0.5,
                border: "1px solid #E7E7E7",
                borderRadius: "10px",
                p: 2,
                mt: 1,
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 1,
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    User Role Name
                  </Typography>
                  <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
                    {roleData?.name}
                  </Typography>
                </Box>

                <Box>
                  <Typography sx={{ fontSize: "0.875rem", textAlign: "right" }}>
                    Created At
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: "0.875rem",
                      fontWeight: 600,
                      textAlign: "right",
                    }}
                  >
                    {dayjs(roleData?.date).format("YYYY-MM-DD HH:mm")}
                  </Typography>
                </Box>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: 1,
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    Last Update
                  </Typography>
                  <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
                    {dayjs(roleData?.updateDate).format("YYYY-MM-DD HH:mm")}
                  </Typography>
                </Box>

                <Box>
                  <Typography sx={{ fontSize: "0.875rem" }}>Status</Typography>
                  <Typography
                    sx={{
                      fontSize: "0.875rem",
                      fontWeight: 600,
                      color: `#${roleData?.status?.color}`,
                    }}
                  >
                    {roleData?.status?.label}
                  </Typography>
                </Box>
              </Box>

              <Box>
                <Typography sx={{ fontSize: "0.875rem" }}>
                  Description{" "}
                </Typography>
                <Typography sx={{ fontSize: "0.875rem" }}>
                  {roleData?.description || `There's no description`}
                </Typography>
              </Box>
            </Box>
          </Box>
        )}

        {isLoading && (
          <Skeleton variant="rounded" height="200px" animation="wave" />
        )}
        {!isLoading && (
          <Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography
                sx={{ color: "#78251E", fontSize: "1rem", fontWeight: 600 }}
              >
                Detail Permission
              </Typography>
            </Box>

            {!permissions.includes("access.show") && (
              <Typography>
                You're not permitted to see this permission
              </Typography>
            )}

            {accessData?.length === 0 &&
              permissions.includes("access.show") && (
                <Typography sx={{ mt: 0.5, fontSize: "0.875rem" }}>
                  There's no permission in this role
                </Typography>
              )}

            {accessData?.length > 0 &&
              permissions.includes("access.show") &&
              uniqueSectionValue?.map((permission, index) => {
                return (
                  <Accordion key={index}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                      <Typography sx={{ fontSize: "0.875rem" }}>
                        {" "}
                        {permission}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Grid
                        container
                        display="flex"
                        alignItems="center"
                        justifyContent={
                          matchedObjectsAccessData?.filter(
                            (item) => item.section === permission
                          )?.length > 3 && "space-between"
                        }
                        rowSpacing={1}
                        gap={1}
                      >
                        {matchedObjectsAccessData
                          ?.filter((item) => item.section === permission)
                          ?.map((item, index) => (
                            <Grid
                              gap={0.5}
                              display="flex"
                              alignItems="center"
                              key={index}
                              item
                              xs="auto"
                            >
                              <CheckRoundedIcon
                                sx={{ fontSize: "0.75rem", color: "#2196F3" }}
                              />
                              <Typography sx={{ fontSize: "0.75rem" }}>
                                {item?.uac?.split(".")[1]}
                              </Typography>
                            </Grid>
                          ))}
                      </Grid>
                    </AccordionDetails>
                  </Accordion>
                );
              })}
          </Box>
        )}

        {isLoading && (
          <Skeleton variant="rounded" height="80px" animation="wave" />
        )}

        {!isLoading && (
          <Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: 2,
              }}
            >
              <Typography
                sx={{ color: "#78251E", fontSize: "1rem", fontWeight: 600 }}
              >
                Detail User
              </Typography>
            </Box>

            {userData?.length === 0 && (
              <Typography sx={{ fontSize: "0.875rem", mt: 0.5 }}>
                There's no user in this role
              </Typography>
            )}

            {userData?.results?.length > 0 && (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: 0.5,
                  border: "1px solid #E7E7E7",
                  borderRadius: "10px",
                  p: 2,
                  mt: 1,
                }}
              >
                {userData?.results?.map((item, index) => (
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: 0.5,
                    }}
                    key={index}
                  >
                    <CircleRoundedIcon
                      sx={{
                        fontSize: "10px",
                        color: `#${item?.status?.color}`,
                      }}
                    />
                    <Typography sx={{ fontSize: "0.75rem" }}>
                      {item?.name} &#40;{item?.emailAddress}&#41;
                    </Typography>
                  </Box>
                ))}
              </Box>
            )}
          </Box>
        )}

        {isLoading && (
          <Skeleton variant="rounded" height="50px" animation="wave" />
        )}

        {!isLoading && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: 2,
            }}
          >
            <Button
              sx={{
                border: "1px solid #78251E",
                px: 2,
                width: "100%",
                borderRadius: "8px",
                color: "#000000",
                textTransform: "capitalize",
              }}
              onClick={() => navigate(`/role/edit/${id}`)}
            >
              Edit
            </Button>
            <Button
              sx={{
                width: "100%",
                bgcolor: "#78251E",
                border: "1px solid #78251E",
                borderRadius: "8px",
                ":hover": {
                  bgcolor: "#78251E",
                  border: "1px solid #78251E",
                },
                color: "#fff",
                textTransform: "capitalize",
              }}
              onClick={() => setOpenDialog(true)}
            >
              Delete
            </Button>
          </Box>
        )}
      </Box>

      <DeleteModalVerification
        open={openDialog}
        setOpen={setOpenDialog}
        permissions={permissions}
        id={id}
      />
    </Box>
  );
};

export default RoleDetail;
