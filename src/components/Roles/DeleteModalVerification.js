import React from "react";
import { useNavigate } from "react-router-dom";
import CustomDialog from "../../shared/CustomDialog";
import {
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { deleteRole } from "../../api/roles";

const DeleteModalVerification = ({ open, setOpen, permissions, id }) => {
  const navigate = useNavigate();
  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteRole = async () => {
    await deleteRole(id)
      .then(() => {
        setOpen(false);
        navigate("/roles");
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <CustomDialog open={open} handleClose={handleClose}>
      {permissions.includes("role.delete") ? (
        <>
          <DialogTitle id="alert-dialog-title" sx={{ fontWeight: 600 }}>
            Delete Department
          </DialogTitle>
          <DialogContent>
            <DialogContentText
              id="alert-dialog-description"
              sx={{ color: "#000000" }}
            >
              Are you sure want to delete this role?
            </DialogContentText>
          </DialogContent>
          <DialogActions sx={{ px: 2 }}>
            <Button
              sx={{
                border: "1px solid #78251E",
                textTransform: "capitalize",
                color: "#000000",
                px: 2,
                borderRadius: "8px",
                mr: 1,
              }}
              size="small"
              onClick={() => setOpen(false)}
            >
              Cancel
            </Button>
            <Button
              sx={{
                backgroundColor: "#78251E",
                border: "1px solid #78251E",
                color: "#fff",
                textTransform: "capitalize",
                px: 2,
                borderRadius: "8px",
              }}
              onClick={() => handleDeleteRole()}
              autoFocus
              size="small"
            >
              Delete
            </Button>
          </DialogActions>
        </>
      ) : (
        <>
          {" "}
          <DialogContent>
            <DialogContentText
              id="alert-dialog-description"
              sx={{ color: "#000000" }}
            >
              You are not permitted to access this action
            </DialogContentText>
          </DialogContent>
          <DialogActions sx={{ px: 2 }}>
            <Button
              sx={{
                backgroundColor: "#78251E",
                border: "1px solid #78251E",
                color: "#fff",
                textTransform: "capitalize",
                px: 2,
                borderRadius: "8px",
              }}
              onClick={() => setOpen(false)}
              autoFocus
              size="small"
            >
              Close
            </Button>
          </DialogActions>
        </>
      )}
    </CustomDialog>
  );
};

export default DeleteModalVerification;
