import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Skeleton, Typography } from "@mui/material";
import PackageIcon from "../../assets/package-icon.png";
import dayjs from "dayjs";
import InfiniteScroll from "react-infinite-scroll-component";
import { viewNotifications } from "../../api/notif";

const NotifCard = ({
  notificationData,
  handleReadNotifications,
  hasMore,
  setNotificationData,
  setHasMore,
}) => {
  const navigate = useNavigate();

  const [page, setPage] = useState(2);

  const fetchMoreNotif = async ({ page }) => {
    const {
      results: {
        data: newNotif,
        pages: { totalPages, currentPage },
      },
    } = await viewNotifications({ page });

    setNotificationData([...notificationData, ...newNotif]);
    setPage(1);

    if (totalPages - currentPage) {
      if (currentPage + 1) {
        setHasMore(true);
        setPage(page + 1);
      }
    } else {
      setHasMore(false);
    }
  };

  return (
    <Box>
      <InfiniteScroll
        dataLength={notificationData?.length || 0}
        next={() => fetchMoreNotif({ page })}
        hasMore={hasMore}
        loader={
          notificationData?.length > 10 && (
            <Skeleton variant="rounded" sx={{ mt: 1 }} height={60} />
          )
        }
        style={{ overflow: "visible" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
          {notificationData?.map((notif, index) => (
            <Box
              sx={{
                display: "flex",
                gap: 2,
                alignItems: "flex-start",
                // border: "1px solid #bdbdbd",
                p: 1,
                borderRadius: "8px",
                boxShadow: 3,
              }}
              key={index}
              onClick={() => {
                if (notif?.readAt !== null) {
                  navigate(`/package/detail/${notif?.package?.id}`);
                } else {
                  handleReadNotifications([notif?.notificationId]);
                  navigate(`/package/detail/${notif?.package?.id}`);
                }
              }}
            >
              <Box component="img" src={PackageIcon} sx={{ mt: 1 }} />
              <Box>
                <Typography
                  sx={{
                    fontSize: "0.75rem",

                    color: notif?.readAt === null ? "#000" : "#bdbdbd",
                    fontWeight: notif?.readAt === null && 600,
                  }}
                >
                  {`${dayjs(notif?.updatedAt?.date).format(
                    "DD MMM YYYY"
                  )} ${dayjs(notif?.updatedAt?.date).format("HH:mm")}`}
                </Typography>
                <Typography
                  sx={{
                    fontSize: "0.75rem",
                    mt: 0.5,
                    color: notif?.readAt === null ? "#000" : "#bdbdbd",
                    fontWeight: notif?.readAt === null && 600,
                  }}
                >
                  {notif?.description}
                </Typography>
              </Box>
            </Box>
          ))}
        </Box>
      </InfiniteScroll>
    </Box>
  );
};

export default NotifCard;
