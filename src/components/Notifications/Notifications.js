import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Skeleton, Typography } from "@mui/material";
import { ArrowBackIosNewRounded as ArrowBackIosNewRoundedIcon } from "@mui/icons-material";

import { readNotif, viewNotifications } from "../../api/notif";
import { AuthContext } from "../../contexts/AuthContext";
import NotifCard from "./NotifCard";

const Notifications = () => {
  const navigate = useNavigate();
  const { getCountNotifications } = useContext(AuthContext);
  const [notificationData, setNotificationData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [hasMore, setHasMore] = useState(false);

  const getNotificationLists = async () => {
    setIsLoading(true);
    try {
      const res = await viewNotifications({});
      setNotificationData(res?.results?.data);

      if (res?.results?.pages?.totalPages - res?.results?.pages?.currentPage) {
        if (res?.results?.pages?.currentPage) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getNotificationLists({});
  }, []);

  const handleReadNotifications = async (id) => {
    setIsLoading(true);
    await readNotif({ ids: id })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleReadAllNotifications = async (id) => {
    setIsLoading(true);
    await readNotif({ ids: id })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoading(false);
        getNotificationLists();
        getCountNotifications();
      });
  };

  const tempNotifData = [...notificationData];
  const filterReadAllData = tempNotifData
    .filter((item) => item.readAt === null)
    .map((item) => item.notificationId);

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
          p: 2,
          color: "#fff",
          bgcolor: "#78251E",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 0.5,

            color: "#fff",
          }}
        >
          <ArrowBackIosNewRoundedIcon onClick={() => navigate(-1)} />
          <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>
            Notifications
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
          px: 2,
          pt: 2,
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
        }}
      >
        <Box
          sx={{ width: "70px" }}
          onClick={() => {
            handleReadAllNotifications(filterReadAllData);
          }}
        >
          <Typography sx={{ color: "#78251E", fontWeight: 600 }}>
            Read All
          </Typography>
        </Box>
      </Box>
      <Box
        sx={{
          p: 2,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {isLoading &&
          [1, 2, 3].map((_, index) => (
            <Skeleton
              sx={{ height: "100px", borderRadius: "10px" }}
              key={index}
            />
          ))}

        {!isLoading && (
          <NotifCard
            handleReadNotifications={handleReadNotifications}
            hasMore={hasMore}
            notificationData={notificationData}
            setHasMore={setHasMore}
            setNotificationData={setNotificationData}
          />
        )}
      </Box>
    </Box>
  );
};

export default Notifications;
