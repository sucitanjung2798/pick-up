import { createContext, useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { postLogout } from "../api/auth";
import { priorityLevel, statusEnum } from "../api/enum";
import { showProfile } from "../api/profile";
import { countNotifications } from "../api/notif";
import { showHome } from "../api/config";

export const AuthContext = createContext({});

export const AuthContextProvider = ({ children }) => {
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("accessToken")
  );

  const [permissions, setPermissions] = useState(
    localStorage.getItem("permissions") || []
  );

  const [saveRecoveryPassword, setSaveRecoveryPassword] = useState("");

  const [enumStatus, setEnumStatus] = useState([]);
  const [enumPriority, setEnumPriority] = useState([]);
  const [profileData, setProfileData] = useState(null);
  const [countNotif, setCountNotif] = useState(null);

  const [homeData, setHomeData] = useState(null);

  const navigate = useNavigate();
  const componentRef = useRef();

  useEffect(() => {
    if (accessToken) {
      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("permissions", permissions);
    } else {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("permissions");
    }
  }, [accessToken, permissions]);

  const handleLogout = async () => {
    try {
      await postLogout;
    } catch (err) {
      console.error(err);
    } finally {
      localStorage.removeItem("accessToken");
      setAccessToken("");
      navigate("/");
    }
  };

  const getStatusEnum = async () => {
    try {
      const { results } = await statusEnum();
      setEnumStatus(results);
    } catch (err) {
      console.error(err);
    }
  };

  const getPriorityEnum = async () => {
    try {
      const { results } = await priorityLevel();
      setEnumPriority(results);
    } catch (err) {
      console.error(err);
    }
  };

  const [isProfileLoading, setIsProfileLoading] = useState(false);

  const getShowProfile = async () => {
    try {
      setIsProfileLoading(true);
      const { result } = await showProfile();
      setProfileData(result);
    } catch (err) {
      console.error(err);
    } finally {
      setIsProfileLoading(false);
    }
  };

  const getCountNotifications = async () => {
    try {
      const { result } = await countNotifications();
      setCountNotif(result);
    } catch (err) {
      console.error(err);
    }
  };

  const getHomeData = async () => {
    try {
      const { results } = await showHome();
      setHomeData(results);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getStatusEnum();
    getPriorityEnum();
    getShowProfile();
    getCountNotifications();
    getHomeData();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        accessToken,
        setAccessToken,
        handleLogout,
        setPermissions,
        permissions,
        setSaveRecoveryPassword,
        saveRecoveryPassword,
        enumStatus,
        enumPriority,
        profileData,
        isProfileLoading,
        countNotif,
        getCountNotifications,
        componentRef,
        homeData,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
