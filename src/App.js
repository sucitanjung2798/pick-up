import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import { Container, ThemeProvider, createTheme } from "@mui/material";

import RoutePath from "./routes/RoutePath";
import { AuthContextProvider } from "./contexts/AuthContext";

function App() {
  const theme = createTheme({
    typography: {
      allVariants: {
        fontFamily: "Poppins",
      },
    },
  });

  return (
    <Router>
      <AuthContextProvider>
        <ThemeProvider theme={theme}>
          <Container maxWidth="xs" disableGutters>
            <RoutePath />
          </Container>
        </ThemeProvider>
      </AuthContextProvider>
    </Router>
  );
}

export default App;
