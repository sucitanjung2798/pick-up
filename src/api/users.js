import { adminAxios } from "../helpers/api";

export const allUsers = ({ page, search, status }) =>
  adminAxios.get("/users", { params: { page, search, status } });

export const showUser = (id) => adminAxios.get(`/users/${id}`);

export const addNewUser = (data) => adminAxios.post("/users", data);

export const editUser = (id, data) => adminAxios.put(`/users/${id}`, data);

export const deleteUser = (id) => adminAxios.delete(`/users/${id}`);

export const viewUserActive = () => adminAxios.get("/users/active");
