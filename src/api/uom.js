import { adminAxios } from "../helpers/api";

export const getAllUom = ({ page, search, status }) =>
  adminAxios.get("/uoms", { params: { page, search, status } });

export const showUom = (id) => adminAxios.get(`/uoms/${id}`);

export const addNewUom = (data) => adminAxios.post("/uoms", data);

export const editUom = (id, data) => adminAxios.put(`/uoms/${id}`, data);

export const deleteUom = (id) => adminAxios.delete(`/uoms/${id}`);
