import { adminAxios } from "../helpers/api";

/* Default */
export const viewConfigDefault = () => adminAxios.get("/configs/defaults");

export const showConfigDefault = (id) =>
  adminAxios.get(`/configs/defaults/${id}`);

export const addConfigDefault = (data) =>
  adminAxios.post("/configs/defaults", data);

export const deleteConfigDefault = (id) =>
  adminAxios.delete(`/configs/defaults/${id}`);

export const editConfigDefault = (id, data) =>
  adminAxios.put(`/configs/defaults/${id}`, data);

/* FTPs */
export const viewFtps = () => adminAxios.get("/configs/ftps");

export const showFtp = (id) => adminAxios.get(`/configs/ftps/${id}`);

export const addNewFtp = (data) => adminAxios.post("/configs/ftps", data);

export const editFtp = (id, data) =>
  adminAxios.put(`/configs/ftps/${id}`, data);

export const deleteFtp = (id) => adminAxios.delete(`/configs/ftps/${id}`);

/* Applications */
export const viewApps = () => adminAxios.get("/configs/apps");

export const showApp = (id) => adminAxios.get(`/configs/apps/${id}`);
export const deleteApp = (id) => adminAxios.delete(`/configs/apps/${id}`);
export const addNewApp = (data) => adminAxios.post(`/configs/apps`, data);
export const editApp = (id, data) =>
  adminAxios.put(`/configs/apps/${id}`, data);

/* Email */
export const viewEmail = () => adminAxios.get("/configs/emails");
export const showEmail = (id) => adminAxios.get(`/configs/emails/${id}`);
export const deleteEmail = (id) => adminAxios.delete(`/configs/emails/${id}`);
export const addNewEmail = (data) => adminAxios.post(`/configs/emails`, data);

export const showHome = () => adminAxios.get("/home");
