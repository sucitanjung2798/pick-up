import { adminAxios } from "../helpers/api";

export const viewNotifications = ({ page }) =>
  adminAxios.get("/notifications", { params: { page, limit: 10 } });

export const countNotifications = () => adminAxios.get("/notifications/count");

export const readNotif = (data) => adminAxios.patch("/notifications", data);
