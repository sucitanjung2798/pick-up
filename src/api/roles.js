import { adminAxios } from "../helpers/api";

export const getRoles = ({ page = 1, search, status }) =>
  adminAxios.get("roles", { params: { page, search, status } });

export const addNewRole = (data) => adminAxios.post("roles", data);

export const deleteRole = (id) => adminAxios.delete(`roles/${id}`);

export const showAccesses = (id) => adminAxios.get(`/roles/${id}/accesses`);

export const showUsers = (id) => adminAxios.get(`/roles/${id}/users`);

export const showRole = (id) => adminAxios.get(`/roles/${id}`);

export const updateAccess = (id, data) =>
  adminAxios.post(`/accesses/${id}`, data);

export const updateRole = (id, permissions) =>
  adminAxios.put(`/roles/${id}`, permissions);

export const viewRoleActive = () => adminAxios.get("/roles/active");
