import { adminAxios } from "../helpers/api";

export const showProfile = () => adminAxios.get("/users/profile");

export const changePassword = (data) =>
  adminAxios.patch("/users/change-password", data);
