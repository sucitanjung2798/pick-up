import { adminAxios } from "../helpers/api";

export const statusEnum = () => adminAxios.get("/enums/active-statuses");

export const priorityLevel = () => adminAxios.get("/enums/priority-levels");

export const packageStatusList = () =>
  adminAxios.get("/enums/package-statuses");

export const settlementStatus = () =>
  adminAxios.get("/enums/settlement-statuses");
