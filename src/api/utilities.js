import { adminAxios } from "../helpers/api";

export const emailServerConnection = () =>
  adminAxios.get("/utilities/email-server-connection");

export const ftpServerConnection = () =>
  adminAxios.get("/utilities/ftp-server-connection");

export const removeAllSessions = () =>
  adminAxios.delete("/utilities/session-remove");

export const clearAllRemoteAssets = () =>
  adminAxios.delete("/utilities/remote-server-asset-clear");

export const databaseImport = () =>
  adminAxios.get("/utilities/database-import");

export const databaseExport = () =>
  adminAxios.get("/utilities/database-export");

export const clearAllPosts = () =>
  adminAxios.delete("/utilities/database-clear");
