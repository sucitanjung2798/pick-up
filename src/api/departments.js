import { adminAxios } from "../helpers/api";

export const getAllDepartments = ({ page, search, status }) =>
  adminAxios.get("/departments", { params: { page, search, status } });

export const showDepartment = (id) => adminAxios.get(`/departments/${id}`);

export const addNewDepartment = (data) => adminAxios.post(`/departments`, data);

export const editDepartment = (data, id) =>
  adminAxios.put(`/departments/${id}`, data);

export const deleteDepartment = (id) => adminAxios.delete(`/departments/${id}`);

export const viewDepartmentActive = () => adminAxios.get("/departments/active");
