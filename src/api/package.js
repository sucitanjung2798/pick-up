import { adminAxios } from "../helpers/api";

export const createPackage = (data) => adminAxios.post("/packages", data);

export const showPackage = (id) => adminAxios.get(`/packages/${id}`);

export const showPackageAdmin = (id) =>
  adminAxios.get(`/packages/admins/${id}`);

export const viewPackages = ({
  status,
  page = 1,
  search,
  etaBeginDate,
  etaEndDate,
  priority,
  createdBeginDate,
  createdEndDate,
}) =>
  adminAxios.get("/packages", {
    params: {
      status,
      page,
      limit: 10,
      search,
      etaBeginDate,
      etaEndDate,
      priority,
      createdBeginDate,
      createdEndDate,
    },
  });

export const viewPackagesAdmin = ({
  status,
  page = 1,
  search,
  etaBeginDate,
  etaEndDate,
  priority,
  createdBeginDate,
  createdEndDate,
}) =>
  adminAxios.get("/packages/admins", {
    params: {
      status,
      page,
      limit: 10,
      search,
      etaBeginDate,
      etaEndDate,
      priority,
      createdBeginDate,
      createdEndDate,
    },
  });

export const deletePackage = (id) =>
  adminAxios.delete(`/packages/admins/${id}`);

export const submitPackage = (data) =>
  adminAxios.post("/packages/submit", data);

export const cancelPackage = (data) =>
  adminAxios.post("/packages/cancel", data);

export const editPackage = (id, data) =>
  adminAxios.put(`/packages/${id}`, data);

export const historyPackage = ({ search, page = 1, activity }) =>
  adminAxios.get("/packages/histories", {
    params: { search, page, limit: 10, activity },
  });

export const trackingPackage = (data) =>
  adminAxios.post("/packages/tracking", data);

/* detail */

export const createPackageDetail = (id, data) =>
  adminAxios.post(`/packages/${id}/details`, data);

export const viewPackageDetail = (id) =>
  adminAxios.get(`/packages/${id}/details`);

export const viewPackageDetailAdmin = (id) =>
  adminAxios.get(`/packages/admins/${id}/details`);

export const showPackageDetail = (packageId, packageDetailId) =>
  adminAxios.get(`/packages/${packageId}/details/${packageDetailId}`);

export const showPackageDetailAdmin = (packageId, packageDetailId) =>
  adminAxios.get(`/packages/admins/${packageId}/details/${packageDetailId}`);

export const editPackageDetail = (packageId, packageDetailId, data) =>
  adminAxios.put(`/packages/${packageId}/details/${packageDetailId}`, data);

/* courier */

export const viewPackageCourier = ({ search, page }) =>
  adminAxios.get("/packages/couriers", { params: { search, page } });

export const showPackageCourier = (id) =>
  adminAxios.get(`/packages/couriers/${id}`);

export const collectedPackage = (data) =>
  adminAxios.post("/consignments", data);

export const showPackageDetailCourier = (packageId) =>
  adminAxios.get(`/packages/couriers/${packageId}/details`);

export const settlementPackage = (data) =>
  adminAxios.post("/settlements", data);

export const confirmPackage = (id, data) =>
  adminAxios.patch(`/settlements/${id}`, data);

export const showConsignment = (id) => adminAxios.get(`/consignments/${id}`);

export const showSettlement = (id) => adminAxios.get(`/settlements/${id}`);
