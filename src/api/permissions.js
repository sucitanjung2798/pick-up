import { adminAxios } from "../helpers/api";

export const getPermissions = ({ page = 1, search, limit = 10 }) =>
  adminAxios.get(`permissions`, {
    params: { page, limit, search },
  });

export const getDetailPermission = (id) => adminAxios.get(`permissions/${id}`);

export const editPermission = ({ data, id }) =>
  adminAxios.put(`permissions/${id}`, data);

export const addNewPermission = (data) => adminAxios.post(`permissions`, data);

export const deletePermission = (id) => adminAxios.delete(`permissions/${id}`);
