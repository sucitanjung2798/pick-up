import { adminAxios, authAxios } from "../helpers/api";

export const postLogin = (data) => authAxios.post("login", data);

export const postLogout = () => adminAxios.get("/logout");

export const sendEmailForgotPassword = (data) =>
  adminAxios.post("/forgot-password", data);

export const sendOtp = (data) =>
  adminAxios.patch("/users/forgot-password", data);

export const resetPassword = (data) =>
  adminAxios.patch("/users/reset-password", data);
