import React from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import BottomMenu from "../shared/BottomMenu";
import { Box } from "@mui/material";

const ProtectedRoutes = () => {
  const { accessToken } = useAuth();
  const location = useLocation();

  return accessToken ? (
    <>
      <Box sx={{ mb: 10 }}>
        <Outlet />
      </Box>

      <BottomMenu isBottomMenuAppear={true} />
    </>
  ) : (
    <Navigate to="/" state={{ from: location }} replace />
  );
};

export default ProtectedRoutes;
