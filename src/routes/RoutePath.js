import { Route, Routes, Navigate } from "react-router-dom";
import Login from "../components/Login";
import Dashboard from "../components/Dashboard";
import Roles from "../components/Roles/Roles";
import Permissions from "../components/Permissions/Permissions";
import Package from "../components/Package/Package";
import PackageDetail from "../components/Package/PackageDetail";
import Departments from "../components/Departments/Departments";
import DepartmentDetail from "../components/Departments/DepartmentDetail";
import DepartmentEdit from "../components/Departments/DepartmentEdit";
import ManageUserList from "../components/ManageUser/ManageUserList";
import ManageUserEdit from "../components/ManageUser/ManageUserEdit";
import ManageUserDetail from "../components/ManageUser/ManageUserDetail";
import Notifications from "../components/Notifications/Notifications";
import Settings from "../components/Settings/Settings";
import RecoveryPassword from "../components/ForgetPassword/RecoveryPassword";
import VerificationCode from "../components/ForgetPassword/VerificationCode";
import ChangePassword from "../components/ForgetPassword/ChangePassword";
import { useAuth } from "../hooks/useAuth";
import ProtectedRoutes from "./ProtectedRoutes";
import NotFound from "../components/NotFound";
import RoleEdit from "../components/Roles/RoleEdit";
import PermissionEdit from "../components/Permissions/PermissionEdit";
import { useMediaQuery } from "@mui/material";
import AddNewRole from "../components/Roles/AddNewRole";
import RoleDetail from "../components/Roles/RoleDetail";
import AddNewPermission from "../components/Permissions/AddNewPermission";
import AddNewDepartment from "../components/Departments/AddNewDepartment";
import AddNewUser from "../components/ManageUser/AddNewUser";
import Uom from "../components/UOM/Uom";
import AddNewUom from "../components/UOM/AddNewUom";
import EditUom from "../components/UOM/EditUom";
import AddNewPackage from "../components/Package/AddNewPackage";
import PackageEdit from "../components/Package/PackageEdit";
import UserProfile from "../components/Profile/UserProfile";
import History from "../components/History/History";
import Tracking from "../components/Tracking/Tracking";
import UserProfileDetail from "../components/Profile/UserProfileDetail";
import HelpCenterDetail from "../components/Profile/HelpCenterDetail";
import ChangePasswordProfile from "../components/Profile/ChangePasswordProfile";
import Default from "../components/Settings/Configurations/Default/Default";
import DefaultDetail from "../components/Settings/Configurations/Default/DefaultDetail";
import AddNewConfigDefault from "../components/Settings/Configurations/Default/AddNewConfigDefault";
import DefaultEdit from "../components/Settings/Configurations/Default/DefaultEdit";
import FTPs from "../components/Settings/Configurations/FTPs/FTPs";
import FTPsDetail from "../components/Settings/Configurations/FTPs/FTPsDetail";
import FTPEdit from "../components/Settings/Configurations/FTPs/FTPEdit";
import AddNewFTP from "../components/Settings/Configurations/FTPs/AddNewFTP";
import Applications from "../components/Settings/Configurations/Applications/Applications";
import ApplicationDetail from "../components/Settings/Configurations/Applications/ApplicationDetail";
import AddNewApplication from "../components/Settings/Configurations/Applications/AddNewApplication";
import AppEdit from "../components/Settings/Configurations/Applications/AppEdit";
import Email from "../components/Settings/Configurations/Email/Email";
import EmailDetail from "../components/Settings/Configurations/Email/EmailDetail";
import AddNewEmail from "../components/Settings/Configurations/Email/AddNewEmail";
import EmailEdit from "../components/Settings/Configurations/Email/EmailEdit";
import EmailServerConnection from "../components/Settings/Utilities/EmailServerConnection";
import FtpServerConnection from "../components/Settings/Utilities/FtpServerConnection";
import RemoveAllSessions from "../components/Settings/Utilities/RemoveAllSessions";
import ClearAllRemoteAssets from "../components/Settings/Utilities/ClearAllRemoteAssets";
import DatabaseImport from "../components/Settings/Utilities/DatabaseImport";
import DatabaseExport from "../components/Settings/Utilities/DatabaseExport";
import ClearAllPost from "../components/Settings/Utilities/ClearAllPost";
import PrintPage from "../PrintPage";

const RoutePath = () => {
  const { accessToken, permissions } = useAuth();

  const matchesPhone = useMediaQuery("(min-width:320px)");
  const desktopView = useMediaQuery("(max-width: 1030px)");

  return (
    <Routes>
      {accessToken ? (
        <Route index element={<Dashboard />} />
      ) : (
        <Route>
          <Route index element={<Login />} />
          <Route path="/recovery-password" element={<RecoveryPassword />} />
          <Route path="/verification-code" element={<VerificationCode />} />
          <Route path="/verified" element={<ChangePassword />} />
        </Route>
      )}

      {accessToken && (
        <Route element={<ProtectedRoutes />}>
          {/* Dashboard */}
          <Route
            path="/dashboard"
            element={<Dashboard permissions={permissions} />}
          />

          <Route path="/print-page" element={<PrintPage />} />

          {/* Roles */}

          {permissions.includes("role.view") && (
            <Route path="/roles">
              <Route
                index
                element={
                  <Roles
                    matchesPhone={matchesPhone}
                    permissions={permissions}
                  />
                }
              />
            </Route>
          )}

          {permissions.includes("role.show") && (
            <Route path="/role/detail/:id">
              <Route index element={<RoleDetail permissions={permissions} />} />
            </Route>
          )}

          {permissions.includes("role.create") && (
            <Route path="/add-new-role">
              <Route index element={<AddNewRole />} />
            </Route>
          )}

          {permissions.includes("role.update") && (
            <Route path="/role/edit/:id">
              <Route index element={<RoleEdit />} />
            </Route>
          )}

          {/* Permissions */}

          {permissions.includes("perm.view") && (
            <Route path="/permissions">
              <Route
                index
                element={<Permissions permissions={permissions} />}
              />
            </Route>
          )}

          {permissions.includes("perm.create") && (
            <Route path="/add-new-permission">
              <Route index element={<AddNewPermission />} />
            </Route>
          )}

          {permissions.includes("perm.update") && (
            <Route path="/permission/edit/:id">
              <Route index element={<PermissionEdit />} />
            </Route>
          )}

          {/* Package */}

          {permissions.includes("package.view") && (
            <Route
              path="/package/lists"
              element={<Package permissions={permissions} />}
            />
          )}

          {permissions.includes("package.show") && (
            <Route
              path="/package/detail/:id"
              element={
                <PackageDetail
                  permissions={permissions}
                  desktopView={desktopView}
                />
              }
            />
          )}

          {permissions.includes("package.create") && (
            <Route
              path="/add-new-package"
              element={<AddNewPackage permissions={permissions} />}
            />
          )}

          {permissions.includes("package.update") && (
            <Route path="/package/edit/:id" element={<PackageEdit />} />
          )}

          {/* Departments */}

          {permissions.includes("dept.view") && (
            <Route
              path="/department/lists"
              element={<Departments permissions={permissions} />}
            />
          )}

          {permissions.includes("dept.show") && (
            <Route
              path="/department/detail/:id"
              element={<DepartmentDetail permissions={permissions} />}
            />
          )}

          {permissions.includes("dept.create") && (
            <Route path="/add-new-department" element={<AddNewDepartment />} />
          )}

          {permissions.includes("dept.update") && (
            <Route path="/department/edit/:id" element={<DepartmentEdit />} />
          )}

          {/* Users */}

          {permissions.includes("user.view") && (
            <Route
              path="/manage-user/lists"
              element={<ManageUserList permissions={permissions} />}
            />
          )}

          {permissions.includes("user.create") && (
            <Route path="/add-new-user" element={<AddNewUser />} />
          )}

          {permissions.includes("user.show") && (
            <Route
              path="/manage-user/detail/:id"
              element={<ManageUserDetail />}
            />
          )}

          {permissions.includes("user.update") && (
            <Route path="/manage-user/edit/:id" element={<ManageUserEdit />} />
          )}

          {/* UOM */}

          {permissions.includes("uom.view") && (
            <Route
              path="/uom-lists"
              element={<Uom permissions={permissions} />}
            />
          )}

          {permissions.includes("uom.create") && (
            <Route path="/add-new-uom" element={<AddNewUom />} />
          )}

          {permissions.includes("uom.update") && (
            <Route path="/uom/edit/:id" element={<EditUom />} />
          )}

          {/* Notifications */}
          <Route path="/notifications" element={<Notifications />} />

          {/* Settings */}
          <Route path="/settings">
            <Route index element={<Settings />} />
            {permissions.includes("config.view") && (
              <>
                <Route path="config/default" element={<Default />} />
                <Route path="config/ftps" element={<FTPs />} />
                <Route path="config/apps" element={<Applications />} />
                <Route path="config/email" element={<Email />} />
              </>
            )}
            {permissions.includes("config.show") && (
              <>
                <Route path="config/default/:id" element={<DefaultDetail />} />
                <Route path="config/ftps/:id" element={<FTPsDetail />} />
                <Route path="config/apps/:id" element={<ApplicationDetail />} />
                <Route path="config/email/:id" element={<EmailDetail />} />
              </>
            )}
            {permissions.includes("config.update") && (
              <>
                <Route
                  path="config/default/edit/:id"
                  element={<DefaultEdit />}
                />
                <Route path="config/ftps/edit/:id" element={<FTPEdit />} />
                <Route path="config/app/edit/:id" element={<AppEdit />} />
                <Route path="config/email/edit/:id" element={<EmailEdit />} />
              </>
            )}
            {permissions.includes("config.create") && (
              <>
                <Route
                  path="config/default/add-new-default"
                  element={<AddNewConfigDefault />}
                />
                <Route
                  path="config/default/add-new-ftp"
                  element={<AddNewFTP />}
                />
                <Route
                  path="config/default/add-new-app"
                  element={<AddNewApplication />}
                />

                <Route
                  path="config/default/add-new-email"
                  element={<AddNewEmail />}
                />
              </>
            )}

            {permissions.includes("util.email_connect") && (
              <Route path="email-connect" element={<EmailServerConnection />} />
            )}

            {permissions.includes("util.ftp_connect") && (
              <Route path="ftp-connect" element={<FtpServerConnection />} />
            )}
            {permissions.includes("util.sess_remove") && (
              <Route path="sess-remove" element={<RemoveAllSessions />} />
            )}
            {permissions.includes("util.asset_clear") && (
              <Route path="asset-clear" element={<ClearAllRemoteAssets />} />
            )}
            {permissions.includes("util.db_import") && (
              <Route path="db-import" element={<DatabaseImport />} />
            )}
            {permissions.includes("util.db_export") && (
              <Route path="db-export" element={<DatabaseExport />} />
            )}
            {permissions.includes("util.db_clear") && (
              <Route path="db-clear" element={<ClearAllPost />} />
            )}
          </Route>

          <Route path="/history" element={<History />} />
          <Route path="/tracking" element={<Tracking />} />

          <Route path="/user-profile" element={<UserProfile />} />
          <Route path="/user-profile-detail" element={<UserProfileDetail />} />
          <Route path="/help-center-detail" element={<HelpCenterDetail />} />
          <Route path="/change-password" element={<ChangePasswordProfile />} />
        </Route>
      )}

      {!accessToken && <Route path="*" element={<Navigate to="/" replace />} />}

      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default RoutePath;
