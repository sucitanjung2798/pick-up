import React from "react";
import {
  FormControl,
  MenuItem,
  OutlinedInput,
  Select,
  Typography,
} from "@mui/material";

const CustomSelect = ({
  value,
  isValueError,
  data,
  isHelperText = false,
  helperText,
  placeholder,
  ...props
}) => {
  return (
    <FormControl
      fullWidth
      sx={{
        mt: 0.5,
        "& .MuiOutlinedInput-root": {
          height: "44px",
          // width: "7.188rem",
          borderRadius: "8px",
          fontSize: "0.875rem",
        },
      }}
    >
      <Select
        displayEmpty
        value={value}
        {...props}
        input={<OutlinedInput error={!value && isValueError} />}
      >
        {placeholder ? (
          <MenuItem disabled value="">
            <Typography sx={{ fontSize: "0.875rem", color: "#bdbdbd" }}>
              {placeholder}
            </Typography>
          </MenuItem>
        ) : (
          ""
        )}

        {data}
      </Select>
      {isHelperText && helperText}
    </FormControl>
  );
};

export default CustomSelect;
