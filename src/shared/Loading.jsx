import React from "react";
import { Box, CircularProgress } from "@mui/material";

const Loading = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "500px",
        transform: "scaleX(-1)",
      }}
    >
      <CircularProgress sx={{ color: "#78251E" }} />
    </Box>
  );
};

export default Loading;
