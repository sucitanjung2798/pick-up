import React from "react";
import { Button, CircularProgress } from "@mui/material";

const CustomButtonSave = ({ isLoading, content }) => {
  return (
    <Button
      disabled={isLoading ? true : false}
      sx={{
        mt: 3,
        textTransform: "capitalize",
        color: "#ffffff",
        fontSize: "18px",
        bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
        height: "45px",
        borderRadius: "8px",
        display: "flex",
        gap: 1,
        justifyContent: "center",
        alignItems: "center",
        ":hover": {
          bgcolor: !isLoading ? "#78251E" : "#bdbdbd",
          color: "#ffffff",
        },
      }}
      type="submit"
      fullWidth
    >
      {isLoading ? (
        <CircularProgress
          sx={{ color: "grey" }}
          thickness={4}
          size={20}
          disableShrink
        />
      ) : (
        ""
      )}
      {content}
    </Button>
  );
};

export default CustomButtonSave;
