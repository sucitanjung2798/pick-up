import { FormControl, OutlinedInput } from "@mui/material";
import React from "react";

const CustomInput = ({
  value,
  isValueError,
  isHelperText = true,
  placeholder,
  helperText,
  formControlSx,
  ...props
}) => {
  return (
    <FormControl
      variant="outlined"
      sx={{
        mt: 0.5,
        "& .MuiOutlinedInput-root": {
          height: "44px",
          borderRadius: "8px",
          display: "flex",
          alignItems: "center",
        },
        ...formControlSx,
      }}
      fullWidth
    >
      <OutlinedInput
        placeholder={placeholder}
        value={value || ""}
        error={!value && isValueError}
        {...props}
      />

      {isHelperText && helperText}
    </FormControl>
  );
};

export default CustomInput;
