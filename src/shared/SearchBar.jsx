import React from "react";
import { Box, InputBase } from "@mui/material";
import { Search as SearchIcon } from "@mui/icons-material";

const SearchBar = ({ isQrCodeNeeded = false, search, ...props }) => {
  return (
    <Box
      component="form"
      sx={{
        p: "6px",
        display: "flex",
        alignItems: "center",
        border: "1px solid #bdbdbd",
        borderRadius: "10px",
        width: "100%",
      }}
    >
      <SearchIcon sx={{ px: 0.5, fontSize: "1.563rem" }} />

      <InputBase
        sx={{ ml: 1, flex: 1, fontSize: "0.75rem", fontWeight: 600 }}
        {...props}
      />
      {/* {isQrCodeNeeded && search === "" && (
        <Box
          component="img"
          src={QrCodeIcon}
          sx={{ px: 0.5, width: "100%", maxWidth: "24px" }}
        />
      )} */}
    </Box>
  );
};

export default SearchBar;
