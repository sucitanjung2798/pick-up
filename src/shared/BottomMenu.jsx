import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import {
  BottomNavigation,
  BottomNavigationAction,
  Box,
  Typography,
} from "@mui/material";
import { ReactComponent as HomeMenu } from "../assets/home-menu.svg";
import { ReactComponent as UserMenu } from "../assets/user-menu.svg";
import { ReactComponent as HistoryIcon } from "../assets/history-icon.svg";
import { ReactComponent as TrackingIcon } from "../assets/tracking-icon.svg";
import { ReactComponent as QRCodeIcon } from "../assets/qr-code-menu.svg";

let stopScan = false;
let scanResult = "";

const BottomMenu = ({ isBottomMenuAppear = false }) => {
  const navigate = useNavigate();

  const [value, setValue] = useState(0);
  const location = useLocation();
  const [scanButton, setScanButton] = useState(true);

  useEffect(() => {
    if (location?.pathname === "/dashboard") {
      setValue(0);
    } else if (location.pathname === "/history") {
      setValue(1);
    } else if (location.pathname === "/tracking") {
      setValue(2);
    } else {
      setValue(3);
    }
  }, [location?.pathname]);

  // const scanNow = async (isScan) => {
  //   setScanButton(isScan);
  //   if (isScan) stopScan = true;
  //   if (scanButton === false) return;
  //   stopScan = false;
  //   await new Promise((r) => setTimeout(r, 100));
  //   const videoElement = document.getElementById("scanView");
  //   const scanner = new QrScanner(
  //     videoElement,
  //     (result) => {
  //       scanResult = result.data;
  //       setScanButton(true);
  //       stopScan = true;
  //     },
  //     {
  //       onDecodeError: (error) => {
  //         console.error(error);
  //       },
  //       maxScansPerSecond: 1,
  //       highlightScanRegion: true,
  //       highlightCodeOutline: true,
  //       returnDetailedScanResult: true,
  //     }
  //   );
  //   await scanner.start();
  //   while (stopScan === false) await new Promise((r) => setTimeout(r, 100));
  //   scanner.stop();
  //   scanner.destroy();
  // };

  return isBottomMenuAppear ? (
    <Box
      sx={{
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
        bgcolor: "#fff",
        overflow: "hidden",
        boxShadow: 2,
        height: "35px",
        p: 2,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        zIndex: 10,
        gap: 0.5,
      }}
    >
      <BottomNavigation
        showLabels
        sx={{
          width: "100%",
        }}
      >
        <BottomNavigationAction
          label={
            <Typography
              sx={{
                fontSize: "10px",
                color:
                  value === 0 &&
                  location?.pathname === "/dashboard" &&
                  "#78251E",
              }}
            >
              Dashboard
            </Typography>
          }
          icon={
            value === 0 && location?.pathname === "/dashboard" ? (
              <HomeMenu stroke={`#78251E`} />
            ) : (
              <HomeMenu stroke="#687176" />
            )
          }
          onClick={() => {
            navigate("/dashboard");
            setValue(0);
          }}
        />

        <BottomNavigationAction
          label={
            <Typography
              sx={{
                fontSize: "10px",
                color:
                  value === 1 && location?.pathname === "/history" && "#78251E",
              }}
            >
              History
            </Typography>
          }
          icon={
            value === 1 && location?.pathname === "/history" ? (
              <HistoryIcon fill="#78251E" />
            ) : (
              <HistoryIcon fill="#687176" />
            )
          }
          onClick={() => {
            navigate("/history");
            setValue(1);
          }}
        />

        {/* <BottomNavigationAction
          // label={
          //   <Typography
          //     sx={{
          //       fontSize: "10px",
          //       mt: 0.5,
          //     }}
          //   >
          //     Scan
          //   </Typography>
          // }
          icon={
            <Box
              sx={{
                bgcolor: "#78251E",
                borderRadius: "50px",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                p: 1,
                boxShadow: 4,
              }}
            >
              <QRCodeIcon fill="#fff" />
            </Box>
          }
          // onClick={() => {
          //   scanNow(!scanButton);
          // }}
        /> */}

        <BottomNavigationAction
          label={
            <Typography
              sx={{
                fontSize: "10px",
                color:
                  value === 2 &&
                  location?.pathname === "/tracking" &&
                  "#78251E",
              }}
            >
              Tracking
            </Typography>
          }
          icon={
            value === 2 && location?.pathname === "/tracking" ? (
              <TrackingIcon fill={`#78251E`} />
            ) : (
              <TrackingIcon fill="#687176" />
            )
          }
          onClick={() => {
            navigate("/tracking");
            setValue(2);
          }}
        />

        <BottomNavigationAction
          label={
            <Typography
              sx={{
                fontSize: "10px",
                color:
                  value === 3 &&
                  location?.pathname === "/user-profile" &&
                  "#78251E",
              }}
            >
              Profile
            </Typography>
          }
          icon={
            value === 3 && location?.pathname === "/user-profile" ? (
              <UserMenu stroke={`#78251E`} />
            ) : (
              <UserMenu stroke="#687176" />
            )
          }
          onClick={() => {
            navigate("/user-profile");
            setValue(3);
          }}
        />
      </BottomNavigation>
    </Box>
  ) : (
    ""
  );
};

export default BottomMenu;
