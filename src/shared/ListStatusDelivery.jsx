import React from "react";
import { Box } from "@mui/material";

const ListStatusDelivery = ({
  listStatus,
  isDefault = false,
  defaultText,
  textTransform = "capitalize",
  selectedFile,
  setSelectedFile,
}) => {
  const handleActiveTab = (value) => {
    setSelectedFile(value);
  };

  return (
    <Box
      sx={{
        overflow: "auto",
        scrollbarWidth: "none",
        "&::-webkit-scrollbar": {
          display: "none",
        },
        "&-ms-overflow-style:": {
          display: "none",
        },
      }}
    >
      <Box
        sx={{
          display: "flex",
          gap: 1,
          alignItems: "center",
          justifyContent: listStatus.length > 4 && "space-between",
        }}
      >
        {isDefault === true && (
          <Box
            component="button"
            sx={{
              border: `1px solid ${
                selectedFile === defaultText ? "#78251E" : "#bdbdbd"
              }`,
              textAlign: "center",
              fontSize: "0.688rem",
              bgcolor: selectedFile === defaultText ? "#78251E" : "transparent",
              p: 1,
              borderRadius: "10px",
              color: selectedFile === defaultText ? "#ffffff" : "#bdbdbd",
              fontWeight: 600,
              textTransform: textTransform,
            }}
            onClick={() => {
              handleActiveTab(defaultText);
            }}
          >
            {defaultText}
          </Box>
        )}

        {listStatus.map((item, index) => (
          <Box
            component="button"
            sx={{
              border: `1px solid ${
                selectedFile === item?.label ? `#${item?.color}` : "#bdbdbd"
              }`,
              textAlign: "center",
              fontSize: "0.688rem",
              bgcolor:
                selectedFile === item?.label
                  ? `#${item?.color}`
                  : "transparent",
              p: 1,
              borderRadius: "10px",
              color: selectedFile === item?.label ? "#fff" : "#bdbdbd",
              fontWeight: 600,
              textTransform: textTransform,
            }}
            onClick={() => {
              handleActiveTab(item?.label);
            }}
            key={index}
          >
            {item.label}
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default ListStatusDelivery;
