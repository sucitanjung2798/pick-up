import React from "react";
import { Box, TextField } from "@mui/material";

const CustomDescriptionField = ({
  value,
  setValue,
  sx,
  placeholder = "Please input the description",
}) => {
  return (
    <Box
      sx={{
        borderRadius: "10px",
        "& .MuiTextField-root": { width: "100%" },
        "& .MuiOutlinedInput-root": {
          borderColor: "#bdbdbd",
          borderRadius: "10px",
          fontSize: "0.875rem",
        },
        ...sx,
      }}
      noValidate
      autoComplete="off"
    >
      <TextField
        multiline
        rows={4}
        placeholder={placeholder}
        value={value || ""}
        onChange={(e) => {
          setValue(e.target.value);
        }}
      />
    </Box>
  );
};

export default CustomDescriptionField;
