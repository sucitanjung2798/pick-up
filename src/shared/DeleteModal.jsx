import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";
import BottomDrawer from "./BottomDrawer";

const DeleteModal = ({ isOpen, toggleDrawer, title, path, message }) => {
  const navigate = useNavigate();

  return (
    <BottomDrawer isOpen={isOpen} toggleDrawer={toggleDrawer}>
      <Typography sx={{ fontSize: "1.125rem", fontWeight: 600 }}>
        {title}
      </Typography>
      {/* <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        
        <CloseRoundedIcon
          sx={{ fontSize: "25px" }}
          onClick={toggleDrawer("bottom", false)}
        />
      </Box> */}

      <Box sx={{ mt: 2 }}>
        <Typography sx={{ textAlign: "center", fontWeight: 500 }}>
          {message}
        </Typography>
      </Box>

      <Box
        sx={{
          mt: 2,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: 2,
        }}
      >
        <Button
          sx={{
            mt: 3,
            textTransform: "capitalize",

            color: "#78251E",
            fontSize: "18px",
            bgcolor: "#ffffff",
            border: "1px solid #78251E",
            height: "45px",
            borderRadius: "10px",
            ":hover": {
              bgcolor: "#ffffff",
              color: "#78251E",
              border: "1px solid #78251E",
            },
          }}
          fullWidth
          onClick={toggleDrawer("bottom", false)}
        >
          Cancel
        </Button>
        <Button
          sx={{
            mt: 3,
            textTransform: "capitalize",

            color: "#ffffff",
            fontSize: "18px",
            bgcolor: "#78251E",
            height: "45px",
            borderRadius: "10px",
            ":hover": {
              bgcolor: "#78251E",
              color: "#ffffff",
            },
          }}
          onClick={() => {
            toggleDrawer("bottom", false);
            navigate(`/${path}`);
          }}
          fullWidth
        >
          Delete
        </Button>
      </Box>
    </BottomDrawer>
  );
};

export default DeleteModal;
