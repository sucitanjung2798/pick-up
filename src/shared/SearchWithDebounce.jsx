import React, { useRef } from "react";
import debounce from "lodash.debounce";
import SearchBar from "./SearchBar";
import { Close as CloseIcon } from "@mui/icons-material";

const SearchWithDebounce = ({
  setSavedSearch,
  updatedSearchParams,
  setSearchParams,
  setSearch,
  search,
}) => {
  const debounceSearch = useRef(
    debounce((nextValue) => {
      setSavedSearch(nextValue);
    }, 1000)
  ).current;

  const handleChangeSearch = (event) => {
    const { value } = event.target;
    value !== ""
      ? updatedSearchParams.set("search", value)
      : updatedSearchParams.delete("search");

    setSearchParams(updatedSearchParams, { replace: true });
    setSearch(value);
    debounceSearch(value);
  };

  const handleClearSearch = () => {
    updatedSearchParams.delete("search");
    setSearchParams(updatedSearchParams, { replace: true });
    setSearch("");
    debounceSearch("");
  };
  return (
    <SearchBar
      value={search}
      onChange={handleChangeSearch}
      placeholder="Search Package"
      isQrCodeNeeded={true}
      search={search}
      endAdornment={
        search !== "" && (
          <CloseIcon onClick={handleClearSearch} sx={{ cursor: "pointer" }} />
        )
      }
    />
  );
};

export default SearchWithDebounce;
