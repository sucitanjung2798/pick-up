import React, { useContext, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import {
  Badge,
  Box,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from "@mui/material";
import {
  MenuRounded as MenuRoundedIcon,
  NotificationsNoneRounded as NotificationsNoneRoundedIcon,
} from "@mui/icons-material";
import BisaGroupIcon from "../assets/bisagroup-logo.png";
import HomeIcon from "../assets/Home 2.png";
import RoleIcon from "../assets/Mask group.png";
import LockIcon from "../assets/Lock.png";
import OrderIcon from "../assets/Order.png";
import DepartmentIcon from "../assets/Department.png";
import UserIcon from "../assets/User.png";
import HomeIconActive from "../assets/home-icon-active.png";
import RoleIconActive from "../assets/role-icon-active.png";
import LockIconActive from "../assets/lock-icon-active.png";
import OrderIconActive from "../assets/order-icon-active.png";
import DepartmentIconActive from "../assets/department-icon-active.png";
import UserIconActive from "../assets/user-icon-active.png";
import UomInactive from "../assets/uom-inactive.png";
import UomActive from "../assets/uom-active.png";
import { AuthContext } from "../contexts/AuthContext";

const Header = ({
  isNotificationsNeed = false,
  title = "Bisa Group",
  isHistoryNeed = false,
}) => {
  const location = useLocation();
  const navigate = useNavigate();
  const { countNotif } = useContext(AuthContext);
  const [isSlidebarOpen, setIsSlidebarOpen] = useState({ left: false });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setIsSlidebarOpen({ ...isSlidebarOpen, [anchor]: open });
  };

  const listMenu = (anchor) => (
    <Box
      sx={{
        width: 210,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100vh",
      }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Box>
        <Toolbar>
          <Box sx={{ display: "flex", gap: 2, mt: 1, alignItems: "center" }}>
            <img
              src={BisaGroupIcon}
              alt="Bisa Group Logo"
              width={40}
              style={{ borderRadius: "50%" }}
            />
            <Typography
              sx={{ fontSize: "1rem", fontWeight: 500, color: "#fff" }}
            >
              BISA GROUP
            </Typography>
          </Box>
        </Toolbar>

        <List sx={{ color: "#ffffff" }}>
          <ListItem disablePadding>
            <ListItemButton
              sx={{ bgcolor: location.pathname === "/dashboard" && "#ffffff" }}
              components={Link}
              to="/dashboard"
            >
              <ListItemIcon sx={{ color: "#78251E" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/dashboard"
                      ? HomeIconActive
                      : HomeIcon
                  }
                  width={24}
                  height={24}
                  sx={{
                    ml: 1,
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Dashboard"
                sx={{
                  color:
                    location.pathname === "/dashboard" ? "#78251E" : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              sx={{ bgcolor: location.pathname === "/roles" && "#ffffff" }}
              components={Link}
              to="/roles"
            >
              <ListItemIcon sx={{ color: "#78251E" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/roles" ? RoleIconActive : RoleIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Roles"
                sx={{
                  color: location.pathname === "/roles" ? "#78251E" : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor: location.pathname === "/permissions" && "#ffffff",
              }}
              components={Link}
              to="/permissions"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/permissions"
                      ? LockIconActive
                      : LockIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Permissions"
                sx={{
                  color:
                    location.pathname === "/permissions"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor: location.pathname === "/package/lists" && "#ffffff",
              }}
              components={Link}
              to="/package/lists"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/package/lists"
                      ? OrderIconActive
                      : OrderIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Packages"
                sx={{
                  color:
                    location.pathname === "/package/lists"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          {/* <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor:
                  location.pathname === "/consignment/lists" && "#ffffff",
              }}
              components={Link}
              to="/consignment/lists"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/consignment/lists"
                      ? PickUpIconActive
                      : DeliverPackage
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Pick Up"
                sx={{
                  color:
                    location.pathname === "/consignment/lists"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem> */}

          <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor: location.pathname === "/department/lists" && "#ffffff",
              }}
              components={Link}
              to="/department/lists"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/department/lists"
                      ? DepartmentIconActive
                      : DepartmentIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Departments"
                sx={{
                  color:
                    location.pathname === "/department/lists"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor:
                  location.pathname === "/manage-user/lists" && "#FFFFFF",
              }}
              components={Link}
              to="/manage-user/lists"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/manage-user/lists"
                      ? UserIconActive
                      : UserIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Manage User"
                sx={{
                  color:
                    location.pathname === "/manage-user/lists"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor: location.pathname === "/uom-lists" && "#FFFFFF",
              }}
              components={Link}
              to="/uom-lists"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/uom-lists" ? UomInactive : UomActive
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="UOM"
                sx={{
                  color:
                    location.pathname === "/uom-lists" ? "#78251E" : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem>

          {/* <ListItem disablePadding>
            <ListItemButton
              sx={{
                bgcolor: location.pathname === "/report-list" && "#ffffff",
              }}
              components={Link}
              to="/report-list"
            >
              <ListItemIcon sx={{ color: "#ffffff" }}>
                <Box
                  component="img"
                  src={
                    location.pathname === "/report-list"
                      ? ReportIconActive
                      : ReportIcon
                  }
                  width={24}
                  height={24}
                  sx={{ ml: 1 }}
                />
              </ListItemIcon>
              <ListItemText
                primary="Report"
                sx={{
                  color:
                    location.pathname === "/report-list"
                      ? "#78251E"
                      : "#ffffff",
                  fontSize: "1rem",
                }}
              />
            </ListItemButton>
          </ListItem> */}
        </List>
      </Box>
    </Box>
  );
  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          bgcolor: "#78251E",
          px: 2,
          py: 1,
          position: "sticky",
          top: 0,
          zIndex: 10,
        }}
      >
        <Box sx={{ display: "flex", gap: 1.5, alignItems: "center" }}>
          {/* <AccountCircleIcon
          sx={{ fontSize: "40px", color: "#78251E", cursor: "pointer" }}
          onClick={toggleDrawer("left", true)}
        /> */}
          <MenuRoundedIcon
            sx={{ color: "#ffffff", fontSize: "1.875rem" }}
            onClick={toggleDrawer("left", true)}
          />
          <Drawer
            anchor={"left"}
            open={isSlidebarOpen["left"]}
            onClose={toggleDrawer("left", false)}
            sx={{
              "& .MuiDrawer-paper": {
                bgcolor: "#78251E",
              },
            }}
          >
            {listMenu("left")}
          </Drawer>
          <Box>
            <Typography
              sx={{ fontWeight: 500, color: "#ffffff", fontSize: "1rem" }}
            >
              {title}
            </Typography>
            <Typography
              sx={{ fontSize: "0.875rem", color: "#bdbdbd", fontWeight: 500 }}
            >
              Post Office
            </Typography>
          </Box>
        </Box>

        {isNotificationsNeed && (
          <Badge badgeContent={countNotif?.totalUnread} color="error">
            <NotificationsNoneRoundedIcon
              onClick={() => navigate("/notifications")}
              sx={{ color: "#ffffff", fontSize: "1.563rem" }}
            />
          </Badge>
        )}

        {/* {isHistoryNeed && <Box component="img" src={HistoryIcon} />} */}
      </Box>
    </Box>
  );
};

export default Header;
