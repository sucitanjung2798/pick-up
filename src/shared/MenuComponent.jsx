import React, { useState } from "react";
import { Box, IconButton, Menu, MenuItem } from "@mui/material";
import { MoreVert as MoreVertIcon } from "@mui/icons-material";

const MenuComponent = ({ menuLists }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    event.stopPropagation();
  };
  const handleClose = (event) => {
    setAnchorEl(null);
    event.stopPropagation();
  };

  return (
    <Box>
      <IconButton
        aria-label="More"
        aria-owns={open ? "role-menu" : null}
        aria-haspopup="true"
        onClick={handleClick}
        sx={{ p: 0 }}
        size="small"
      >
        <MoreVertIcon sx={{ color: "#000000" }} />
      </IconButton>
      <Menu
        id="role-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        {menuLists.map((item, index) => (
          <MenuItem
            key={index}
            onClick={(event) => {
              item.clickHandler();
              setAnchorEl(null);
              event.stopPropagation();
            }}
            sx={{ fontSize: "0.875rem" }}
          >
            {item.name}
          </MenuItem>
        ))}
      </Menu>
    </Box>
  );
};

export default MenuComponent;
