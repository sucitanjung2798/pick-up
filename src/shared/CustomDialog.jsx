import React from "react";
import { Dialog } from "@mui/material";

const CustomDialog = ({ open, handleClose, children, sx }) => {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      sx={{
        "& .MuiDialog-paper": {
          width: "100%",
          pb: 2,
        },
        ...sx,
      }}
    >
      {children}
    </Dialog>
  );
};

export default CustomDialog;
