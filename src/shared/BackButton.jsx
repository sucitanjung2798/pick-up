import React from "react";
import { ArrowBackRounded as ArrowBackRoundedIcon } from "@mui/icons-material";
import { Box, Typography } from "@mui/material";

const BackButton = ({ onClick }) => {
  return (
    <Box
      sx={{
        display: "flex",
        gap: 2,
        alignItems: "center",

        width: "100px",
      }}
      onClick={onClick}
    >
      <ArrowBackRoundedIcon sx={{ fontSize: "27px" }} />
      <Typography sx={{ fontSize: "0.875rem", fontWeight: 600 }}>
        Back
      </Typography>
    </Box>
  );
};

export default BackButton;
