import React from "react";
import { Box, Drawer } from "@mui/material";

export default function BottomDrawer({ isOpen, toggleDrawer, children }) {
  return (
    <div>
      <React.Fragment key={"bottom"}>
        <Drawer
          anchor={"bottom"}
          open={isOpen["bottom"]}
          //   onClose={toggleDrawer("bottom", false)}
          sx={{
            "& .MuiDrawer-paper": {
              borderTopRightRadius: "20px",
              borderTopLeftRadius: "20px",
            },
          }}
        >
          <Box
            sx={{ p: 1.5 }}
            role="presentation"
            // onClick={toggleDrawer("bottom", false)}
            onKeyDown={toggleDrawer("bottom", false)}
          >
            {children}
          </Box>
        </Drawer>
      </React.Fragment>
    </div>
  );
}
