import React, { useState } from "react";
import { Box, Fab } from "@mui/material";
import { KeyboardArrowUpRounded as KeyboardArrowUpRoundedIcon } from "@mui/icons-material";

const ScrollOnTop = () => {
  const [visible, setVisible] = useState(false);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) {
      setVisible(true);
    } else if (scrolled <= 300) {
      setVisible(false);
    }
  };

  const scrollOnTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  window.addEventListener("scroll", toggleVisible);
  return (
    <Box
      sx={{
        position: "fixed",
        bottom: 80,
        right: 20,
        color: "red",
        zIndex: 10,
        display: visible ? "inline" : "none",
      }}
      onClick={scrollOnTop}
    >
      <Fab size="small" color="primary" sx={{ color: "#fff" }}>
        <KeyboardArrowUpRoundedIcon sx={{ fontSize: "2rem" }} />
      </Fab>
    </Box>
  );
};

export default ScrollOnTop;
