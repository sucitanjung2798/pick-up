import React from "react";
import { FormControlLabel, FormGroup, Switch, styled } from "@mui/material";
import CheckIcon from "../assets/check-icon.png";
import CheckIconInactive from "../assets/check-icon-inactive.png";

const CustomSwitchStyle = styled(Switch)(({ theme }) => ({
  width: 50,
  height: 28,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 2,
    transform: "translateX(1px)",
    "&.Mui-checked": {
      color: "#78251E",
      transform: "translateX(21px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url(${CheckIcon})`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: "#78251E",
        border: 0,
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: "#fff",
    width: 22,
    height: 22,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url(${CheckIconInactive})`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: "#bdbdbd",
    borderRadius: 26 / 2,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

const CustomSwitch = ({ label, value, setValue }) => {
  return (
    <FormGroup
      sx={{
        "& .MuiFormControlLabel-root": {
          marginRight: 0,
        },
      }}
    >
      <FormControlLabel
        labelPlacement="end"
        label={label}
        control={
          <CustomSwitchStyle
            checked={value}
            onChange={() => setValue(!value)}
          />
        }
      />
    </FormGroup>
  );
};

export default CustomSwitch;
