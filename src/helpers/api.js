import axios from "axios";
import { camelizeKeys, decamelizeKeys } from "humps";

export const authAxios = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

export const adminAxios = axios.create({
  baseURL: "/api/v1",
});

const controller = new AbortController();

axios.defaults.headers.common["Authorization"] =
  localStorage.getItem("accessToken");

axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

adminAxios.interceptors.request.use(
  (config, data) => {
    const accessToken = localStorage.getItem("accessToken");

    if (accessToken) config.headers.Authorization = `Bearer ${accessToken}`;

    data = decamelizeKeys(data);
    config.data = decamelizeKeys(config.data);

    config.params = decamelizeKeys(config.params);
    config.signal = controller.signal;

    return config;
  },
  (error) => Promise.reject(error)
);

adminAxios.interceptors.response.use(
  (response) => camelizeKeys(response.data),
  (error) => {
    if (
      error?.response?.status === 403 &&
      localStorage.getItem("accessToken") !== null
    ) {
      controller.abort();
      localStorage.removeItem("accessToken");
      localStorage.removeItem("permissions");
      alert("Your session has expired");
      window.location.reload();
    } else if (error?.response?.status === 502) {
      controller.abort();
      alert("Bad Gateway");
    }

    return Promise.reject(error);
  }
);
