import { Slide, toast } from "react-toastify";

export const viewAccess = Object.freeze({
  configView: "config.view",
  configShow: "config.show",
  configCreate: "config.create",
  configUpdate: "config.update",
  configDelete: "config.delete",
  roleView: "role.view",
  roleShow: "role.show",
  roleCreate: "role.create",
  roleUpdate: "role.update",
  roleDelete: "role.delete",
  permView: "perm.view",
  permShow: "perm.show",
  permCreate: "perm.create",
  permUpdate: "perm.update",
  permDelete: "perm.delete",
  accessShow: "access.show",
  accessUpdate: "access.update",
  deptView: "dept.view",
  deptShow: "dept.show",
  deptCreate: "dept.create",
  deptUpdate: "dept.update",
  deptDelete: "dept.delete",
  userView: "user.view",
  userShow: "user.show",
  userCreate: "user.create",
  userUpdate: "user.update",
  userDelete: "user.delete",
  userReset: "user.reset",
  uomView: "uom.view",
  uomShow: "uom.show",
  uomCreate: "uom.create",
  uomUpdate: "uom.update",
  uomDelete: "uom.delete",
  packageAdmin: "package.admin",
  packageView: "package.view",
  packageShow: "package.show",
  packageCreate: "package.create",
  packageUpdate: "package.update",
  packageDelete: "package.delete",
  consignCourier: "consign.courier",
  consignAdmin: "consign.admin",
  consignView: "consign.view",
  consignShow: "consign.show",
  consignCreate: "consign.create",
  consignUpdate: "consign.update",
  consignDelete: "consign.delete",
  settleAdmin: "settle.admin",
  settleView: "settle.view",
  settleShow: "settle.show",
  settleCreate: "settle.create",
  settleUpdate: "settle.update",
  settleDelete: "settle.delete",
  reportView: "report.view",
  utilView: "util.view",
  utilSessRemove: "util.sess_remove",
  utilDbImport: "util.db_import",
  utilDbExport: "util.db_export",
  utilDbClear: "util.db_clear",
});

export const roleNamePermission = Object.freeze({
  config: "Configurations",
  role: "Roles",
  perm: "Permissions",
  access: "Accesses",
  dept: "Departments",
  user: "Users",
  uom: "UOM",
  package: "Packages",
  consign: "Consigns",
  settle: "Settles",
  report: "Reports",
  util: "Utils",
});

export function convertUtcToWib(utcDateTime) {
  const utcDate = new Date(utcDateTime + " UTC");
  const offset = new Date().getTimezoneOffset();
  const localTime = new Date(utcDate.getTime() - offset * 60 * 1000);
  return localTime;
}

export function formatTime(value) {
  const hours = new Date(value).getHours();
  const minutes = new Date(value).getMinutes();

  const getTime = `${hours + 7 < 10 ? "0" : ""}${hours + 7}:${
    minutes < 10 ? "0" : ""
  }${minutes}`;

  return getTime;
}

export function toastSuccess(text) {
  toast.success(text, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "light",
    transition: Slide,
  });
}
